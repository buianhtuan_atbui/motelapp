package com.example.motelapp.motel;

import com.example.motelapp.models.Apartment;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.utils.ConfigApi;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MotelService {

    @POST(ConfigApi.Api.LOGIN)
    @Headers("Content-Type:application/json; charset=utf-8")
    Call<ResponseBody> login(@Body RequestBody body);

    @POST(ConfigApi.Api.SIGNUP)
    Call<ResponseBody> signUp(@Body RequestBody body);
    @PUT(ConfigApi.Api.CHANGE_PASSWORD)
    Call<ResponseBody> changePassword(@Body RequestBody body);

    @GET(ConfigApi.Api.GET_APARTMENT)
    Call<ResponseBody> getApartment(@Query("AccountId") int accountId);
    @POST(ConfigApi.Api.CREATE_APARTMENT)
    Call<ResponseBody> createApartment(@Body RequestBody body);
    @PUT(ConfigApi.Api.UPDATE_APARTMENT)
    Call<ResponseBody> updateApartment(@Body RequestBody body);
    @DELETE(ConfigApi.Api.DELETE_APARTMENT)
    Call<ResponseBody> deleteApartment(@Query("Id") int apartmentId);

    @GET(ConfigApi.Api.GET_ROOM)
    Call<ResponseBody> getRoom(@Query("ApartmentId") int apartmentId);
    @POST(ConfigApi.Api.CREATE_ROOM)
    Call<ResponseBody> createRoom(@Body RequestBody body);
    @PUT(ConfigApi.Api.UPDATE_ROOM)
    Call<ResponseBody> updateRoom(@Body RequestBody body);


    @GET(ConfigApi.Api.GET_CONTRACT)
    Call<ResponseBody> getContract(@Query("apartment_id") int apartmentId);
    @POST(ConfigApi.Api.CREATE_CONTRACT)
    Call<ResponseBody> createContract(@Body RequestBody body);
    @PUT(ConfigApi.Api.UPDATE_CONTRACT)
    Call<ResponseBody> updateContract(@Body RequestBody body);
    @DELETE(ConfigApi.Api.DELETE_CONTRACT)
    Call<ResponseBody> deleteContract(@Query("Id") int contractId);

    @GET(ConfigApi.Api.GET_PROVINCE)
    Call<ResponseBody> getProvince();
    @GET(ConfigApi.Api.GET_DISTRICT)
    Call<ResponseBody> getDistrict(@Query("Country_Id") int country_id);
    @GET(ConfigApi.Api.GET_WARDS)
    Call<ResponseBody> getWards(@Query("District_Id") int district_Id);



    @GET(ConfigApi.Api.GET_CUSTOMER)
    Call<ResponseBody> getCustomer(@Query("apartment_id") int apartmentId);
    @PUT(ConfigApi.Api.UPDATE_CUSTOMER)
    Call<ResponseBody> updateCustomer(@Body RequestBody body);
    @POST(ConfigApi.Api.CREATE_CUSTOMER)
    Call<ResponseBody> createCustomer(@Body RequestBody body);
    @DELETE(ConfigApi.Api.DELETE_CUSTOMER)
    Call<ResponseBody> deleteCustomer(@Query("ids") List<Integer> listId);

    @POST(ConfigApi.Api.CREATE_NEWS)
    Call<ResponseBody> createNews(@Body RequestBody body);
    @GET(ConfigApi.Api.GET_NEWS)
    Call<ResponseBody> getNews(@Query("apartment_id") int apartmentId);

    @POST(ConfigApi.Api.CREATE_BILL)
    Call<ResponseBody> createBills(@Body RequestBody body);

    @Multipart
    @POST(ConfigApi.Api.UPLOAD_IMAGE)
    Call<ResponseBody> uploadImage(@Part List<MultipartBody.Part> image);



}
