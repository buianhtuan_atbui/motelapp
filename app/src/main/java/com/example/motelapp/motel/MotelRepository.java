package com.example.motelapp.motel;

import android.content.Context;

import com.example.motelapp.models.Account;
import com.example.motelapp.models.Apartment;

import com.example.motelapp.models.Bill;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.District;
import com.example.motelapp.models.Image;
import com.example.motelapp.models.News;
import com.example.motelapp.models.Province;
import com.example.motelapp.models.Room;
import com.example.motelapp.models.Wards;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.utils.CallBackData;

import java.util.List;

import okhttp3.MultipartBody;

public interface MotelRepository {
    void login(Context context, String userName, String password, CallBackData<User> callBackData);
    void signUp(Context context, Account account, CallBackData<Account> callBackData);
    void changePassword(Context context, Account account, CallBackData<Account> callBackData);

    void getListApartment(Context context, int userId, CallBackData<List<Apartment>> callBackData);
    void createApartment(Context context, Apartment apartment, CallBackData<Apartment> callBackData);
    void updateApartment(Context context, Apartment apartment, CallBackData<Apartment> callBackData);
    void deleteApartment(Context context, int apartmentId, CallBackData<Apartment> callBackData);

    void getListRoom(Context context, int apartmentId, CallBackData<List<Room>> callBackData);
    void createRoom(Context context, Room room, CallBackData<Room> callBackData);
    void updateRoom(Context context, Room room, CallBackData<Room> callBackData);

    void getListContract(Context context, int aparmentId, CallBackData<List<Contract>> callBackData);
    void createContract(Context context, Contract contract, CallBackData<Contract> callBackData);
    void updateContract(Context context, Contract contract, CallBackData<Contract> callBackData);
    void deleteContract(Context context, int contractId, CallBackData<Contract> callBackData);


    void getListProvince(Context context, CallBackData<List<Province>> callBackData);
    void getListDistrict(Context context, int country_id, CallBackData<List<District>> callBackData);
    void getListWards(Context context, int district_Id, CallBackData<List<Wards>> callBackData);


    void getListCustomer(Context context, int aparmentId, CallBackData<List<Customer>> callBackData);
    void updateCustomer(Context context, Customer customer, CallBackData<Customer> callBackData);
    void createCustomer(Context context, Customer customer, CallBackData<Customer> callBackData);
    void deleteCustomer(Context context, List<Integer> listId, CallBackData<String> callBackData);

    void createNews(Context context, News news, CallBackData<News> callBackData);
    void getNews(Context context, int apartmentId, CallBackData<List<News>> callBackData);

    void createBill(Context context, Bill bill, CallBackData<Bill> callBackData);

    void uploadFile(Context context, List<MultipartBody.Part> file, CallBackData<List<Image>> callBackData);



}
