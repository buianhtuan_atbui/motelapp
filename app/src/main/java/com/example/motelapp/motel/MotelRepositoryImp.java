package com.example.motelapp.motel;

import android.content.Context;

import com.example.motelapp.models.Account;
import com.example.motelapp.models.Apartment;

import com.example.motelapp.models.Bill;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.District;
import com.example.motelapp.models.Image;
import com.example.motelapp.models.News;
import com.example.motelapp.models.Province;
import com.example.motelapp.models.Room;
import com.example.motelapp.models.Wards;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.utils.ClientApi;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MotelRepositoryImp implements MotelRepository {

    public static User user;
    public static List<Apartment> listApartment;


    @Override
    public void login(Context context, String phone, String password, final CallBackData<User> callBackData) {
        ClientApi clientApi = new ClientApi();
        JSONObject object = new JSONObject();
        try {
            object.put("phone", phone);
            object.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), object.toString());
        Call<ResponseBody> serviceCall = clientApi.motelService().login(body);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        User responseResult = new Gson().fromJson(result, User.class);
                        if (responseResult == null) {
                            callBackData.onFail(response.message());
                        } else {
                            callBackData.onSuccess(responseResult);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    callBackData.onFail("");
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callBackData.onFail("Login Fail (ResoRepositoryImp)");
            }
        });
    }

    @Override
    public void signUp(Context context, Account account, final CallBackData<Account> callBackData) {
        ClientApi clientApi = new ClientApi();
        final Gson gson = new Gson();
        String json = gson.toJson(account);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Call<ResponseBody> serviceCall = clientApi.motelService().signUp(requestBody);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        Account account = gson.fromJson(result, Account.class);
                        if (account != null) {
                            callBackData.onSuccess(account);
                        } else {
                            callBackData.onFail("Sign Up Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void changePassword(Context context, Account account, final CallBackData<Account> callBackData) {
        ClientApi clientApi = new ClientApi();
        final Gson gson = new Gson();
        String json = gson.toJson(account);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Call<ResponseBody> serviceCall = clientApi.motelService().changePassword(requestBody);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        Account account = gson.fromJson(result, Account.class);
                        if (account != null) {
                            callBackData.onSuccess(account);
                        } else {
                            callBackData.onFail("Sign Up Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    @Override
    public void getListApartment(Context context, int userId, final CallBackData<List<Apartment>> callBackData) {
        listApartment = new ArrayList<>();
        try {
            ClientApi clientApi = new ClientApi();
            Call<ResponseBody> serviceCall = clientApi.motelService().getApartment(userId);
            serviceCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200 && response.body() != null) {
                        try {
                            String result = response.body().string();
                            JSONArray arr = new JSONArray(result);
                            for (int i = 0; i < arr.length(); i++) {
                                Apartment info = new Gson().fromJson(arr.getJSONObject(i).toString(), Apartment.class);
                                listApartment.add(info);
                            }
                            if (listApartment == null) {
                                callBackData.onFail("Login Failed");
                            } else {
                                callBackData.onSuccess(listApartment);

                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    callBackData.onFail("GEet List Apartment Failed (ResoRepositoryImp)");
                }
            });
        } catch (Exception e) {

        }
    }

    @Override
    public void getListRoom(Context context, int apartmentId, final CallBackData<List<Room>> callBackData) {
        ClientApi clientApi = new ClientApi();
        final List<Room> roomList;
        roomList = new ArrayList<>();
        Call<ResponseBody> serviceCall = clientApi.motelService().getRoom(apartmentId);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        JSONArray arr = new JSONArray(result);
                        for (int i = 0; i < arr.length(); i++) {
                            Room info = new Gson().fromJson(arr.getJSONObject(i).toString(), Room.class);
                            roomList.add(info);
                        }
                        if (roomList == null) {
                            callBackData.onFail("Login Failed");
                        } else {
                            callBackData.onSuccess(roomList);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callBackData.onFail("GEet List Apartment Failed (ResoRepositoryImp)");
            }
        });
    }

    @Override
    public void getListContract(Context context, int aparmentId, final CallBackData<List<Contract>> callBackData) {
        try {
            ClientApi clientApi = new ClientApi();
            final List<Contract> contractList = new ArrayList<>();
            Call<ResponseBody> serviceCall = clientApi.motelService().getContract(aparmentId);
            serviceCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200 && response.body() != null) {
                        contractList.removeAll(contractList);
                        try {
                            String result = response.body().string();
                            JSONArray arr = new JSONArray(result);
                            for (int i = 0; i < arr.length(); i++) {

                                Contract info = new Gson().fromJson(arr.getJSONObject(i).toString(), Contract.class);
                                contractList.add(info);
                            }
                            if (contractList.size() == 0) {
                                callBackData.onFail("Login Failed");
                            } else {
                                callBackData.onSuccess(contractList);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    callBackData.onFail("FAIL");
                }
            });
        } catch (Exception ex) {

        }
    }

    @Override
    public void getListProvince(Context context, final CallBackData<List<Province>> callBackData) {
        ClientApi clientApi = new ClientApi();
        final List<Province> provinceList;
        provinceList = new ArrayList<>();
        Call<ResponseBody> serviceCall = clientApi.motelService().getProvince();
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        JSONArray arr = new JSONArray(result);
                        for (int i = 0; i < arr.length(); i++) {
                            Province info = new Gson().fromJson(arr.getJSONObject(i).toString(), Province.class);
                            provinceList.add(info);
                        }
                        if (provinceList == null) {
                            callBackData.onFail("get list province fail");
                        } else {
                            callBackData.onSuccess(provinceList);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void getListDistrict(Context context, int country_id, final CallBackData<List<District>> callBackData) {
        ClientApi clientApi = new ClientApi();
        final List<District> districtList;
        districtList = new ArrayList<>();
        Call<ResponseBody> serviceCall = clientApi.motelService().getDistrict(country_id);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        JSONArray arr = new JSONArray(result);
                        for (int i = 0; i < arr.length(); i++) {
                            District info = new Gson().fromJson(arr.getJSONObject(i).toString(), District.class);
                            districtList.add(info);
                        }
                        if (districtList == null) {
                            callBackData.onFail("get list district fail");
                        } else {
                            callBackData.onSuccess(districtList);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void getListWards(Context context, int district_Id, final CallBackData<List<Wards>> callBackData) {
        ClientApi clientApi = new ClientApi();
        final List<Wards> wardsList;
        wardsList = new ArrayList<>();
        Call<ResponseBody> serviceCall = clientApi.motelService().getWards(district_Id);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        JSONArray arr = new JSONArray(result);
                        for (int i = 0; i < arr.length(); i++) {
                            Wards info = new Gson().fromJson(arr.getJSONObject(i).toString(), Wards.class);
                            wardsList.add(info);
                        }
                        if (wardsList == null) {
                            callBackData.onFail("get list district fail");
                        } else {
                            callBackData.onSuccess(wardsList);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void createApartment(Context context, Apartment apartment, final CallBackData<Apartment> callBackData) {
        ClientApi clientApi = new ClientApi();
        final Gson gson = new Gson();
        String json = gson.toJson(apartment);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Call<ResponseBody> serviceCall = clientApi.motelService().createApartment(requestBody);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        Apartment apartment1 = gson.fromJson(result, Apartment.class);
                        if (apartment1 != null) {
                            callBackData.onSuccess(apartment1);
                        } else {
                            callBackData.onFail("Create Apartment Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void updateApartment(Context context, Apartment apartment, final CallBackData<Apartment> callBackData) {
        ClientApi clientApi = new ClientApi();
        final Gson gson = new Gson();
        String json = gson.toJson(apartment);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Call<ResponseBody> serviceCall = clientApi.motelService().updateApartment(requestBody);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        Apartment apartment = gson.fromJson(result, Apartment.class);
                        if (apartment != null) {
                            callBackData.onSuccess(apartment);
                        } else {
                            callBackData.onFail("Create Apartment Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void deleteApartment(Context context, int apartmentId, final CallBackData<Apartment> callBackData) {
        ClientApi clientApi = new ClientApi();
        Call<ResponseBody> serviceCall = clientApi.motelService().deleteApartment(apartmentId);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        Gson gson = new Gson();
                        String result = response.body().string();
                        Apartment apartment = gson.fromJson(result, Apartment.class);
                        if (apartment != null) {
                            callBackData.onSuccess(apartment);
                        } else {
                            callBackData.onFail("Create Apartment Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void createRoom(Context context, Room room, final CallBackData<Room> callBackData) {
        ClientApi clientApi = new ClientApi();
        final Gson gson = new Gson();
        String json = gson.toJson(room);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Call<ResponseBody> serviceCall = clientApi.motelService().createRoom(requestBody);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        Room room = gson.fromJson(result, Room.class);
                        if (room != null) {
                            callBackData.onSuccess(room);
                        } else {
                            callBackData.onFail("Create Room Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    callBackData.onFail("Loi api");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void updateRoom(Context context, Room room, final CallBackData<Room> callBackData) {
        ClientApi clientApi = new ClientApi();
        final Gson gson = new Gson();
        String json = gson.toJson(room);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Call<ResponseBody> serviceCall = clientApi.motelService().updateRoom(requestBody);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        Room room = gson.fromJson(result, Room.class);
                        if (room != null) {
                            callBackData.onSuccess(room);
                        } else {
                            callBackData.onFail("Create Room Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void getListCustomer(Context context, int aparmentId, final CallBackData<List<Customer>> callBackData) {
        ClientApi clientApi = new ClientApi();
        final List<Customer> customerList;
        customerList = new ArrayList<>();
        Call<ResponseBody> serviceCall = clientApi.motelService().getCustomer(aparmentId);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        JSONArray arr = new JSONArray(result);
                        for (int i = 0; i < arr.length(); i++) {
                            Customer info = new Gson().fromJson(arr.getJSONObject(i).toString(), Customer.class);
                            customerList.add(info);
                        }
                        if (customerList.size() == 0) {
                            callBackData.onFail("get list province fail");
                        } else {
                            callBackData.onSuccess(customerList);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void updateCustomer(Context context, Customer customer, final CallBackData<Customer> callBackData) {
        ClientApi clientApi = new ClientApi();
        final Gson gson = new Gson();
        String json = gson.toJson(customer);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Call<ResponseBody> serviceCall = clientApi.motelService().updateCustomer(requestBody);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        Customer customer = gson.fromJson(result, Customer.class);
                        if (customer != null) {
                            callBackData.onSuccess(customer);
                        } else {
                            callBackData.onFail("Update Customer Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void createCustomer(Context context, Customer customer, final CallBackData<Customer> callBackData) {
        ClientApi clientApi = new ClientApi();
        final Gson gson = new Gson();
        String json = gson.toJson(customer);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Call<ResponseBody> serviceCall = clientApi.motelService().createCustomer(requestBody);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        Customer customer = gson.fromJson(result, Customer.class);
                        if (customer != null) {
                            callBackData.onSuccess(customer);
                        } else {
                            callBackData.onFail("Update Customer Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void deleteCustomer(Context context, List<Integer> listId, final CallBackData<String> callBackData) {
        ClientApi clientApi = new ClientApi();
        Call<ResponseBody> serviceCall = clientApi.motelService().deleteCustomer(listId);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    callBackData.onSuccess("Delete Successful");
                } else {
                    callBackData.onFail("Delete Fail");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void createNews(Context context, News news, final CallBackData<News> callBackData) {
        ClientApi clientApi = new ClientApi();
        final Gson gson = new Gson();
        String json = gson.toJson(news);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Call<ResponseBody> serviceCall = clientApi.motelService().createNews(requestBody);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        News news = gson.fromJson(result, News.class);
                        if (news != null) {
                            callBackData.onSuccess(news);
                        } else {
                            callBackData.onFail("Update Customer Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void getNews(Context context, int apartmentId, final CallBackData<List<News>> callBackData) {
        ClientApi clientApi = new ClientApi();
        final List<News> mNewsList = new ArrayList<>();
        Call<ResponseBody> serviceCall = clientApi.motelService().getNews(apartmentId);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        JSONArray arr = new JSONArray(result);
                        for (int i = 0; i < arr.length(); i++) {
                            News info = new Gson().fromJson(arr.getJSONObject(i).toString(), News.class);
                            mNewsList.add(info);
                        }
                        if (mNewsList != null) {
                            callBackData.onSuccess(mNewsList);
                        } else {
                            callBackData.onFail("Update Customer Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void createBill(Context context, Bill bill, final CallBackData<Bill> callBackData) {
        ClientApi clientApi = new ClientApi();
        final Gson gson = new Gson();
        String json = gson.toJson(bill);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Call<ResponseBody> serviceCall = clientApi.motelService().createBills(requestBody);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        Bill bill = gson.fromJson(result, Bill.class);
                        if (bill != null) {
                            callBackData.onSuccess(bill);
                        } else {
                            callBackData.onFail("Update Customer Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void uploadFile(Context context, List<MultipartBody.Part> file, final CallBackData<List<Image>> callBackData) {
        ClientApi clientApi = new ClientApi();
        final Gson gson = new Gson();
        final List<Image> mImageList = new ArrayList<>();
 //       String json = gson.toJson(image);
  //      RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Call<ResponseBody> serviceCall = clientApi.motelService().uploadImage(file);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        JSONArray arr = new JSONArray(result);
                        for (int i = 0; i < arr.length(); i++) {
                            Image info = new Gson().fromJson(arr.getJSONObject(i).toString(), Image.class);
                            mImageList.add(info);
                        }
                        if (mImageList != null) {
                            callBackData.onSuccess(mImageList);
                        } else {
                            callBackData.onFail("Update Customer Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void createContract(Context context, Contract contract, final CallBackData<Contract> callBackData) {
        ClientApi clientApi = new ClientApi();
        final Gson gson = new Gson();
        String json = gson.toJson(contract);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Call<ResponseBody> serviceCall = clientApi.motelService().createContract(requestBody);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        Contract contract = gson.fromJson(result, Contract.class);
                        if (contract != null) {
                            callBackData.onSuccess(contract);
                        } else {
                            callBackData.onFail("Update Customer Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void updateContract(Context context, Contract contract, final CallBackData<Contract> callBackData) {
        ClientApi clientApi = new ClientApi();
        final Gson gson = new Gson();
        String json = gson.toJson(contract);
        RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
        Call<ResponseBody> serviceCall = clientApi.motelService().updateContract(requestBody);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        Contract contract = gson.fromJson(result, Contract.class);
                        if (contract != null) {
                            callBackData.onSuccess(contract);
                        } else {
                            callBackData.onFail("Update Customer Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void deleteContract(Context context, int contractId, final CallBackData<Contract> callBackData) {
        ClientApi clientApi = new ClientApi();
        final Gson gson = new Gson();
        Call<ResponseBody> serviceCall = clientApi.motelService().deleteContract(contractId);
        serviceCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200 && response.body() != null) {
                    try {
                        String result = response.body().string();
                        Contract contract = gson.fromJson(result, Contract.class);
                        if (contract != null) {
                            callBackData.onSuccess(contract);
                        } else {
                            callBackData.onFail("Update Customer Fail");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

}
