package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Image;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.UploadImageView;

import java.util.List;

import okhttp3.MultipartBody;

public class UploadImagePresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private UploadImageView mUploadImageView;

    public UploadImagePresenter(Application mApplication, Context mContext, UploadImageView mUploadImageView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mUploadImageView = mUploadImageView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void uploadImage(List<MultipartBody.Part> file) {
        mMotelRepository.uploadFile(mContext, file, new CallBackData<List<Image>>() {
            @Override
            public void onSuccess(List<Image> images) {
                mUploadImageView.uploadImageSuccessFul(images);
            }

            @Override
            public void onFail(String msgFail) {
                mUploadImageView.uploadImageFail();
            }
        });
    }
}
