package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Contract;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.UpdateContractView;

public class UpdateContractPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private UpdateContractView mUpdateContractView;

    public UpdateContractPresenter(Application mApplication, Context mContext, UpdateContractView mUpdateContractView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mUpdateContractView = mUpdateContractView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void updateContract(Contract contract) {
        mMotelRepository.updateContract(mContext, contract, new CallBackData<Contract>() {
            @Override
            public void onSuccess(Contract contract) {
                mUpdateContractView.updateContractSuccessful(contract);
            }

            @Override
            public void onFail(String msgFail) {
                mUpdateContractView.updateContractFail();
            }
        });
    }
}
