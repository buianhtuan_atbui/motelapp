package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Account;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.ChangePasswordView;

public class ChangePasswordPresenter {
    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private ChangePasswordView mChangePasswordView;

    public ChangePasswordPresenter(Application mApplication, Context mContext, ChangePasswordView mChangePasswordView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mChangePasswordView = mChangePasswordView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void changePassword(Account account){
        mMotelRepository.changePassword(mContext, account, new CallBackData<Account>() {
            @Override
            public void onSuccess(Account account) {
                mChangePasswordView.changePasswordSuccessful();
            }

            @Override
            public void onFail(String msgFail) {
                mChangePasswordView.changePasswordFail();
            }
        });
    }
}
