package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Room;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.ContractView;

import java.util.ArrayList;
import java.util.List;

public class ContractPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private ContractView mContractView;
    private Context mContext;

    private List<Contract> mContractList;

    public ContractPresenter(Application mApplication, Context mContext, ContractView mContractView) {
        mMotelRepository = new MotelRepositoryImp();
        this.mApplication = mApplication;
        this.mContractView = mContractView;
        this.mContext = mContext;
        mContractList = new ArrayList<>();
    }

    public void getListContract(int apartmentId) {
        mMotelRepository.getListContract(mContext, apartmentId, new CallBackData<List<Contract>>() {
            @Override
            public void onSuccess(List<Contract> contractList) {
                if (contractList.size() > 0) {
                    mContractList.addAll(contractList);
                    mContractView.getListContractSuccess(mContractList);
                }
            }

            @Override
            public void onFail(String msgFail) {
                mContractView.getListContractFail("FAIL");
            }
        });
    }
}

