package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Room;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.RoomView;

import java.util.List;

public class RoomPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private RoomView mRoomView;
    private Context mContext;

    public RoomPresenter(Application mApplication, Context mContext, RoomView mRoomView) {
        this.mMotelRepository = new MotelRepositoryImp();
        this.mApplication = mApplication;
        this.mRoomView = mRoomView;
        this.mContext = mContext;
    }

    public void getListRoom(int apartmentId){
        mMotelRepository.getListRoom(mContext, apartmentId, new CallBackData<List<Room>>() {
            @Override
            public void onSuccess(List<Room> roomList) {
                if(roomList != null){
                    mRoomView.getListRoomSuccess(roomList);
                }
            }

            @Override
            public void onFail(String msgFail) {

            }
        });
    }
}
