package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Bill;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.CreateBillView;

public class CreateBillPresenter {
    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private CreateBillView mCreateBillView;

    public CreateBillPresenter(Application mApplication, Context mContext, CreateBillView mCreateBillView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mCreateBillView = mCreateBillView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void createBill(Bill bill) {
        mMotelRepository.createBill(mContext, bill, new CallBackData<Bill>() {
            @Override
            public void onSuccess(Bill bill) {
                mCreateBillView.createBillSuccessful(bill);
            }

            @Override
            public void onFail(String msgFail) {
                mCreateBillView.createBillFail();
            }
        });
    }
}
