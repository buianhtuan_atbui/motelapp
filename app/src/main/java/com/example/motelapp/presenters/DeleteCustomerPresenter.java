package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Customer;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.DeleteCustomerView;

import java.util.List;

public class DeleteCustomerPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private DeleteCustomerView mDeleteCustomerView;

    public DeleteCustomerPresenter(Application mApplication, Context mContext, DeleteCustomerView mDeleteCustomerView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mDeleteCustomerView = mDeleteCustomerView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void deleteCustomer(List<Integer> listId){
        mMotelRepository.deleteCustomer(mContext, listId, new CallBackData<String>() {
            @Override
            public void onSuccess(String s) {
                mDeleteCustomerView.deleteCustomerSuccessful();
            }

            @Override
            public void onFail(String msgFail) {
                mDeleteCustomerView.deleteCustomerSuccessful();
            }
        });
    }
}
