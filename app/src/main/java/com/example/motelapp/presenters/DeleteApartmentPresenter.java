package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Apartment;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.DeleteApartmentView;

public class DeleteApartmentPresenter {
    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private DeleteApartmentView mDeleteApartmentView;

    public DeleteApartmentPresenter(Application mApplication, Context mContext, DeleteApartmentView mDeleteApartmentView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mDeleteApartmentView = mDeleteApartmentView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void deleteApartment(int apartmentId) {
        mMotelRepository.deleteApartment(mContext, apartmentId, new CallBackData<Apartment>() {
            @Override
            public void onSuccess(Apartment apartment) {
                mDeleteApartmentView.deleteApartmentSuccessful(apartment);
            }

            @Override
            public void onFail(String msgFail) {
                mDeleteApartmentView.deleteApartmentFail();
            }
        });
    }
}
