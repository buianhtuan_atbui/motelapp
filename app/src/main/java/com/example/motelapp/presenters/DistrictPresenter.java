package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.District;
import com.example.motelapp.models.Province;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.DistrictView;
import com.example.motelapp.views.ProvinceView;

import java.util.List;

public class DistrictPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private DistrictView mDistrictView;
    private Context mContext;
    private List<District> mDistrictList;

    public DistrictPresenter(Application mApplication, Context mContext, DistrictView mDistrictView) {
        this.mMotelRepository = new MotelRepositoryImp();
        this.mApplication = mApplication;
        this.mDistrictView = mDistrictView;
        this.mContext = mContext;
    }

    public List<District> getListDistrict(int country_id){
        mMotelRepository.getListDistrict(mContext, country_id, new CallBackData<List<District>>() {
            @Override
            public void onSuccess(List<District> districtList) {
                if(districtList != null){
                    mDistrictList = districtList;
                    mDistrictView.getListDistrictSuccess(districtList);
                }
            }

            @Override
            public void onFail(String msgFail) {

            }
        });
        return mDistrictList;
    }

}
