package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import com.example.motelapp.room.entities.User;
import com.example.motelapp.room.managements.UserManagement;
import com.example.motelapp.views.LogoutView;

public class LogoutPresenter {

    private Application mApplication;
    private Context mContext;
    private LogoutView mLogoutView;
    private UserManagement mUserManagement;

    public LogoutPresenter(Application application, Context context, LogoutView logoutView) {
        this.mApplication = application;
        this.mContext = context;
        this.mLogoutView = logoutView;
        this.mUserManagement = new UserManagement(mApplication);
    }

    //delete user in room
    public void deleteUserToRomm(){
        mUserManagement.deleteUser(new UserManagement.OnDataCallBackUser() {
            @Override
            public void onDataSuccess(User user) {
                mLogoutView.logoutSuccess();
            }

            @Override
            public void onDataFail() {
                Toast.makeText(mContext, "onDataFail (LogoutPresenter)", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

