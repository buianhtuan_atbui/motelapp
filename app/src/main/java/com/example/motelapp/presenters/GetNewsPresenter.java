package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.News;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.GetNewsView;

import java.util.List;

public class GetNewsPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private GetNewsView mGetNewsView;
    private Context mContext;

    public GetNewsPresenter(Application mApplication, GetNewsView mGetNewsView, Context mContext) {
        this.mApplication = mApplication;
        this.mGetNewsView = mGetNewsView;
        this.mContext = mContext;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void getListNews(int apartmentId) {
        mMotelRepository.getNews(mContext, apartmentId, new CallBackData<List<News>>() {
            @Override
            public void onSuccess(List<News> news) {
                mGetNewsView.getNewsSuccessful(news);
            }

            @Override
            public void onFail(String msgFail) {
                mGetNewsView.getNewsFail();
            }
        });
    }
}
