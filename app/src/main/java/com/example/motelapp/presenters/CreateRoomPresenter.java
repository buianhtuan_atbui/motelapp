package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Room;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.CreateRoomView;

public class CreateRoomPresenter {
    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private CreateRoomView mCreateRoomView;

    public CreateRoomPresenter(Application mApplication, Context mContext, CreateRoomView mCreateRoomView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mCreateRoomView = mCreateRoomView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void createRoom(Room room){
        mMotelRepository.createRoom(mContext, room, new CallBackData<Room>() {
            @Override
            public void onSuccess(Room room) {
                mCreateRoomView.createRoomSuccess(room);
            }

            @Override
            public void onFail(String msgFail) {
                mCreateRoomView.createRoomFail(msgFail);
            }
        });
    }
}
