package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Customer;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.CreateCustomerView;

public class CreateCustomerPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private CreateCustomerView mCreateCustomerView;

    public CreateCustomerPresenter(Application mApplication, Context mContext, CreateCustomerView mCreateCustomerView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mCreateCustomerView = mCreateCustomerView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void createCustomer(Customer customer) {
        mMotelRepository.createCustomer(mContext, customer, new CallBackData<Customer>() {
            @Override
            public void onSuccess(Customer customer) {
                mCreateCustomerView.createCustomerSuccessful(customer);
            }

            @Override
            public void onFail(String msgFail) {
                mCreateCustomerView.createCustomerFail();
            }
        });
    }
}
