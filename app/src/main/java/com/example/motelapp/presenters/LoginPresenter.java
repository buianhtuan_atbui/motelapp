package com.example.motelapp.presenters;

import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;

import com.example.motelapp.R;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.room.managements.UserManagement;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.LoginView;

public class LoginPresenter {

    private MotelRepository mResoRepository;
    private LoginView mLoginView;
    private Context mContext;
    private UserManagement mUserManagement;
    private Application mApplication;

    private static User IUser;

    public LoginPresenter(Application application, Context context, LoginView loginView) {
        this.mApplication = application;
        this.mContext = context;
        this.mLoginView = loginView;
        mResoRepository = new MotelRepositoryImp();
        mUserManagement = new UserManagement(mApplication);
    }

    public void login(String username, String password){
        mResoRepository.login(mContext, username, password,
                new CallBackData<User>() {
                    @Override
                    public void onSuccess(User user) {
                        if (user != null){
                            addUserToRoom(user);
                            IUser = user;
                        } else {
//                            showLoginDialog();
                        }
                    }

                    @Override
                    public void onFail(String msgFail) {
                        showLoginDialog();
//                        if (msgFail != null || msgFail.length() > 0){
//                            mLoginView.loginFail("XXXXXXXXX");
////                            showLoginDialog();
//                        } else {
//                            mLoginView.loginFail("Tải dữ liệu không thành công");
//                        }
                    }
                });
    }

    //    add user to room
    public void addUserToRoom(User user) {
        mUserManagement.addUser(user, new UserManagement.OnDataCallBackUser() {
            @Override
            public void onDataSuccess(User user) {
                mLoginView.loginSuccess(IUser);
            }

            @Override
            public void onDataFail() {
                mLoginView.loginFail("luu thong tin nguoi dung that bai");
            }
        });
    }

    //    show dialog
    public void showLoginDialog() {
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.dialog_login_fail);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button buttonOk = dialog.findViewById(R.id.btn_ok_login);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
