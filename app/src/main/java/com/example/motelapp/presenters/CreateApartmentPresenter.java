package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Apartment;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.CreateApartmentView;

public class CreateApartmentPresenter {
    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private CreateApartmentView mCreateApartmentView;

    public CreateApartmentPresenter(Application mApplication, Context mContext, CreateApartmentView mCreateApartmentView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mCreateApartmentView = mCreateApartmentView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void createApartment(Apartment apartment){
        mMotelRepository.createApartment(mContext, apartment, new CallBackData<Apartment>() {
            @Override
            public void onSuccess(Apartment apartment) {
                mCreateApartmentView.createApartmentSuccess(apartment);
            }

            @Override
            public void onFail(String msgFail) {

            }
        });
    }
}
