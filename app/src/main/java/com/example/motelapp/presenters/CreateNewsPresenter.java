package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.News;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.CreateNewsView;

public class CreateNewsPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private CreateNewsView mCreateNewsView;

    public CreateNewsPresenter(Application mApplication, Context mContext, CreateNewsView mCreateNewsView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mCreateNewsView = mCreateNewsView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void createNews(News news) {
        mMotelRepository.createNews(mContext, news, new CallBackData<News>() {
            @Override
            public void onSuccess(News news) {
                mCreateNewsView.createNewSuccessful(news);
            }

            @Override
            public void onFail(String msgFail) {
                mCreateNewsView.createNewsFail();
            }
        });
    }
}
