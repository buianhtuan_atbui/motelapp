package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Customer;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.UpdateTenantsView;

public class UpdateCustomerPresenter {
    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private UpdateTenantsView mUpdateTenantsView;

    public UpdateCustomerPresenter(Application mApplication, Context mContext, UpdateTenantsView mUpdateTenantsView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mUpdateTenantsView = mUpdateTenantsView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void updateCustomer(Customer customer){
        mMotelRepository.updateCustomer(mContext, customer, new CallBackData<Customer>() {
            @Override
            public void onSuccess(Customer customer) {
                mUpdateTenantsView.updateTenantsSuccess(customer);
            }

            @Override
            public void onFail(String msgFail) {
                mUpdateTenantsView.updateTenantsFail();
            }
        });
    }
}
