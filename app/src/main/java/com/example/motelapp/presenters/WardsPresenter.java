package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.District;
import com.example.motelapp.models.Wards;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.DistrictView;
import com.example.motelapp.views.WardsView;

import java.util.List;

public class WardsPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private WardsView mWardsView;
    private Context mContext;
    private List<Wards> mWardsList;

    public WardsPresenter(Application mApplication, Context mContext, WardsView mWardsView) {
        this.mMotelRepository = new MotelRepositoryImp();
        this.mApplication = mApplication;
        this.mWardsView = mWardsView;
        this.mContext = mContext;
    }

    public List<Wards> getListWards(int district_Id){
        mMotelRepository.getListWards(mContext, district_Id, new CallBackData<List<Wards>>() {
            @Override
            public void onSuccess(List<Wards> wardsList) {
                if(wardsList != null){
                    mWardsList = wardsList;
                    mWardsView.getListWardsSuccess(wardsList);
                }
            }

            @Override
            public void onFail(String msgFail) {

            }
        });
        return mWardsList;
    }

}
