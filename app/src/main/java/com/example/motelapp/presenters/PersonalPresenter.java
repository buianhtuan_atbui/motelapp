package com.example.motelapp.presenters;

import android.content.Context;

import com.example.motelapp.room.entities.User;
import com.example.motelapp.room.managements.UserManagement;
import com.example.motelapp.views.PersonalView;

public class PersonalPresenter {
    private Context mContext;
    private PersonalView mPersonalView;
    private UserManagement mUserManagement;

    public PersonalPresenter(Context context, PersonalView personalView) {
        this.mContext = context;
        this.mPersonalView = personalView;
        this.mUserManagement = new UserManagement(mContext);
    }

    public void getInfoPersonal(){
        mUserManagement.getmUserInfo(new UserManagement.OnDataCallBackUser() {
            @Override
            public void onDataSuccess(User user) {
                if (user != null){
                    mPersonalView.showInfoPersonal(user);
                } else {
                    mPersonalView.showInfoPersonalErr("getInfoPersonal (PersonalPresenter)");
                }
            }

            @Override
            public void onDataFail() {

            }
        });
    }
}

