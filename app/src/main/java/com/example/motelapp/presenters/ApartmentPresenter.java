package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.example.motelapp.models.Apartment;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.ApartmentView;


import java.util.ArrayList;
import java.util.List;

public class ApartmentPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private ApartmentView mApartmentView;
    private Context mContext;

    private List<Apartment> mListApartment;


    public ApartmentPresenter(Application mApplication, Context mContext, ApartmentView mApartmentView) {
        this.mApplication = mApplication;
        this.mApartmentView = mApartmentView;
        this.mContext = mContext;
        mMotelRepository = new MotelRepositoryImp();
    }


    public void getListApartment(final int userId) {
        mListApartment = new ArrayList<>();
        mMotelRepository.getListApartment(mContext, userId, new CallBackData<List<Apartment>>() {
            @Override
            public void onSuccess(List<Apartment> list) {
                mApartmentView.getListApartmentSuccess(list);
            }

            @Override
            public void onFail(String msgFail) {

                System.out.println(msgFail);
            }
        });
    }

}
