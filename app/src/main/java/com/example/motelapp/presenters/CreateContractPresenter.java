package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Contract;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.CreateContractView;

public class CreateContractPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private CreateContractView mCreateContractView;

    public CreateContractPresenter(Application mApplication, Context mContext, CreateContractView mCreateContractView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mCreateContractView = mCreateContractView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void createContract(Contract contract) {
        mMotelRepository.createContract(mContext, contract, new CallBackData<Contract>() {
            @Override
            public void onSuccess(Contract contract) {
                mCreateContractView.createContractSuccessful(contract);
            }

            @Override
            public void onFail(String msgFail) {
                mCreateContractView.createContractFail();
            }
        });
    }
}
