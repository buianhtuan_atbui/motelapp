package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Account;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.room.managements.UserManagement;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.AccountRegistrationView;

public class AccountRegistrationPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private AccountRegistrationView mAccountRegistrationView;

    public AccountRegistrationPresenter(Application mApplication, Context mContext, AccountRegistrationView mAccountRegistrationView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mAccountRegistrationView = mAccountRegistrationView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void createAccount(Account account){
        mMotelRepository.signUp(mContext, account, new CallBackData<Account>() {
            @Override
            public void onSuccess(Account account) {
                mAccountRegistrationView.createAccountSuccess();
            }

            @Override
            public void onFail(String msgFail) {
                mAccountRegistrationView.createAccountFail();
            }
        });
    }
}
