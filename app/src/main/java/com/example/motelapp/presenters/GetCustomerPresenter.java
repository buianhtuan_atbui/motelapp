package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.GetCustomerView;

import java.util.ArrayList;
import java.util.List;

public class GetCustomerPresenter {
    private MotelRepository mMotelRepository;
    private Application mApplication;
    private GetCustomerView mCustomerView;
    private Context mContext;

    private List<Customer> mCustomerList;

    public GetCustomerPresenter(Application mApplication, GetCustomerView mCustomerView, Context mContext) {
        this.mApplication = mApplication;
        this.mCustomerView = mCustomerView;
        this.mContext = mContext;
        mMotelRepository = new MotelRepositoryImp();
        mCustomerList = new ArrayList<>();
    }

    public void getListCustomer(int apartmentId) {
        mMotelRepository.getListCustomer(mContext, apartmentId, new CallBackData<List<Customer>>() {
            @Override
            public void onSuccess(List<Customer> customerList) {
                if (customerList.size() > 0) {
                    mCustomerList.addAll(customerList);
                    mCustomerView.getListCustomerSuccess(mCustomerList);
                }
            }

            @Override
            public void onFail(String msgFail) {
                mCustomerView.getListCustomerFail();
            }
        });
    }
}
