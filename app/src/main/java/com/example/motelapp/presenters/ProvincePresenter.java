package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Province;
import com.example.motelapp.models.Room;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.ProvinceView;
import com.example.motelapp.views.RoomView;

import java.util.List;

public class ProvincePresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private ProvinceView mProvinceView;
    private Context mContext;
    private List<Province> mProvinceList;

    public ProvincePresenter(Application mApplication, Context mContext, ProvinceView mProvinceView) {
        this.mMotelRepository = new MotelRepositoryImp();
        this.mApplication = mApplication;
        this.mProvinceView = mProvinceView;
        this.mContext = mContext;
    }

    public List<Province> getListProvince(){
        mMotelRepository.getListProvince(mContext, new CallBackData<List<Province>>() {
            @Override
            public void onSuccess(List<Province> provinceList) {
                if(provinceList != null){
                    mProvinceList = provinceList;
                    mProvinceView.getListProvinceSuccess(provinceList);
                }
            }

            @Override
            public void onFail(String msgFail) {

            }
        });
        return mProvinceList;
    }

}
