package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Apartment;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.UpdateApartmentView;

public class UpdateApartmentPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private UpdateApartmentView mUpdateApartmentView;

    public UpdateApartmentPresenter(Application mApplication, Context mContext, UpdateApartmentView mUpdateApartmentView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mUpdateApartmentView = mUpdateApartmentView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void updateApartment(Apartment apartment) {
        mMotelRepository.updateApartment(mContext, apartment, new CallBackData<Apartment>() {
            @Override
            public void onSuccess(Apartment apartment) {
                mUpdateApartmentView.updateApartmentSuccessful(apartment);
            }

            @Override
            public void onFail(String msgFail) {
                mUpdateApartmentView.updateApartmentFail();
            }
        });
    }
}
