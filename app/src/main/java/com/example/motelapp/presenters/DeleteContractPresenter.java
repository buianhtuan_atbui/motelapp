package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Contract;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.DeleteContractView;

public class DeleteContractPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private DeleteContractView mDeleteContractView;

    public DeleteContractPresenter(Application mApplication, Context mContext, DeleteContractView mDeleteContractView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mDeleteContractView = mDeleteContractView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void deleteContract(int contractId) {
        mMotelRepository.deleteContract(mContext, contractId, new CallBackData<Contract>() {
            @Override
            public void onSuccess(Contract contract) {
                mDeleteContractView.deleteContractSuccessful(contract);
            }

            @Override
            public void onFail(String msgFail) {
                mDeleteContractView.deleteContractFail();
            }
        });
    }
}
