package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;

import com.example.motelapp.models.Room;
import com.example.motelapp.motel.MotelRepository;
import com.example.motelapp.motel.MotelRepositoryImp;
import com.example.motelapp.utils.CallBackData;
import com.example.motelapp.views.UpdateRoomView;

public class UpdateRoomPresenter {

    private MotelRepository mMotelRepository;
    private Application mApplication;
    private Context mContext;
    private UpdateRoomView mUpdateRoomView;

    public UpdateRoomPresenter(Application mApplication, Context mContext, UpdateRoomView mUpdateRoomView) {
        this.mApplication = mApplication;
        this.mContext = mContext;
        this.mUpdateRoomView = mUpdateRoomView;
        mMotelRepository = new MotelRepositoryImp();
    }

    public void updateRoom(Room room) {
        mMotelRepository.updateRoom(mContext, room, new CallBackData<Room>() {
            @Override
            public void onSuccess(Room room) {
                mUpdateRoomView.updateRoomSuccessful(room);
            }

            @Override
            public void onFail(String msgFail) {
                mUpdateRoomView.updateRoomFail();
            }
        });
    }
}
