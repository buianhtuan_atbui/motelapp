package com.example.motelapp.presenters;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import com.example.motelapp.room.entities.User;
import com.example.motelapp.room.managements.UserManagement;
import com.example.motelapp.views.AccountRememberView;

public class AccountRememberPresenter {

    private Application mApplication;
    private Context mContext;
    private AccountRememberView mAccountRememberView;
    private UserManagement mUserManagement;

    public AccountRememberPresenter(Application application, Context context, AccountRememberView accountRememberView) {
        this.mApplication = application;
        this.mContext = context;
        this.mAccountRememberView = accountRememberView;
        this.mUserManagement = new UserManagement(mApplication);
    }

    public void AccountRemember(){
        mUserManagement.selectUser(new UserManagement.OnDataCallBackUser() {
            @Override
            public void onDataSuccess(User user) {
                mAccountRememberView.accountIsExist(user);
            }

            @Override
            public void onDataFail() {
                mAccountRememberView.accountNoExist();
                Toast.makeText(mApplication, "Chua co account", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

