package com.example.motelapp.room.databases;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.motelapp.room.daos.UserDao;
import com.example.motelapp.room.entities.User;

import static com.example.motelapp.room.databases.MotelDatabase.DATABASE_VERSION;

@Database(entities = {User.class}, exportSchema = false, version = DATABASE_VERSION)
public abstract class MotelDatabase extends RoomDatabase {

    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "motel_database";
    private static MotelDatabase INSTANCE;
    public abstract UserDao userDao();

    public static MotelDatabase getDatabase(Context context) {
        if(INSTANCE == null) {
            synchronized (MotelDatabase.class) {
                if(INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MotelDatabase.class, DATABASE_NAME).build();
                }
            }
        }
        return INSTANCE;
    }
}
