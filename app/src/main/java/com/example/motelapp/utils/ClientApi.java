package com.example.motelapp.utils;

import com.example.motelapp.motel.MotelService;

public class ClientApi extends BaseApi {

    public MotelService motelService(){
        return this.getService(MotelService.class, ConfigApi.BASE_URL);
    }

}
