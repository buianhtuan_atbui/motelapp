package com.example.motelapp.utils;

public class ConfigApi {

    public static final String BASE_URL = "http://motel.upod.vn/api/";

    public interface Api {
        String LOGIN = "Account/Login";

        String SIGNUP = "Account";
        String CHANGE_PASSWORD = "Account";


        String GET_APARTMENT = "Apartment";
        String CREATE_APARTMENT = "Apartment";
        String UPDATE_APARTMENT = "Apartment";
        String DELETE_APARTMENT = "Apartment";


        String GET_ROOM = "Room";
        String CREATE_ROOM = "Room";
        String UPDATE_ROOM = "Room";

        String GET_CONTRACT = "Contract";
        String CREATE_CONTRACT = "Contract";
        String UPDATE_CONTRACT = "Contract";
        String DELETE_CONTRACT = "Contract";

        String GET_PROVINCE = "Country";
        String GET_DISTRICT = "District";
        String GET_WARDS = "Ward";


        String GET_CUSTOMER = "Customer";
        String CREATE_CUSTOMER = "Customer";
        String UPDATE_CUSTOMER = "Customer";
        String DELETE_CUSTOMER = "Customer/listCustomer";

        String CREATE_NEWS = "News";
        String GET_NEWS = "News";

        String CREATE_BILL = "Bills";

        //        String UPLOAD_IMAGE = "Image";
        String UPLOAD_IMAGE = "UploadFile";
    }
}
