package com.example.motelapp.utils;

public interface CallBackData<T> {
    void onSuccess(T t);
    void onFail(String msgFail);
}
