package com.example.motelapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.motelapp.R;
import com.example.motelapp.activities.CreateRoomActivity;
import com.example.motelapp.activities.RoomActivity;
import com.example.motelapp.activities.RoomBlankOptionActivity;
import com.example.motelapp.adapters.RoomAdapter;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.Room;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class RoomFragment extends Fragment implements View.OnClickListener {

    private FloatingActionButton mFabAddNewRoom;
    private View mView;
    private RecyclerView mRclRoom;
    private List<Room> mListRoom;
    private List<Contract> mListContract;
    private RoomAdapter mRoomAdapter;
    private List<Apartment> mApartmentList;
    private List<Customer> mCustomerList;
    private int index;
    private TextView txt_apartment_name;


    public RoomFragment(List<Room> mListRoom, List<Contract> mListContract, List<Apartment> mApartmentList, int index, List<Customer> mCustomerList) {
        this.mListRoom = mListRoom;
        this.mListContract = mListContract;
        this.mApartmentList = mApartmentList;
        this.index = index;
        this.mCustomerList = mCustomerList;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_room, container, false);

        initialView();
        initialData();

        return mView;
    }

    private void initialView() {
        mFabAddNewRoom = mView.findViewById(R.id.fab_add_new_room);
        mRclRoom = mView.findViewById(R.id.recycle_room);
        mRclRoom.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRclRoom.setLayoutManager(layoutManager);
        txt_apartment_name = mView.findViewById(R.id.txt_apartment_name);
        txt_apartment_name.setText(mApartmentList.get(index).getName());
    }

    private void initialData() {
        mFabAddNewRoom.setOnClickListener(this);
        updateUI();
    }

    public void updateUI() {
        if (mRoomAdapter == null) {
            mRoomAdapter = new RoomAdapter(getContext(), mListRoom, mListContract);
            mRclRoom.setAdapter(mRoomAdapter);

        } else {
            mRoomAdapter.notifyDataSetChanged();
        }

        //on click
        mRoomAdapter.getPosition(new RoomAdapter.OnClickListener() {
            @Override
            public void onClickListener(int position) {
                if (mListRoom.get(position).isActive()){
                    Toast.makeText(getContext(), "pos room " + position, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity().getApplication(), RoomBlankOptionActivity.class);
                    intent.putExtra("LIST_ROOM", (ArrayList<Room>) mListRoom);
                    intent.putExtra("POSITION", position);
                    intent.putExtra("LIST_CONTRACT",(ArrayList<Contract>) mListContract);
                    startActivity(intent);
                } else {
                    Toast.makeText(getContext(), "pos room " + position, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity().getApplication(), RoomActivity.class);
                    intent.putExtra("LIST_ROOM", (ArrayList<Room>) mListRoom);
                    intent.putExtra("POSITION", position);
                    intent.putExtra("LIST_CONTRACT",(ArrayList<Contract>) mListContract);
                    intent.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.fab_add_new_room:
                Intent intent = new Intent(getActivity().getApplication(), CreateRoomActivity.class);
                intent.putExtra("APARTMENT", mApartmentList.get(index));
                startActivity(intent);
//                startActivityForResult(intent, 2);
                break;
        }
    }
}
