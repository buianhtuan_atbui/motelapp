package com.example.motelapp.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.motelapp.R;
import com.example.motelapp.activities.ChangePasswordActivity;
import com.example.motelapp.activities.LoginActivity;
import com.example.motelapp.networks.CheckNetwork;
import com.example.motelapp.presenters.LogoutPresenter;
import com.example.motelapp.presenters.PersonalPresenter;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.views.LogoutView;
import com.example.motelapp.views.PersonalView;

public class SettingFragment extends Fragment implements View.OnClickListener, LogoutView, PersonalView {

    private View mView;
    private Button mBtnLogout, mBtnChangePass;
    private LogoutPresenter mLogoutPresenter;
    private PersonalPresenter mPersonalPresenter;
    private ImageView mImgPictureUser;
    private TextView mTxtAccountName, mTxtAccountEmail, mTxtPhoneNumber, mTxtEmailAddress;
    private LinearLayout mLnlPhoneCall, mLnlEmailSend;

    private User mUser;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_setting, container, false);

        initialView();
        initialData();

        return mView;
    }

    public void initialView() {

        mImgPictureUser = mView.findViewById(R.id.img_picture_user);
        mTxtAccountName = mView.findViewById(R.id.txt_account_name);
        mTxtAccountEmail = mView.findViewById(R.id.txt_account_email);
        mLnlPhoneCall = mView.findViewById(R.id.lnl_phone_call);
        mLnlEmailSend = mView.findViewById(R.id.lnl_email_send);
        mTxtPhoneNumber = mView.findViewById(R.id.txt_phone_number);
        mTxtEmailAddress = mView.findViewById(R.id.txt_email_address);

        mBtnLogout = mView.findViewById(R.id.btn_logout);
        mBtnChangePass = mView.findViewById(R.id.btn_change_pass);
    }

    public void initialData() {

        mBtnLogout.setOnClickListener(this);
        mBtnChangePass.setOnClickListener(this);
        mLogoutPresenter = new LogoutPresenter(getActivity().getApplication(), getActivity(), this);
        //account personal info
        mPersonalPresenter = new PersonalPresenter(getActivity().getApplication(), this);
        mPersonalPresenter.getInfoPersonal();

        //Call phone
        mLnlPhoneCall.setOnClickListener(this);
        //send mail
        mLnlEmailSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.btn_logout:
                showDialogLogout();
                break;

            case R.id.btn_change_pass:
                Intent intentToChangePass = new Intent(getActivity().getApplication(), ChangePasswordActivity.class);
                intentToChangePass.putExtra("USER", mUser);
                startActivity(intentToChangePass);
                break;

            case R.id.lnl_phone_call:
                Intent intentCallPhone = new Intent(Intent.ACTION_DIAL);
                try {
                    String phoneNumber = "tel:" + mTxtPhoneNumber.getText().toString().trim();
                    intentCallPhone.setData(Uri.parse(phoneNumber));
                    startActivity(intentCallPhone);
                } catch (Exception ex){
                    String phoneErr = getString(R.string.phone_number_error);
                    Toast.makeText(mView.getContext(), phoneErr, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.lnl_email_send:
                Intent intentSendMail = new Intent(Intent.ACTION_SEND);
                try {
                    String emailName = "mailto:" + mTxtEmailAddress.getText().toString().trim();
                    intentSendMail.setData(Uri.parse(emailName));
                    intentSendMail.setType("text/plain");
                    startActivity(intentSendMail);
                } catch (Exception ex) {
                    String emailErr = getString(R.string.email_name_error);
                    Toast.makeText(mView.getContext(), emailErr, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void showDialogLogout(){
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_logout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button btnLogoutOk = dialog.findViewById(R.id.btn_logout_ok);
        Button btnLogoutNo = dialog.findViewById(R.id.btn_logout_no);
        btnLogoutOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getContext(), "show logout ok", Toast.LENGTH_SHORT).show();
                logoutApp();
                dialog.dismiss();
            }
        });
        btnLogoutNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    //logout
    public void logoutApp(){
        if (CheckNetwork.isInternetAvailable(getActivity())){
            mLogoutPresenter.deleteUserToRomm();
        } else {
            String disconnect = getString(R.string.internet_disconnect);
            Toast.makeText(getActivity(), disconnect, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void logoutSuccess() {
        Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
        intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intentLogin);
        getActivity().finish();
    }

    @Override
    public void logoutFail(String msgLogouFail) {
        Toast.makeText(getContext(), "debug di", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showInfoPersonal(User user) {

        mTxtAccountName.setText("Xin chào: " + user.getFull_name());
        mTxtAccountEmail.setText(user.getEmail());
        mUser = user;
    }

    @Override
    public void showInfoPersonalErr(String msg) {

    }
}
