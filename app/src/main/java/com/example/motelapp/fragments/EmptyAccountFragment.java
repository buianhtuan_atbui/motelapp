package com.example.motelapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.motelapp.R;
import com.example.motelapp.activities.CreateApartmentActivity;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.room.managements.UserManagement;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class EmptyAccountFragment extends Fragment implements View.OnClickListener {

    private View mView;
    private FloatingActionButton mFabNewCreateApartment;
    private UserManagement mUserManagement;
    private User user;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_empty_account, container, false);

        initialView();
        initialData();

        return mView;
    }

    private void initialView() {
        mFabNewCreateApartment = mView.findViewById(R.id.fab_new_create_apartment);
    }

    private void initialData() {
        mFabNewCreateApartment.setOnClickListener(this);
        mUserManagement = new UserManagement(getActivity().getApplication());
        mUserManagement.getmUserInfo(new UserManagement.OnDataCallBackUser() {
            @Override
            public void onDataSuccess(User IUser) {
                user = IUser;
            }

            @Override
            public void onDataFail() {

            }
        });
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.fab_new_create_apartment:

                Intent intentCreateApartment = new Intent(getContext().getApplicationContext(), CreateApartmentActivity.class);
                intentCreateApartment.putExtra("USER", user);
                startActivity(intentCreateApartment);
                break;
        }

    }
}
