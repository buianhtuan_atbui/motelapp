package com.example.motelapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.motelapp.R;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Room;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class OverviewFragment extends Fragment {

    private View mView;

    private List<Apartment> apartmentList;
    private List<Room> roomList;
    private List<Contract> contractList;
    private int index = 0;

    private int count = 0;
    private float deposit = 0;

    private int checkExpirationIn30Day;
    private int checkExpirationIn60Day;
    private int checkExpirationIn90Day;

    TextView mTxtApartmentName, mTxtEmptyRoom, mTxtTotalAmount, mTxtTotalPay, mTxtFinalAmount, mTxtDeposit, mTxtExpirationIn30Day, mTxtExpirationIn60Day, mTxtExpirationIn90Day;

    public OverviewFragment(List<Apartment> apartmentList, List<Room> roomList, List<Contract> contractList, int index) {
        this.apartmentList = apartmentList;
        this.roomList = roomList;
        this.contractList = contractList;
        this.index = index;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_overview, container, false);

        initialView();
        initialData();

        return mView;
    }

    private void initialView() {
        mTxtApartmentName = (TextView) mView.findViewById(R.id.txt_apartment_name_ov);
        mTxtEmptyRoom = (TextView) mView.findViewById(R.id.txt_empty_room_ov);
        mTxtTotalAmount = (TextView) mView.findViewById(R.id.txt_total_amout);
        mTxtTotalPay = (TextView) mView.findViewById(R.id.txt_total_pay);
        mTxtFinalAmount = (TextView) mView.findViewById(R.id.txt_final_amount);
        mTxtDeposit = mView.findViewById(R.id.txt_deposit);
        mTxtExpirationIn30Day = (TextView) mView.findViewById(R.id.txt_expiration_in_30_day);
        mTxtExpirationIn60Day = (TextView) mView.findViewById(R.id.txt_expiration_in_60_day);
        mTxtExpirationIn90Day =(TextView) mView.findViewById(R.id.txt_expiration_in_90_day);
    }

    private void initialData() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = format.format( new Date() );
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        //getTime() returns the current date in default time zone
        Date date = calendar.getTime();
        int day = calendar.get(Calendar.DATE);
        //Note: +1 the month for current month
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        if (apartmentList.size() > 0){
            Apartment apartment = apartmentList.get(index);
            String apartmentName = apartment.getName();
            mTxtApartmentName.setText(apartmentName);
            if (roomList.size() > 0){
                for (Room room:roomList
                ) {
                    if (room.isActive()){
                        count++;
                    }
                }
            }
            if (contractList.size() > 0){
                for (Contract contract: contractList
                     ) {
                    if (contract.isActive()){

                        try {
                            deposit+=contract.getDeposit();
                            Date dateContract = format.parse(contract.getEndDate());
                            if (month == dateContract.getMonth() && dateContract.getDay() - day < 30){
                                checkExpirationIn30Day++;
                            }
                            if (month - dateContract.getMonth() == 1 && dateContract.getDay() - day < 30){
                                checkExpirationIn60Day++;
                            }
                            if (month - dateContract.getMonth() == 2 && dateContract.getDay() - day < 30){
                                checkExpirationIn60Day++;
                            }
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }
                }
            }
        }
        mTxtEmptyRoom.setText(String.valueOf(count));
        mTxtDeposit.setText(String.valueOf(deposit));
        mTxtExpirationIn90Day.setText(String.valueOf(checkExpirationIn90Day));
        mTxtExpirationIn60Day.setText(String.valueOf(checkExpirationIn60Day));
        mTxtExpirationIn30Day.setText(String.valueOf(checkExpirationIn30Day));

    }
}
