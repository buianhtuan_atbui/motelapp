package com.example.motelapp.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.motelapp.R;
import com.example.motelapp.activities.ChangePasswordActivity;
import com.example.motelapp.activities.LoginActivity;
import com.example.motelapp.activities.RoomBlankActivity;
import com.example.motelapp.adapters.NewsAdapter;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.models.News;
import com.example.motelapp.models.Room;
import com.example.motelapp.networks.CheckNetwork;
import com.example.motelapp.presenters.LogoutPresenter;
import com.example.motelapp.presenters.PersonalPresenter;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.views.LogoutView;
import com.example.motelapp.views.PersonalView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class NewsFragment extends Fragment implements View.OnClickListener {

    private View mView;
    private FloatingActionButton mFabAddNewNews;
    private RecyclerView mRclNews;

    private NewsAdapter mNewsAdapter;

    private List<Room> mListRoom;
    private int index;
    private List<Apartment> mApartmentList;
    private List<News> mNewsList;

    public NewsFragment(List<Room> mListRoom, int index, List<Apartment> mApartmentList, List<News> mNewsList) {
        this.mListRoom = mListRoom;
        this.index = index;
        this.mApartmentList = mApartmentList;
        this.mNewsList = mNewsList;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_news, container, false);

        initialView();
        initialData();

        return mView;
    }

    public void initialView() {
        mFabAddNewNews = mView.findViewById(R.id.fab_add_news);
        mRclNews = mView.findViewById(R.id.recycle_news);
        mRclNews.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRclNews.setLayoutManager(layoutManager);
    }

    public void initialData() {
        mFabAddNewNews.setOnClickListener(this);
        updateUI(mNewsList);

    }

    public void updateUI(List<News> newsList) {
        if (mNewsAdapter == null) {
            mNewsAdapter = new NewsAdapter(getContext(), newsList);
            mRclNews.setAdapter(mNewsAdapter);
        } else {
            mNewsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.fab_add_news:
                Intent intent = new Intent(getActivity().getApplication(), RoomBlankActivity.class);
                intent.putExtra("CHECK", true);
                intent.putExtra("LIST_ROOM", (ArrayList<Room>) mListRoom);
                intent.putExtra("LIST_APARTMENT", (ArrayList<Apartment>) mApartmentList);
                startActivity(intent);
                break;
        }
    }


}
