package com.example.motelapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.motelapp.R;
import com.example.motelapp.activities.CreateContractActivity;
import com.example.motelapp.activities.RoomBlankActivity;
import com.example.motelapp.activities.TenantsActivity;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.Room;

import java.util.ArrayList;
import java.util.List;

public class ApartmentFragment extends Fragment implements View.OnClickListener {

    private LinearLayout mLnlTenants, mLnlRoomBlank, mLnlCreateContract;
    private List<Apartment> apartmentList;
    private List<Room> roomList;
    private List<Contract> contractList;
    private List<Customer> customerList;

    private Apartment apartment;
    int numberOfCustomer;
    private View mView;

    private int countRoom = 0;
    private int countCustomer = 0;
    private int index = 0;

    public ApartmentFragment(List<Apartment> apartmentList, List<Room> roomList, List<Contract> contractList, int index, List<Customer> customerList) {
        this.apartmentList = apartmentList;
        this.roomList = roomList;
        this.contractList = contractList;
        this.index = index;
        this.customerList = customerList;
    }

    TextView txt_name_of_apartment;
    TextView txt_number_of_room;
    TextView txt_number_of_customer;
    TextView txt_empty_room;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_apartment, container, false);

        txt_name_of_apartment = (TextView) mView.findViewById(R.id.txt_apartment_name);
        txt_number_of_room = (TextView) mView.findViewById(R.id.txt_number_of_room);
        txt_number_of_customer = (TextView) mView.findViewById(R.id.txt_number_of_customer);
        txt_empty_room = (TextView) mView.findViewById(R.id.txt_empty_room);
        mLnlTenants = (LinearLayout) mView.findViewById(R.id.lnl_tenants);
        mLnlRoomBlank = (LinearLayout) mView.findViewById(R.id.lnl_room_blank);
        mLnlCreateContract = (LinearLayout) mView.findViewById(R.id.lnl_btn_create_contract);
        initialData();
        return mView;
    }

    private void initialData() {

        mLnlTenants.setOnClickListener(this);
        mLnlRoomBlank.setOnClickListener(this);
        mLnlCreateContract.setOnClickListener(this);

        apartment = new Apartment();
        apartment = apartmentList.get(index);
        txt_name_of_apartment.setText(apartment.getName());
        String numOfRoom = String.valueOf(roomList.size());
        txt_number_of_room.setText(numOfRoom);
        for (Room room : roomList) {
            if (room.isActive())
                countRoom++;
        }
        txt_empty_room.setText(String.valueOf(countRoom));

       for (int i = 0; i < contractList.size(); i++){
           if (!contractList.get(i).isActive()){
               contractList.remove(i);
               i = i-1;
           }
       }

        if (contractList.size() > 0) {
            for (Contract contract : contractList) {
                countCustomer += contract.getNumberOfCustomer();
            }
        }
        txt_number_of_customer.setText(String.valueOf(countCustomer));
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_tenants:
//                Intent intentTenants = new Intent(getActivity().getApplication(), TenantsActivity.class);
//                intentTenants.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) customerList);
//                startActivity(intentTenants);
//                break;
                Intent intent = new Intent(getActivity().getApplication(), TenantsActivity.class);
                intent.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) customerList);
                startActivityForResult(intent, 1);
                break;

            case R.id.lnl_room_blank:

                //xet dk cho cai roomblank nha
                //neu co phongf trong intent qua roomblankactivity
                //khg co phong trong thi TOAST len la dc, t lam cai dialog show len sau
                Intent intentToRoomBlank = new Intent(getActivity().getApplication(), RoomBlankActivity.class);
                intentToRoomBlank.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                startActivity(intentToRoomBlank);
                break;

            case R.id.lnl_btn_create_contract:
                //giong cai tphong trong
                // neu co phong trong moi cho tao hop dong
                //khong thi cu TOAST len roi t tao cai dialog cho nay
                Intent intentCreateContract = new Intent(getActivity().getApplication(), RoomBlankActivity.class);
                intentCreateContract.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                startActivity(intentCreateContract);
                break;
        }
    }
}
