package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.example.motelapp.R;
import com.example.motelapp.models.District;
import com.example.motelapp.models.Province;
import com.example.motelapp.models.Wards;
import com.example.motelapp.presenters.ProvincePresenter;
import com.example.motelapp.views.AccountRememberView;
import com.example.motelapp.models.Account;
import com.example.motelapp.presenters.AccountRegistrationPresenter;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.room.managements.UserManagement;
import com.example.motelapp.views.AccountRegistrationView;


public class AccountRegistrationActivity extends AppCompatActivity implements View.OnClickListener, AccountRegistrationView {

    private LinearLayout mLnlItemBack, mLnlChoiceGender, mLnlProvince, mLnlDistrict, mLnlWards;
    private TextView mTxtGenderShow, mTxtProvince, mTxtDistrict, mTxtWards;
    private EditText mEdtFirstName, mEdtLastName, mEdtPhoneNumber, mEdtEmail, mEdtPassword, mEdtComfirmPassword;
    private Button mBtnRegisterAccount;

    private AwesomeValidation awesomeValidation;

    private AccountRegistrationPresenter mAccountRegistrationPresenter;
    private static Province province;
    private static District district;
    private static Wards wards;

    private static String phone = "";
    private static String password = "";
    private static String gender = "";
    private static String confirmPassword = "";
    private static String firstName = "";
    private static String lastName = "";
    private static String email = "";
    private static String sex = "";
    private static String YOB = "";
    private static String address = "";
    private static boolean active = true;
    private static String image = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_registration);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mLnlChoiceGender = findViewById(R.id.lnl_choice_gender);
        mTxtGenderShow = findViewById(R.id.txt_gender_show);
        mEdtFirstName = findViewById(R.id.edt_first_name);
        mEdtLastName = findViewById(R.id.edt_last_name);
        mEdtPhoneNumber = findViewById(R.id.edt_phone_number);
        mEdtEmail = findViewById(R.id.edt_email);
        mEdtPassword = findViewById(R.id.edt_password);
        mEdtComfirmPassword = findViewById(R.id.edt_confirm_password);
        mBtnRegisterAccount = findViewById(R.id.btn_register_account);
        mLnlProvince = findViewById(R.id.lnl_province);
        mLnlDistrict = findViewById(R.id.lnl_district);
        mLnlWards = findViewById(R.id.lnl_wards);
        mTxtProvince = findViewById(R.id.txt_province);
        mTxtDistrict = findViewById(R.id.txt_district);
        mTxtWards = findViewById(R.id.txt_wards);


    }

    private void initialData() {
        mAccountRegistrationPresenter = new AccountRegistrationPresenter(getApplication(), this, this);
        mLnlItemBack.setOnClickListener(this);
        mLnlChoiceGender.setOnClickListener(this);
        mBtnRegisterAccount.setOnClickListener(this);
        mLnlProvince.setOnClickListener(this);
        mLnlDistrict.setOnClickListener(this);
        mLnlWards.setOnClickListener(this);
        province = (Province) getIntent().getSerializableExtra("PROVINCE");
        district = (District) getIntent().getSerializableExtra("DISTRICT");
        wards = (Wards) getIntent().getSerializableExtra("WARDS");
        String provinceName = (province != null) ? province.getName() : "";
        String districtName = (district != null) ? district.getName() : "";
        String wardsName = (wards != null) ? wards.getName() : "";
        mTxtProvince.setText(provinceName);
        mTxtDistrict.setText(districtName);
        mTxtWards.setText(wardsName);

        mEdtPhoneNumber.setText((phone.trim().length() > 0) ? phone : "");
        mTxtGenderShow.setText((gender.trim().length() > 0) ? gender : "");
        mEdtPassword.setText((password.trim().length() > 0) ? password : "");
        mEdtComfirmPassword.setText((confirmPassword.trim().length() > 0) ? confirmPassword : "");
        mEdtEmail.setText((email.trim().length() > 0) ? email : "");
        mEdtLastName.setText((lastName.trim().length() > 0) ? lastName : "");
        mEdtFirstName.setText((firstName.trim().length() > 0) ? firstName : "");

        if(province != null) {
                mBtnRegisterAccount.requestFocus();
        }
    }

    public void setData() {
        phone = mEdtPhoneNumber.getText().toString();
        gender = mTxtGenderShow.getText().toString();
        password = mEdtPassword.getText().toString();
        confirmPassword = mEdtComfirmPassword.getText().toString();
        lastName = mEdtLastName.getText().toString();
        firstName = mEdtFirstName.getText().toString();
        email = mEdtEmail.getText().toString();
        sex = mTxtGenderShow.getText().toString();
        YOB = "1997-08-02";
        address = mTxtWards.getText().toString() + ", " + mTxtDistrict.getText().toString() + ", " + mTxtProvince.getText().toString();
        active = true;
        image = "";
    }


    public void createAccount() {

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.edt_first_name, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_last_name, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_email, Patterns.EMAIL_ADDRESS, R.string.emailerror);
        awesomeValidation.addValidation(this, R.id.edt_password, RegexTemplate.NOT_EMPTY, R.string.passworderror);
        awesomeValidation.addValidation(this, R.id.edt_phone_number, Patterns.PHONE, R.string.mobileerror);

        if (awesomeValidation.validate()) {
            if (password.equals(confirmPassword)) {
                String fullName = firstName + lastName;
                Account account = new Account(password, email, fullName, phone, address, YOB, sex, image, active);
                mAccountRegistrationPresenter.createAccount(account);
            } else {
                awesomeValidation.addValidation(this, R.id.edt_confirm_password, confirmPassword, R.string.comfirm_password_not_match);
            }
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
//                chọn giới tính
            case R.id.lnl_choice_gender:
                showGenderDialog();
                break;
//                chọn thành phố - tỉnh
            case R.id.lnl_province:
                Intent intentProvince = new Intent(AccountRegistrationActivity.this, ProvinceActivity.class);
                setData();
                startActivity(intentProvince);
                break;
//                chọn quận - huyện
            case R.id.lnl_district:
                if (mTxtProvince.getText().toString().equals("")) {
                    Toast.makeText(this, "vui long chon thanh pho", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intentDistrict = new Intent(AccountRegistrationActivity.this, DistrictActivity.class);
                    intentDistrict.putExtra("PROVINCE", province);
                    setData();
                    startActivity(intentDistrict);
                }
                break;
//                chọn phường - xã
            case R.id.lnl_wards:
                if (mTxtDistrict.getText().toString().equals("")) {
                    Toast.makeText(this, "Vui Long chon tinh va huyen", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intentWards = new Intent(AccountRegistrationActivity.this, WardsActivity.class);
                    intentWards.putExtra("DISTRICT", district);
                    intentWards.putExtra("PROVINCE", province);
                    setData();
                    startActivity(intentWards);
                }
                break;
            case R.id.btn_register_account:
                createAccount();
                setData();
                break;
        }
    }

    private void showGenderDialog() {
        final Dialog dialog = new Dialog(AccountRegistrationActivity.this);
        dialog.setContentView(R.layout.dialog_gender);
//        dialog.setTitle("Chọn giới tính");
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.YELLOW));
        //gender
        TextView mTxtNam, mTxtNu, mTxtKhac;

        mTxtNam = dialog.findViewById(R.id.txt_nam);
        mTxtNu = dialog.findViewById(R.id.txt_nu);
        mTxtKhac = dialog.findViewById(R.id.txt_khac);

        //gender onclick
        mTxtNam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtGenderShow.setText("Nam");
                dialog.dismiss();
            }
        });
        mTxtNu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtGenderShow.setText("Nữ");
                dialog.dismiss();
            }
        });
        mTxtKhac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtGenderShow.setText("Khác");
                dialog.dismiss();
            }
        });

        dialog.show();

    }


    @Override
    public void createAccountSuccess() {
        Toast.makeText(this, "Sign Up Successfully", Toast.LENGTH_SHORT).show();
        Intent intentMainHome = new Intent(AccountRegistrationActivity.this, LoginActivity.class);
        startActivity(intentMainHome);
        finish();
    }

    @Override
    public void createAccountFail() {

    }
}
