package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.example.motelapp.R;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.Room;
import com.example.motelapp.presenters.DeleteContractPresenter;
import com.example.motelapp.presenters.DeleteCustomerPresenter;
import com.example.motelapp.presenters.UpdateContractPresenter;
import com.example.motelapp.presenters.UpdateRoomPresenter;
import com.example.motelapp.views.DeleteContractView;
import com.example.motelapp.views.DeleteCustomerView;
import com.example.motelapp.views.UpdateContractView;
import com.example.motelapp.views.UpdateRoomView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class UpdateContractActivity extends AppCompatActivity implements View.OnClickListener, UpdateContractView, DeleteContractView, DeleteCustomerView, UpdateRoomView {

    private LinearLayout mLnlItemBack;

    private LinearLayout mLnlDateStart, mLnlDateEnd, mLnlRoomPayCycle;
    private TextView mTxtDateStart, mTxtDateEnd, mTxtRoomName, mTxtPayCycle;
    private EditText mEdtContractCode, mEdtDeposit, mEdtCurrentElectricity, mEdtCurrentWater,
            mEdtNumberOfBike, mEdtNumberOfCustomer;
    private Button mBtnUpdateContract, mBtnLiquidation;
    private TextView mTxtFocus;

    private int position;
    private List<Room> mRoomList;
    private List<Contract> mContractList;
    private List<Customer> mCustomerList;

    private static String startDate = "";
    private static String endDate = "";
    private static int deposit = 0;
    private static int paymentCycle = 1;
    private static int paymentDay = 1;
    private static String contractCode = "";
    private static String presentator = "";
    private static int numberOfCustomer = 0;
    private static int numberOfBike = 0;
    private static float currentElectricity = 0;
    private static float currentWater = 0;

    private AwesomeValidation awesomeValidation;

    private UpdateContractPresenter mUpdateContractPresenter;
    private DeleteContractPresenter mDeleteContractPresenter;
    private DeleteCustomerPresenter mDeleteCustomerPresenter;
    private UpdateRoomPresenter mUpdateRoomPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_contract);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mLnlDateStart = findViewById(R.id.lnl_date_start);
        mLnlDateEnd = findViewById(R.id.lnl_date_end);
        mTxtDateStart = findViewById(R.id.txt_date_start);
        mTxtDateEnd = findViewById(R.id.txt_date_end);
        mTxtRoomName = findViewById(R.id.txt_room_name);
        mEdtDeposit = findViewById(R.id.edt_deposit);
        mEdtCurrentElectricity = findViewById(R.id.edt_current_electricity);
        mEdtCurrentWater = findViewById(R.id.edt_current_water);
        mEdtNumberOfBike = findViewById(R.id.edt_number_of_bike);
        mEdtNumberOfCustomer = findViewById(R.id.edt_numer_of_customer);
        mLnlRoomPayCycle = findViewById(R.id.lnl_room_pay_cycle);
        mTxtPayCycle = findViewById(R.id.txt_pay_cycle);
        mEdtContractCode = findViewById(R.id.edt_contract_code);

        mBtnUpdateContract = findViewById(R.id.btn_update_contract);
        mBtnLiquidation = findViewById(R.id.btn_delete_contract);
    }

    private void initialData() {
        mLnlItemBack.setOnClickListener(this);
        mBtnUpdateContract.setOnClickListener(this);
        mBtnLiquidation.setOnClickListener(this);
        mLnlDateStart.setOnClickListener(this);
        mLnlDateEnd.setOnClickListener(this);
        mLnlRoomPayCycle.setOnClickListener(this);

        mUpdateContractPresenter = new UpdateContractPresenter(getApplication(), this, this);
        mDeleteContractPresenter = new DeleteContractPresenter(getApplication(), this, this);
        mDeleteCustomerPresenter = new DeleteCustomerPresenter(getApplication(), this, this);
        mUpdateRoomPresenter = new UpdateRoomPresenter(getApplication(), this, this);

        mRoomList = (ArrayList<Room>) getIntent().getSerializableExtra("LIST_ROOM");
        position = getIntent().getIntExtra("POSITION", position);
        mContractList = (ArrayList<Contract>) getIntent().getSerializableExtra("LIST_CONTRACT");
        mCustomerList = (ArrayList<Customer>) getIntent().getSerializableExtra("LIST_CUSTOMER");

        mTxtDateStart.setText(mContractList.get(0).getStartDate());
        mTxtDateEnd.setText(mContractList.get(0).getEndDate());
        mTxtPayCycle.setText(String.valueOf(mContractList.get(0).getPayment_cycle()));
        mTxtRoomName.setText(mRoomList.get(position).getName());
        mEdtContractCode.setText(mContractList.get(0).getContractCode());
        mEdtCurrentElectricity.setText(String.valueOf(mContractList.get(0).getCurrentElectricity()));
        mEdtCurrentWater.setText(String.valueOf(mContractList.get(0).getCurrentWater()));
        mEdtDeposit.setText(String.valueOf(mContractList.get(0).getDeposit()));
        mEdtNumberOfBike.setText(String.valueOf(mContractList.get(0).getNumberOfBike()));
        mEdtNumberOfCustomer.setText(String.valueOf(mContractList.get(0).getNumberOfCustomer()));

    }

    public void showDialogDateStart() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog pickerDialog = new DatePickerDialog(UpdateContractActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String dateStart = year + "/" + month + "/" + day;
                mTxtDateStart.setText(dateStart);
            }
        },
                year, month, day);
        pickerDialog.show();
    }

    //date end
    public void showDialogDateEnd() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog pickerDialog = new DatePickerDialog(UpdateContractActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String dateStart = year + "/" + month + "/" + day;
                mTxtDateEnd.setText(dateStart);
            }
        },
                year, month, day);
        pickerDialog.show();
    }

    private void showPayCycleDialog() {
        final Dialog dialog = new Dialog(UpdateContractActivity.this);
        dialog.setContentView(R.layout.dialog_pay_cycle);
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        TextView mTxt1Month, mTxt2Month, mTxt3Month;

        mTxt1Month = dialog.findViewById(R.id.txt_1_month);
        mTxt2Month = dialog.findViewById(R.id.txt_2_month);
        mTxt3Month = dialog.findViewById(R.id.txt_3_month);

        //gender onclick
        mTxt1Month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtPayCycle.setText("1");
                dialog.dismiss();
            }
        });
        mTxt2Month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtPayCycle.setText("2");
                dialog.dismiss();
            }
        });
        mTxt3Month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtPayCycle.setText("3");
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void setDataContract() {
        startDate = mTxtDateStart.getText().toString();
        endDate = mTxtDateEnd.getText().toString();
        deposit = (mEdtDeposit.getText().toString().trim().length() > 0) ? Integer.parseInt(mEdtDeposit.getText().toString()) : 0;
        paymentCycle = (mTxtPayCycle.getText().toString().trim().length() > 0) ? Integer.parseInt(mTxtPayCycle.getText().toString()) : 0;
        paymentDay = 1;
        contractCode = !(mEdtContractCode.getText().toString().trim().length() > 0) ? mEdtContractCode.getText().toString() : "";
        presentator = mContractList.get(0).getRepresentator();
        numberOfCustomer = (mEdtNumberOfCustomer.getText().toString().trim().length() > 0) ? Integer.parseInt(mEdtNumberOfCustomer.getText().toString()) : 0;
        numberOfBike = (mEdtNumberOfBike.getText().toString().trim().length() > 0) ? Integer.parseInt(mEdtNumberOfBike.getText().toString()) : 0;
        currentElectricity = (mEdtCurrentElectricity.getText().toString().trim().length() > 0) ? Float.parseFloat(mEdtCurrentElectricity.getText().toString()) : 0;
        currentWater = (mEdtCurrentWater.getText().toString().trim().length() > 0) ? Float.parseFloat(mEdtCurrentWater.getText().toString()) : 0;
    }

    public void updateContract() {
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.edt_deposit, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_customer_name, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_current_electricity, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_current_water, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_numer_of_customer, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_number_of_bike, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.txt_pay_cycle, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.txt_date_start, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.txt_date_end, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        if (awesomeValidation.validate()) {
            setDataContract();
            Contract contract = new Contract(startDate, endDate, deposit, paymentCycle, paymentDay, contractCode, presentator, numberOfCustomer, numberOfBike, currentElectricity, currentWater, mRoomList.get(0).getId(), true);
            contract.setId(mContractList.get(0).getId());
            mUpdateContractPresenter.updateContract(contract);
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
                Intent intentRoom = new Intent(UpdateContractActivity.this, RoomActivity.class);
                intentRoom.putExtra("LIST_CONTRACT", (ArrayList<Contract>) mContractList);
                intentRoom.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                intentRoom.putExtra("LIST_ROOM", (ArrayList<Room>) mRoomList);
                intentRoom.putExtra("POSITION", position);
                startActivity(intentRoom);
                break;
            case R.id.btn_update_contract:
                updateContract();
                break;
            case R.id.btn_delete_contract:
                mDeleteContractPresenter.deleteContract(mContractList.get(0).getId());
                break;
            case R.id.lnl_date_start:
                showDialogDateStart();
                break;
            case R.id.lnl_date_end:
                showDialogDateEnd();
                break;
            case R.id.lnl_room_pay_cycle:
                showPayCycleDialog();
                break;

        }
    }

    @Override
    public void updateContractSuccessful(Contract contract) {
        mContractList.set(0, contract);
        Intent intentRoom = new Intent(UpdateContractActivity.this, RoomActivity.class);
        intentRoom.putExtra("LIST_CONTRACT", (ArrayList<Contract>) mContractList);
        intentRoom.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
        intentRoom.putExtra("LIST_ROOM", (ArrayList<Room>) mRoomList);
        intentRoom.putExtra("POSITION", position);
        startActivity(intentRoom);
    }

    @Override
    public void updateContractFail() {

    }

    @Override
    public void deleteContractSuccessful(Contract contract) {
        List<Integer> listId = new ArrayList<>();
        for (Customer customer:mCustomerList
             ) {
            listId.add(customer.getId());
        }
        mDeleteCustomerPresenter.deleteCustomer(listId);
    }

    @Override
    public void deleteContractFail() {

    }

    @Override
    public void deleteCustomerSuccessful() {
        mRoomList.get(position).setActive(true);
        mUpdateRoomPresenter.updateRoom(mRoomList.get(position));
    }

    @Override
    public void deleteCustomerFail() {

    }

    @Override
    public void updateRoomSuccessful(Room room) {
        Intent intentMainHome = new Intent(UpdateContractActivity.this, MainHomeActivity.class);
        startActivity(intentMainHome);
    }

    @Override
    public void updateRoomFail() {

    }
}
