package com.example.motelapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.os.UserManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.motelapp.R;
import com.example.motelapp.fragments.ApartmentFragment;
import com.example.motelapp.fragments.EmptyAccountFragment;
import com.example.motelapp.fragments.NewsFragment;
import com.example.motelapp.fragments.OverviewFragment;
import com.example.motelapp.fragments.RoomFragment;
import com.example.motelapp.fragments.SettingFragment;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.News;
import com.example.motelapp.models.Room;
import com.example.motelapp.presenters.ApartmentPresenter;
import com.example.motelapp.presenters.ContractPresenter;
import com.example.motelapp.presenters.GetCustomerPresenter;
import com.example.motelapp.presenters.GetNewsPresenter;
import com.example.motelapp.presenters.RoomPresenter;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.room.managements.UserManagement;
import com.example.motelapp.views.ApartmentView;
import com.example.motelapp.views.ContractView;
import com.example.motelapp.views.GetCustomerView;
import com.example.motelapp.views.GetNewsView;
import com.example.motelapp.views.RoomView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainHomeActivity extends AppCompatActivity implements ApartmentView, RoomView, ContractView, GetCustomerView, GetNewsView {

    private Toolbar mtlbTopToolbar;
    private BottomNavigationView mbnvBottomNavigationView;

    private static ApartmentPresenter mApartmentPresenter;
    private static RoomPresenter mRoomPresenter;
    private static ContractPresenter mContractPresenter;
    private static GetCustomerPresenter mCustomerPresenter;
    private static GetNewsPresenter mGetNewsPresenter;

    private static List<Apartment> apartmentList;
    private static List<Room> roomList;
    private static List<Contract> contractList;
    private static List<Customer> customerList;
    private static List<News> newsList;

    private static UserManagement mUserManager;


    private User user;

    private static Application mApplication;

    private static int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_home);
        innitialData();


    }

    public void initialView() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        mtlbTopToolbar = (Toolbar) findViewById(R.id.tlb_top_toolbar);
//        mtlbTopToolbar.setNavigationIcon(R.drawable.phone_huwei);
        setSupportActionBar(mtlbTopToolbar);
        getSupportActionBar().setTitle("Thống kê");

        mbnvBottomNavigationView = findViewById(R.id.bnv_Bottom_Navigation_Bar);
        mbnvBottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        Fragment selectedFragment = null;
        if (apartmentList.size() > 0) {
            selectedFragment = new OverviewFragment(apartmentList, roomList, contractList, index);
        } else {
            selectedFragment = new EmptyAccountFragment();
        }
        getSupportFragmentManager().beginTransaction().add(R.id.frame_container,
                selectedFragment).commit();
    }

    public void innitialData() {
        index = getIntent().getIntExtra("POSITION", index);
        apartmentList = new ArrayList<>();
        roomList = new ArrayList<>();
        contractList = new ArrayList<>();
        newsList = new ArrayList<>();

        mApartmentPresenter = new ApartmentPresenter(getApplication(), this, this);
        mRoomPresenter = new RoomPresenter(getApplication(), this, this);
        mContractPresenter = new ContractPresenter(getApplication(), this, this);
        mCustomerPresenter = new GetCustomerPresenter(getApplication(), this, this);
        mGetNewsPresenter = new GetNewsPresenter(getApplication(), this, this);

        mUserManager = new UserManagement(mApplication);
        mUserManager.getmUserInfo(new UserManagement.OnDataCallBackUser() {
            @Override
            public void onDataSuccess(User IUser) {
                user = IUser;
                mApartmentPresenter.getListApartment(user.getUserId());
            }

            @Override
            public void onDataFail() {

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbar_item_apartment:
                Intent intentToCreateApartment = new Intent(getApplicationContext(), ApartmentActivity.class);
                intentToCreateApartment.putExtra("LIST_APARTMENT", (ArrayList<Apartment>) apartmentList);
                intentToCreateApartment.putExtra("USER", user);
                intentToCreateApartment.putExtra("POSITION", index);
                startActivity(intentToCreateApartment);
                break;
        }
        return super.onOptionsItemSelected(item);
//        return true;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;

                    switch (menuItem.getItemId()) {
                        case R.id.nav_overview:
                            selectedFragment = (apartmentList.size() > 0) ? new OverviewFragment(apartmentList, roomList, contractList, index) : new EmptyAccountFragment();
                            mtlbTopToolbar.setTitle("Thống kê");
//                            mToolbar.setVisibility(View.VISIBLE);
                            break;
                        case R.id.nav_apartment:
                            selectedFragment = (apartmentList.size() > 0) ? new ApartmentFragment(apartmentList, roomList, contractList, index, customerList) : new EmptyAccountFragment();
                            mtlbTopToolbar.setTitle("Nhà trọ");
//                            mToolbar.setVisibility(View.VISIBLE);
                            break;
                        case R.id.nav_room:
                            selectedFragment = (apartmentList.size() > 0) ? new RoomFragment(roomList, contractList, apartmentList, index, customerList) : new EmptyAccountFragment();
                            mtlbTopToolbar.setTitle("Phòng trọ");
                            break;
                        case R.id.nav_setting:
                            selectedFragment = new SettingFragment();
                            mtlbTopToolbar.setTitle("Cài đặt");
//                            mToolbar.setVisibility(View.GONE);
                            break;
                        case R.id.nav_news:
                            selectedFragment = (apartmentList.size() > 0) ? new NewsFragment(roomList, index, apartmentList, newsList) : new EmptyAccountFragment();
                            mtlbTopToolbar.setTitle("Tin tức");
//                            mToolbar.setVisibility(View.GONE);
                            break;
                    }
//                    getSupportFragmentManager().beginTransaction().add(R.id.frame_container,
//                            selectedFragment).commit();
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame_container,
                            selectedFragment).commit();
                    return true;
                }
            };

    @Override
    public void getListApartmentSuccess(List<Apartment> apartmentList) {
        this.apartmentList = apartmentList;
        if (apartmentList.size() > 0) {
            for (int i = 0; i < this.apartmentList.size(); i++) {
                if (!this.apartmentList.get(i).isActive()) {
                    this.apartmentList.remove(i);
                    i = i - 1;
                }
            }
            mRoomPresenter.getListRoom(apartmentList.get(index).getId());
        } else {
            initialView();
        }
    }

    @Override
    public void getListApartmentFail(String msgGetListApartment) {

    }

    @Override
    public void getListRoomSuccess(List<Room> roomList) {
        this.roomList = roomList;
        if (roomList.size() > 0) {
            mContractPresenter.getListContract(apartmentList.get(index).getId());
        } else {
            initialView();
        }
    }

    @Override
    public void getListRoomFail(String msgGetListApartment) {
        initialView();
    }

    @Override
    public void getListContractSuccess(List<Contract> contractList) {
        this.contractList = contractList;
        if (contractList.size() > 0) {
            mCustomerPresenter.getListCustomer(apartmentList.get(index).getId());
        } else {
            mGetNewsPresenter.getListNews(apartmentList.get(index).getId());
        }
    }

    @Override
    public void getListContractFail(String msgGetListApartment) {
        mGetNewsPresenter.getListNews(apartmentList.get(index).getId());
    }

    @Override
    public void getListCustomerSuccess(List<Customer> customerList) {
        if (customerList.size() > 0) {
            for (int i = 0; i < customerList.size(); i++){
                if (!customerList.get(i).isActive()){
                    customerList.remove(i);
                    i = i -1;
                }
            }
            this.customerList = customerList;
            mGetNewsPresenter.getListNews(apartmentList.get(index).getId());
        } else {
            initialView();
        }
    }

    @Override
    public void getListCustomerFail() {
        initialView();
    }

    @Override
    public void getNewsSuccessful(List<News> mNewsList) {
        newsList = mNewsList;
        initialView();
    }

    @Override
    public void getNewsFail() {
        initialView();
    }
}

