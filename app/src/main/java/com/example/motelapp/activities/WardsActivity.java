package com.example.motelapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.motelapp.R;
import com.example.motelapp.adapters.DistrictAdapter;
import com.example.motelapp.adapters.WardsAdapter;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.District;
import com.example.motelapp.models.Province;
import com.example.motelapp.models.Room;
import com.example.motelapp.models.Wards;
import com.example.motelapp.presenters.DistrictPresenter;
import com.example.motelapp.presenters.WardsPresenter;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.views.WardsView;

import java.util.ArrayList;
import java.util.List;

public class WardsActivity extends AppCompatActivity implements View.OnClickListener, WardsView {

    private LinearLayout mLnlItemBack;

    private static WardsPresenter mWardsPresenter;
    private WardsAdapter mWardsAdapter;
    private List<Wards> mWardsList;
    private RecyclerView mRecycleWards;

    private User mUser;
    //    private Customer mCustomer;
    private List<Customer> mCustomerList;
    private List<Room> roomList;
    private District district;
    private Province province;
    private List<Apartment> mApartmentList;
    private List<Contract> mContractList;
    private int mPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wards);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);

        mRecycleWards = findViewById(R.id.recycle_wards);
        mRecycleWards.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        mRecycleWards.setLayoutManager(layoutManager);
    }

    private void initialData() {
        mUser = (User) getIntent().getSerializableExtra("USER");
        mCustomerList = (ArrayList<Customer>) getIntent().getSerializableExtra("LIST_CUSTOMER");
        roomList = (ArrayList<Room>) getIntent().getSerializableExtra("LIST_ROOM");
        mPosition = getIntent().getIntExtra("POSITION", mPosition);
        mApartmentList = (ArrayList<Apartment>) getIntent().getSerializableExtra("LIST_APARTMENT");
        mContractList = (ArrayList<Contract>) getIntent().getSerializableExtra("LIST_CONTRACT");
        mLnlItemBack.setOnClickListener(this);

        mWardsPresenter = new WardsPresenter(getApplication(), this, this);
        district = (District) getIntent().getSerializableExtra("DISTRICT");
        province = (Province) getIntent().getSerializableExtra("PROVINCE");
        int districtId = Integer.parseInt(district.getId());
        mWardsList = mWardsPresenter.getListWards(districtId);
    }

    public void updateUI(final List<Wards> wardsList) {
        if (mWardsAdapter == null) {
            mWardsAdapter = new WardsAdapter(getApplicationContext(), wardsList);
            mRecycleWards.setAdapter(mWardsAdapter);


            mWardsAdapter.getPosition(new WardsAdapter.OnClickListener() {
                @Override
                public void onClickListener(int position) {
                    Toast.makeText(WardsActivity.this, "position province " + position, Toast.LENGTH_SHORT).show();
                    if (mUser != null) {
                        Intent intent = new Intent(WardsActivity.this, CreateApartmentActivity.class);
                        intent.putExtra("USER", mUser);
                        intent.putExtra("WARDS", wardsList.get(position));
                        intent.putExtra("DISTRICT", district);
                        intent.putExtra("PROVINCE", province);
                        startActivity(intent);
                    } else if (mCustomerList != null && mContractList == null) {
                        Intent intent = new Intent(WardsActivity.this, TenantsInfoActivity.class);
                        intent.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                        intent.putExtra("POSITION", mPosition);
                        intent.putExtra("WARDS", wardsList.get(position));
                        intent.putExtra("DISTRICT", district);
                        intent.putExtra("PROVINCE", province);
                        startActivity(intent);
                    } else if (roomList != null && mContractList == null) {
                        Intent intent = new Intent(WardsActivity.this, CreateContractActivity.class);
                        intent.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                        intent.putExtra("POSITION", mPosition);
                        intent.putExtra("WARDS", wardsList.get(position));
                        intent.putExtra("DISTRICT", district);
                        intent.putExtra("PROVINCE", province);
                        startActivity(intent);
                    } else if (mApartmentList != null ) {
                        Intent intentUpdateApartment = new Intent(WardsActivity.this, UpdateApartmentActivity.class);
                        intentUpdateApartment.putExtra("LIST_APARTMENT", (ArrayList<Apartment>) mApartmentList);
                        intentUpdateApartment.putExtra("POSITION", mPosition);
                        intentUpdateApartment.putExtra("DISTRICT", district);
                        intentUpdateApartment.putExtra("PROVINCE", province);
                        intentUpdateApartment.putExtra("WARDS", wardsList.get(position));
                        startActivity(intentUpdateApartment);
                    } else if (mContractList != null) {
                        Intent intentCreateTenants = new Intent(WardsActivity.this, CreateTenantsActivity.class);
                        intentCreateTenants.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                        intentCreateTenants.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                        intentCreateTenants.putExtra("LIST_CONTRACT", (ArrayList<Contract>) mContractList);
                        intentCreateTenants.putExtra("POSITION", mPosition);
                        intentCreateTenants.putExtra("DISTRICT", district);
                        intentCreateTenants.putExtra("PROVINCE", province);
                        intentCreateTenants.putExtra("WARDS", wardsList.get(position));
                        startActivity(intentCreateTenants);
                    } else {
                        Intent intent = new Intent(WardsActivity.this, AccountRegistrationActivity.class);
                        intent.putExtra("WARDS", wardsList.get(position));
                        intent.putExtra("DISTRICT", district);
                        intent.putExtra("PROVINCE", province);
                        startActivity(intent);
                    }
                }
            });
        } else {
            mWardsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
                if (mUser != null) {
                    Intent intentCreateApartment = new Intent(WardsActivity.this, CreateApartmentActivity.class);
                    intentCreateApartment.putExtra("USER", mUser);
                    intentCreateApartment.putExtra("DISTRICT", district);
                    intentCreateApartment.putExtra("PROVINCE", province);
                    startActivity(intentCreateApartment);
                } else if (mCustomerList != null && mContractList == null) {
                    Intent intentTenantInfo = new Intent(WardsActivity.this, TenantsInfoActivity.class);
                    intentTenantInfo.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                    intentTenantInfo.putExtra("POSITION", mPosition);
                    intentTenantInfo.putExtra("DISTRICT", district);
                    intentTenantInfo.putExtra("PROVINCE", province);
                    startActivity(intentTenantInfo);
                } else if (roomList != null && mContractList == null) {
                    Intent intentCreateContract = new Intent(WardsActivity.this, CreateContractActivity.class);
                    intentCreateContract.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                    intentCreateContract.putExtra("POSITION", mPosition);
                    intentCreateContract.putExtra("DISTRICT", district);
                    intentCreateContract.putExtra("PROVINCE", province);
                    startActivity(intentCreateContract);
                } else if (mContractList != null) {
                    Intent intentCreateTenants = new Intent(WardsActivity.this, CreateTenantsActivity.class);
                    intentCreateTenants.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                    intentCreateTenants.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                    intentCreateTenants.putExtra("LIST_CONTRACT", (ArrayList<Contract>) mContractList);
                    intentCreateTenants.putExtra("POSITION", mPosition);
                    intentCreateTenants.putExtra("DISTRICT", district);
                    intentCreateTenants.putExtra("PROVINCE", province);
                    startActivity(intentCreateTenants);
                } else {
                    Intent intentNewAccount = new Intent(WardsActivity.this, AccountRegistrationActivity.class);
                    intentNewAccount.putExtra("DISTRICT", district);
                    intentNewAccount.putExtra("PROVINCE", province);
                    startActivity(intentNewAccount);
                }
                break;
        }
    }

    @Override
    public void getListWardsSuccess(List<Wards> wardsList) {
        if (mWardsList == null) {
            mWardsList = new ArrayList<>();
            mWardsList = wardsList;
        } else {
            mWardsList.addAll(wardsList);
        }
        updateUI(mWardsList);
    }

    @Override
    public void getListWardsFail(String msgErr) {

    }
}
