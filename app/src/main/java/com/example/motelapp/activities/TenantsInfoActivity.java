package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Update;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.motelapp.R;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.District;
import com.example.motelapp.models.Province;
import com.example.motelapp.models.Wards;
import com.example.motelapp.presenters.UpdateCustomerPresenter;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.views.UpdateTenantsView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TenantsInfoActivity extends AppCompatActivity implements View.OnClickListener, UpdateTenantsView {

    private DatePickerDialog.OnDateSetListener mDateSetListener;

    private LinearLayout mLnlItemBack, mLnlTenantsYOB, mLnlProvince, mLnlDistrict, mLnlWard, mLnlCallPhone;
    private EditText mEdtTenantNumberPhone;
    private TextView mTxtTenantName, mTxtTenantsYOB, mTxtProvince, mTxtDistrict, mTxtWards;
    private Button mBtnUpdateInfoTenants;

    private Customer customer;
    private List<Customer> customerList;
    private int position;

    private static Province province;
    private static District district;
    private static Wards wards;

    private static String phone = "";
    private static String yob = "";


    private UpdateCustomerPresenter mUpdateCustomerPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tenants_info);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mLnlTenantsYOB = findViewById(R.id.lnl_tenants_yob);
        mEdtTenantNumberPhone = findViewById(R.id.edt_tenants_phone_number);
        mTxtTenantName = findViewById(R.id.txt_tenants_name);
        mTxtTenantsYOB = findViewById(R.id.txt_tenants_yob);
        mTxtProvince = findViewById(R.id.txt_province);
        mTxtDistrict = findViewById(R.id.txt_district);
        mTxtWards = findViewById(R.id.txt_wards);
        mBtnUpdateInfoTenants = findViewById(R.id.btn_update_info_tenants);
        mLnlProvince = findViewById(R.id.lnl_province);
        mLnlDistrict = findViewById(R.id.lnl_district);
        mLnlWard = findViewById(R.id.lnl_wards);
        mLnlCallPhone = findViewById(R.id.lnl_call_phone);
    }

    private void initialData() {
        mUpdateCustomerPresenter = new UpdateCustomerPresenter(getApplication(), this, this);
        customerList = (ArrayList<Customer>) getIntent().getSerializableExtra("LIST_CUSTOMER");
        position = getIntent().getIntExtra("POSITION", position);
        customer = customerList.get(position);
        if (customer != null) {
            mTxtTenantName.setText(customer.getName());
            mEdtTenantNumberPhone.setText((phone.trim().length() > 0) ? phone : customer.getPhone());
            mTxtTenantsYOB.setText((yob.trim().length() > 0) ? yob : customer.getYob());
            mTxtProvince.setText(customer.getAddress().split(",")[2]);
            mTxtDistrict.setText(customer.getAddress().split(",")[1]);
            mTxtWards.setText(customer.getAddress().split(",")[0]);
        }
        mLnlItemBack.setOnClickListener(this);
        mLnlTenantsYOB.setOnClickListener(this);
        mLnlProvince.setOnClickListener(this);
        mLnlDistrict.setOnClickListener(this);
        mLnlWard.setOnClickListener(this);
        mBtnUpdateInfoTenants.setOnClickListener(this);

        //dia chi
        province = (Province) getIntent().getSerializableExtra("PROVINCE");
        district = (District) getIntent().getSerializableExtra("DISTRICT");
        wards = (Wards) getIntent().getSerializableExtra("WARDS");
        String provinceName = (province != null) ? province.getName() : "";
        String districtName = (district != null) ? district.getName() : "";
        String wardsName = (wards != null) ? wards.getName() : "";
        mTxtProvince.setText(provinceName);
        mTxtDistrict.setText(districtName);
        mTxtWards.setText(wardsName);

        mLnlCallPhone.setOnClickListener(this);
    }

    public void setData() {
        phone = (mEdtTenantNumberPhone.getText().toString().length() > 0) ? mEdtTenantNumberPhone.getText().toString() : "";
        yob = (mTxtTenantsYOB.getText().toString().length() > 0) ? mTxtTenantsYOB.getText().toString() : "";
    }

    public void updateCustomer() {
        String phone = mEdtTenantNumberPhone.getText().toString();
        customer.setPhone(phone);
        mUpdateCustomerPresenter.updateCustomer(customer);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
                Intent intentToTennant = new Intent(TenantsInfoActivity.this, TenantsActivity.class);
                intentToTennant.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) customerList);
                startActivity(intentToTennant);
                break;
            case R.id.btn_update_info_tenants:
                updateCustomer();
                break;
            case R.id.lnl_tenants_yob:
                showDialogYOB();
                break;
            case R.id.lnl_province:
                Intent intentProvince = new Intent(TenantsInfoActivity.this, ProvinceActivity.class);
                intentProvince.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) customerList);
                intentProvince.putExtra("POSITION", position);
                startActivity(intentProvince);
                break;
            case R.id.lnl_district:
                if (mTxtProvince.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Vui lòng chọn tỉnh", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intentToDistrict = new Intent(TenantsInfoActivity.this, DistrictActivity.class);
                    intentToDistrict.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) customerList);
                    intentToDistrict.putExtra("POSITION", position);
                    intentToDistrict.putExtra("PROVINCE", province);
                    startActivity(intentToDistrict);
                }
                break;
            case R.id.lnl_wards:
                if (mTxtDistrict.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Vui lòng chọn tỉnh, huyện", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intentToWards = new Intent(TenantsInfoActivity.this, WardsActivity.class);
                    intentToWards.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) customerList);
                    intentToWards.putExtra("POSITION", position);
                    intentToWards.putExtra("DISTRICT", district);
                    intentToWards.putExtra("PROVINCE", province);
                    startActivity(intentToWards);
                }
                break;

            case R.id.lnl_call_phone:
                Intent intentCallPhone = new Intent(Intent.ACTION_DIAL);
                try {
                    String phoneNumber = "tel:" + mEdtTenantNumberPhone.getText().toString().trim();
                    intentCallPhone.setData(Uri.parse(phoneNumber));
                    startActivity(intentCallPhone);
                } catch (Exception ex) {
                    String phoneErr = getString(R.string.phone_number_error);
                    Toast.makeText(this, phoneErr, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void showDialogYOB() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog pickerDialog = new DatePickerDialog(TenantsInfoActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String tenantYOB = year + "-" + month + "-" + day;
                mTxtTenantsYOB.setText(tenantYOB);
            }
        },
                year, month, day);
        pickerDialog.show();

    }

    @Override
    public void updateTenantsSuccess(Customer customer) {
        if (customer != null) {
            mTxtTenantName.setText(customer.getName());
            mEdtTenantNumberPhone.setText(customer.getPhone());
        }
    }

    @Override
    public void updateTenantsFail() {

    }

}
