package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.motelapp.R;
import com.example.motelapp.networks.CheckNetwork;
import com.example.motelapp.presenters.AccountRememberPresenter;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.views.AccountRememberView;


public class IndexActivity extends AppCompatActivity implements AccountRememberView {

    private ImageView mImgIndex;
    private Animation mAnimRotate;
    private AccountRememberPresenter mAccountRememberPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        initialView();
        initialData();
    }

    public void initialView(){
        mImgIndex = findViewById(R.id.img_app_index);
    }

    public void initialData() {
        Animation();
        waitTimeAnimation();
        mImgIndex.startAnimation(mAnimRotate);
        mAccountRememberPresenter = new AccountRememberPresenter(getApplication(), this, this);
        AccountIsExist();

    }

    //check account is exist
    public void AccountIsExist(){
        mAccountRememberPresenter.AccountRemember();

    }


    //Animation img
    public void Animation() {
        mAnimRotate = AnimationUtils.loadAnimation(this, R.anim.anim_rotate);

    }

    public void waitTimeAnimation() {
        Thread thread = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    Intent intent = new Intent(IndexActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        };
        thread.start();

    }

    public void onPause() {
        super.onPause();
        finish();
    }


    @Override
    public void accountIsExist(final User user) {
        Handler handler = new Handler();
        if (CheckNetwork.isInternetAvailable(this)) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
//                    LoginActivity.intentToMainHome(IndexActivity.this);
                    Intent intent= new Intent(IndexActivity.this,MainHomeActivity.class);
                    intent.putExtra("USER", user);
                    startActivity(intent);
                    finish();
                }
            }, 3000);
        } else {
            finish();
        }
    }

    @Override
    public void accountNoExist() {
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                LoginActivity.intentToLoginActivity(IndexActivity.this);
//                finish();
//            }
//        }, 3000);
//        finish();
    }



}
