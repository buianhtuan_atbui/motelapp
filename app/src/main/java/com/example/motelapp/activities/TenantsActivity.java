package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.motelapp.R;
import com.example.motelapp.adapters.TenantsAdapter;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.Room;

import java.util.ArrayList;
import java.util.List;

import retrofit2.http.POST;

public class TenantsActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout mLnlItemBack, mLnlAddTenants;

    private RecyclerView mCustomerRecycle;
    private TenantsAdapter mTenantsAdapter;

    private static int mPosition;
    private static List<Room> mRoomList;
    private static List<Contract> mContractList;
    private static List<Customer> mCustomerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tenants);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlAddTenants = findViewById(R.id.lnl_add_tenants);
        mLnlItemBack = (LinearLayout) findViewById(R.id.lnl_item_back);
        mCustomerRecycle = findViewById(R.id.recycle_tenants);
        mCustomerRecycle.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        mCustomerRecycle.setLayoutManager(layoutManager);
    }

    private void initialData() {
        mCustomerList = new ArrayList<>();
        mRoomList =(ArrayList<Room>) getIntent().getSerializableExtra("LIST_ROOM");
        mPosition = getIntent().getIntExtra("POSITION", mPosition);
        mContractList = (ArrayList<Contract>) getIntent().getSerializableExtra("LIST_CONTRACT");
        mCustomerList = (ArrayList<Customer>) getIntent().getSerializableExtra("LIST_CUSTOMER");
        mLnlAddTenants.setOnClickListener(this);
        mLnlItemBack.setOnClickListener(this);
        for (int i = 0; i < mCustomerList.size(); i++){
            if (!mCustomerList.get(i).isActive()){
                mCustomerList.remove(i);
                i = i -1;
            }
        }
        for (int i = 0; i < mContractList.size(); i++){
            if (!mContractList.get(i).isActive()){
                mContractList.remove(i);
                i = i -1;
            }
        }
        UpdateUI(mCustomerList);
    }

    public void UpdateUI(final List<Customer> customerList){
        if (mTenantsAdapter == null){
            mTenantsAdapter = new TenantsAdapter(getApplicationContext(), customerList);
            mCustomerRecycle.setAdapter(mTenantsAdapter);

        } else{
            mTenantsAdapter.notifyDataSetChanged();
        }
        mTenantsAdapter.getPosition(new TenantsAdapter.OnClickListener() {
            @Override
            public void onClickListener(int position) {
                Toast.makeText(TenantsActivity.this, "position " + position, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(TenantsActivity.this, TenantsInfoActivity.class);
                intent.putExtra("LIST_CUSTOMER", (ArrayList<Customer>)customerList);
                intent.putExtra("POSITION", position);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
//                Intent intentToHome = new Intent(TenantsActivity.this, MainHomeActivity.class);
//                startActivity(intentToHome);
//                break;
                Intent intentToHome = new Intent(TenantsActivity.this, MainHomeActivity.class);
                setResult(1);
                finish();
                break;
            case R.id.lnl_add_tenants:
                if (mCustomerList.size() < mContractList.get(0).getNumberOfCustomer()){
                    Intent intentAddTenants = new Intent(TenantsActivity.this, CreateTenantsActivity.class);
                    intentAddTenants.putExtra("LIST_CONTRACT", (ArrayList<Contract>)mContractList);
                    intentAddTenants.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                    intentAddTenants.putExtra("LIST_ROOM", (ArrayList<Room>) mRoomList);
                    intentAddTenants.putExtra("POSITION", mPosition);
                    startActivity(intentAddTenants);
                    break;
                } else {
                    Toast.makeText(this, "Phòng đã đủ người", Toast.LENGTH_SHORT).show();
                }
        }
    }
}
