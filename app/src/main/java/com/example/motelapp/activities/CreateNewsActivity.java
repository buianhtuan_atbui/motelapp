package com.example.motelapp.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.motelapp.R;
import com.example.motelapp.adapters.NewsAdapter;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.models.News;
import com.example.motelapp.models.Room;
import com.example.motelapp.presenters.CreateNewsPresenter;
import com.example.motelapp.utils.Permission_Access;
import com.example.motelapp.views.CreateNewsView;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CreateNewsActivity extends AppCompatActivity implements View.OnClickListener ,CreateNewsView {

    private TextView mTxtNewsName, mTxtNewDescription, mTxtNewPrice, mTxtNewArea, mTxtNewElectric, mTxtNewWater;
    private EditText mEdtNewsTitle;
    private LinearLayout mLnlItemBack, mLnlImgNews;
    private Button mBtnCreateNews;
    private ImageView mImgLoadImageNews;
    private String mediaPath;

    private CreateNewsPresenter mCreateNewsPresenter;
    private List<Room> mRoomList;
    private List<Apartment> mApartmentList;
    private int mPosition;
    private Room room;

    private String content;
    private String tittle;
    private String createDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_news);

        initialView();
        initialData();
    }

    private void initialView() {
        mTxtNewArea = findViewById(R.id.txt_news_area);
        mTxtNewDescription = findViewById(R.id.txt_news_description);
        mTxtNewElectric = findViewById(R.id.txt_news_price_electric);
        mTxtNewPrice = findViewById(R.id.txt_news_price_room);
        mTxtNewsName = findViewById(R.id.txt_news_name_room);
        mTxtNewWater = findViewById(R.id.txt_new_price_water);
        mEdtNewsTitle = findViewById(R.id.edt_news_title);
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mBtnCreateNews = findViewById(R.id.btn_create_news);

        mLnlImgNews = findViewById(R.id.lnl_img_news);
        mImgLoadImageNews = findViewById(R.id.img_load_img_news);
        mImgLoadImageNews.setImageResource(R.drawable.img_room);
    }

    private void initialData() {

        mLnlItemBack.setOnClickListener(this);
        mBtnCreateNews.setOnClickListener(this);

        mCreateNewsPresenter = new CreateNewsPresenter(getApplication(), this, this);
        mRoomList = (ArrayList<Room>) getIntent().getSerializableExtra("LIST_ROOM");
        mApartmentList = (ArrayList<Apartment>) getIntent().getSerializableExtra("LIST_APARTMENT");
        mPosition = (Integer) getIntent().getSerializableExtra("POSITION");
        room = mRoomList.get(mPosition);
        mTxtNewsName.setText(room.getName());
        mTxtNewDescription.setText(room.getDescription());
        mTxtNewArea.setText(String.valueOf(room.getArea()));
        mTxtNewPrice.setText(String.valueOf(room.getRawPrice()));
        for (Apartment apartment: mApartmentList
             ) {
            if(apartment.getId() == room.getApartmentId()){
                mTxtNewElectric.setText(String.valueOf(apartment.getElectricity()));
                mTxtNewWater.setText(String.valueOf(apartment.getWater()));
            }
        }
        content = mTxtNewsName.getText().toString() + "/n" + mTxtNewArea.getText().toString() + "/n" + mTxtNewPrice.getText().toString()
                + "/n" + mTxtNewDescription.getText().toString();

        mLnlImgNews.setOnClickListener(this);
    }

    public void createNews(){
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        createDate = simpleDateFormat.format(date);
        tittle = mEdtNewsTitle.getText().toString();
        content = mTxtNewsName.getText().toString() + "/n" + mTxtNewArea.getText().toString() + "/n" + mTxtNewPrice.getText().toString()
                + "/n" + mTxtNewDescription.getText().toString() + "/n Điện: " + mTxtNewElectric.getText().toString() + "/n Nước: " + mTxtNewWater.getText().toString();
        News news = new News(content, createDate, tittle, room.getId(), true);
        mCreateNewsPresenter.createNews(news);
    }

    @Override
    public void createNewSuccessful(News news) {
        Toast.makeText(this, "Successful", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(CreateNewsActivity.this, MainHomeActivity.class);
        startActivity(intent);
    }

    @Override
    public void createNewsFail() {

    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:

                Intent intentToHome = new Intent(CreateNewsActivity.this, RoomBlankActivity.class);
                intentToHome.putExtra("LIST_ROOM", (ArrayList<Room>) mRoomList);
                startActivity(intentToHome);
                break;
            case R.id.btn_create_news:
                createNews();
                break;
            case R.id.lnl_img_news:
                Permission_Access.verifyStoragePermissions(CreateNewsActivity.this);
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 0);
                break;
        }
    }

    //load img
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == 0 && resultCode == RESULT_OK && data != null) {

                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mediaPath = cursor.getString(columnIndex);
                mImgLoadImageNews.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                // Set the Image in ImageView for Previewing the Media

                cursor.close();

            } else {
                Toast.makeText(this, "You haven't picked Image/Video", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }
}
