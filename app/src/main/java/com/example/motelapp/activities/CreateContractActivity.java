package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.example.motelapp.R;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.District;
import com.example.motelapp.models.Province;
import com.example.motelapp.models.Room;
import com.example.motelapp.models.Wards;
import com.example.motelapp.presenters.CreateContractPresenter;
import com.example.motelapp.presenters.CreateCustomerPresenter;
import com.example.motelapp.presenters.UpdateRoomPresenter;
import com.example.motelapp.views.CreateContractView;
import com.example.motelapp.views.CreateCustomerView;
import com.example.motelapp.views.UpdateRoomView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CreateContractActivity extends AppCompatActivity implements View.OnClickListener, CreateContractView, CreateCustomerView, UpdateRoomView {

    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private LinearLayout mLnlItemBack, mLnlDateStart, mLnlDateEnd, mLnlDateTenantsYOB, mLnlChoiceGender, mLnlRoomPayCycle,
            mLnlProvince, mLnlDistrict, mLnlWards;
    private TextView mTxtDateStart, mTxtDateEnd, mTxtDateTenantsYOB, mTxtGenderShow, mTxtRoomName, mTxtPayCycle,
            mTxtProvince, mTxtDistrict, mTxtWards;
    private EditText mEdtContractCode, mEdtDeposit, mEdtCustomerName, mEdtCustomerPhone, mEdtPersonalId, mEdtCurrentElectricity, mEdtCurrentWater,
            mEdtNumberOfBike, mEdtNumberOfCustomer;
    private Button mBtnCreateContract;
    private TextView mTxtFocus;

    private static Province province;
    private static District district;
    private static Wards wards;

    private List<Room> roomList;
    private int position;
    private Room room;

    private static String startDate = "";
    private static String endDate = "";
    private static int deposit = 0;
    private static int paymentCycle = 1;
    private static int paymentDay = 1;
    private static String contractCode = "";
    private static String presentator = "";
    private static int numberOfCustomer = 0;
    private static int numberOfBike = 0;
    private static float currentElectricity = 0;
    private static float currentWater = 0;

    private static String name = "";
    private static String phone = "";
    private static String personalId = "";
    private static String address = "";
    private static String sex = "";
    private static String yob = "";

    private Contract contract;
    private Customer customer;

    private CreateCustomerPresenter mCreateCustomerPresenter;
    private CreateContractPresenter mCreateContractPresenter;
    private UpdateRoomPresenter mUpdateRoomPresenter;

    private AwesomeValidation awesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contract);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mLnlDateStart = findViewById(R.id.lnl_date_start);
        mLnlDateEnd = findViewById(R.id.lnl_date_end);
        mTxtDateStart = findViewById(R.id.txt_date_start);
        mTxtDateEnd = findViewById(R.id.txt_date_end);
        mLnlDateTenantsYOB = findViewById(R.id.lnl_date_tenants_yob);
        mTxtDateTenantsYOB = findViewById(R.id.txt_date_tenants_yob);
        mLnlChoiceGender = findViewById(R.id.lnl_choice_gender);
        mTxtGenderShow = findViewById(R.id.txt_gender_show);
        mTxtRoomName = findViewById(R.id.txt_room_name);
        mEdtDeposit = findViewById(R.id.edt_deposit);
        mEdtCustomerName = findViewById(R.id.edt_customer_name);
        mEdtCustomerPhone = findViewById(R.id.edt_customer_phone);
        mEdtPersonalId = findViewById(R.id.edt_personal_id);
        mEdtCurrentElectricity = findViewById(R.id.edt_current_electricity);
        mEdtCurrentWater = findViewById(R.id.edt_current_water);
        mEdtNumberOfBike = findViewById(R.id.edt_number_of_bike);
        mEdtNumberOfCustomer = findViewById(R.id.edt_numer_of_customer);
        mBtnCreateContract = findViewById(R.id.btn_create_contract);
        mLnlRoomPayCycle = findViewById(R.id.lnl_room_pay_cycle);
        mTxtPayCycle = findViewById(R.id.txt_pay_cycle);
        mEdtContractCode = findViewById(R.id.edt_contract_code);

        //address
        mLnlProvince = findViewById(R.id.lnl_province);
        mLnlDistrict = findViewById(R.id.lnl_district);
        mLnlWards = findViewById(R.id.lnl_wards);
        mTxtProvince = findViewById(R.id.txt_province);
        mTxtDistrict = findViewById(R.id.txt_district);
        mTxtWards = findViewById(R.id.txt_wards);

        //focus
        mTxtFocus = findViewById(R.id.txt_focus);
    }

    private void initialData() {
        mCreateCustomerPresenter = new CreateCustomerPresenter(getApplication(), this, this);
        mCreateContractPresenter = new CreateContractPresenter(getApplication(), this, this);
        mUpdateRoomPresenter = new UpdateRoomPresenter(getApplication(), this, this);
        roomList = (ArrayList<Room>) getIntent().getSerializableExtra("LIST_ROOM");
        position = (Integer) getIntent().getSerializableExtra("POSITION");
        room = roomList.get(position);
        mTxtRoomName.setText(room.getName());
        mLnlItemBack.setOnClickListener(this);
        mLnlDateStart.setOnClickListener(this);
        mLnlDateEnd.setOnClickListener(this);
        mLnlDateTenantsYOB.setOnClickListener(this);
        mLnlChoiceGender.setOnClickListener(this);
        mBtnCreateContract.setOnClickListener(this);
        mLnlProvince.setOnClickListener(this);
        mLnlDistrict.setOnClickListener(this);
        mLnlWards.setOnClickListener(this);
        mLnlRoomPayCycle.setOnClickListener(this);

        province = (Province) getIntent().getSerializableExtra("PROVINCE");
        district = (District) getIntent().getSerializableExtra("DISTRICT");
        wards = (Wards) getIntent().getSerializableExtra("WARDS");
        String provinceName = (province != null) ? province.getName() : "";
        String districtName = (district != null) ? district.getName() : "";
        String wardsName = (wards != null) ? wards.getName() : "";
        mTxtProvince.setText(provinceName);
        mTxtDistrict.setText(districtName);
        mTxtWards.setText(wardsName);

        mTxtDateStart.setText((startDate.trim().length() > 0) ? startDate : "");
        mTxtDateEnd.setText((endDate.trim().length() > 0) ? endDate : "");
        mEdtDeposit.setText((deposit > 0) ? String.valueOf(deposit) : "");
        mTxtPayCycle.setText((paymentCycle > 0) ? String.valueOf(paymentCycle) : "");
        mEdtCurrentElectricity.setText((currentElectricity > 0) ? String.valueOf(currentElectricity) : "");
        mEdtCurrentWater.setText((currentWater > 0) ? String.valueOf(currentElectricity) : "");
        mEdtNumberOfCustomer.setText((numberOfCustomer > 0) ? String.valueOf(numberOfCustomer) : "");
        mEdtNumberOfBike.setText((numberOfBike > 0) ? String.valueOf(numberOfBike) : "");

        mEdtCustomerPhone.setText((phone.trim().length() > 0) ? phone : "");
        mEdtCustomerName.setText((name.trim().length() > 0) ? name : "");
        mTxtDateTenantsYOB.setText((yob.trim().length() > 0) ? yob : "");
        mTxtGenderShow.setText((sex.trim().length() > 0) ? sex : "");
        mEdtPersonalId.setText((personalId.trim().length() > 0) ? personalId : "");

        if(province != null) {
            mTxtFocus.requestFocus();
        }

    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:

                Intent intentToHome = new Intent(CreateContractActivity.this, RoomBlankActivity.class);
                intentToHome.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
//                startActivity(intentToHome);
                setResult(1, intentToHome);
                finish();
                break;

            case R.id.lnl_date_start:
                showDialogDateStart();
                break;
            case R.id.lnl_date_end:
                showDialogDateEnd();
                break;
            case R.id.lnl_date_tenants_yob:
                showDialogDateTenantsYOB();
                break;
            case R.id.lnl_choice_gender:
                showGenderDialog();
                break;
            case R.id.btn_create_contract:
                createContract();
                break;
            case R.id.lnl_room_pay_cycle:
                showPayCycleDialog();
                break;
            case R.id.lnl_province:
                Intent intentToProvince = new Intent(CreateContractActivity.this, ProvinceActivity.class);
                intentToProvince.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                intentToProvince.putExtra("POSITION", position);
                setDataContract();
                setDataCustomer();
                startActivity(intentToProvince);
                break;
            case R.id.lnl_district:
                if (mTxtProvince.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Vui lòng chọn tỉnh", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intentToDistrict = new Intent(CreateContractActivity.this, DistrictActivity.class);
                    intentToDistrict.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                    intentToDistrict.putExtra("POSITION", position);
                    intentToDistrict.putExtra("PROVINCE", province);
                    setDataContract();
                    setDataCustomer();
                    startActivity(intentToDistrict);
                }
                break;
            case R.id.lnl_wards:
                if (mTxtDistrict.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Vui lòng chọn tỉnh, huyện", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intentToWards = new Intent(CreateContractActivity.this, WardsActivity.class);
                    intentToWards.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                    intentToWards.putExtra("POSITION", position);
                    intentToWards.putExtra("DISTRICT", district);
                    intentToWards.putExtra("PROVINCE", province);
                    setDataContract();
                    setDataCustomer();
                    startActivity(intentToWards);
                }
                break;
        }
    }

    public void showDialogDateStart() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog pickerDialog = new DatePickerDialog(CreateContractActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String dateStart = year + "/" + month + "/" + day;
                mTxtDateStart.setText(dateStart);
            }
        },
                year, month, day);
        pickerDialog.show();
    }

    //date end
    public void showDialogDateEnd() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog pickerDialog = new DatePickerDialog(CreateContractActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String dateStart = year + "/" + month + "/" + day;
                mTxtDateEnd.setText(dateStart);
            }
        },
                year, month, day);
        pickerDialog.show();
    }

    //date tenant yob
    public void showDialogDateTenantsYOB() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog pickerDialog = new DatePickerDialog(CreateContractActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String dateStart = year + "/" + month + "/" + day;
                mTxtDateTenantsYOB.setText(dateStart);
            }
        },
                year, month, day);
        pickerDialog.show();
    }

    //dialog gioi tinh
    private void showGenderDialog() {
        final Dialog dialog = new Dialog(CreateContractActivity.this);
        dialog.setContentView(R.layout.dialog_gender);
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.YELLOW));
        //gender
        TextView mTxtNam, mTxtNu, mTxtKhac;

        mTxtNam = dialog.findViewById(R.id.txt_nam);
        mTxtNu = dialog.findViewById(R.id.txt_nu);
        mTxtKhac = dialog.findViewById(R.id.txt_khac);

        //gender onclick
        mTxtNam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtGenderShow.setText("Nam");
                dialog.dismiss();
            }
        });
        mTxtNu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtGenderShow.setText("Nữ");
                dialog.dismiss();
            }
        });
        mTxtKhac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtGenderShow.setText("Khác");
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    //dialog pay cycle
    private void showPayCycleDialog() {
        final Dialog dialog = new Dialog(CreateContractActivity.this);
        dialog.setContentView(R.layout.dialog_pay_cycle);
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        TextView mTxt1Month, mTxt2Month, mTxt3Month;

        mTxt1Month = dialog.findViewById(R.id.txt_1_month);
        mTxt2Month = dialog.findViewById(R.id.txt_2_month);
        mTxt3Month = dialog.findViewById(R.id.txt_3_month);

        //gender onclick
        mTxt1Month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtPayCycle.setText("1");
                dialog.dismiss();
            }
        });
        mTxt2Month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtPayCycle.setText("2");
                dialog.dismiss();
            }
        });
        mTxt3Month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtPayCycle.setText("3");
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void setDataContract() {
        startDate = mTxtDateStart.getText().toString();
        endDate = mTxtDateEnd.getText().toString();
        deposit = (mEdtDeposit.getText().toString().trim().length() > 0) ? Integer.parseInt(mEdtDeposit.getText().toString()) : 0;
        paymentCycle = (mTxtPayCycle.getText().toString().trim().length() > 0) ? Integer.parseInt(mTxtPayCycle.getText().toString()) : 0;
        paymentDay = 1;
        contractCode = !(mEdtContractCode.getText().toString().trim().length() > 0) ? mEdtContractCode.getText().toString() : "";
        presentator = mEdtCustomerName.getText().toString();
        numberOfCustomer = (mEdtNumberOfCustomer.getText().toString().trim().length() > 0) ? Integer.parseInt(mEdtNumberOfCustomer.getText().toString()) : 0;
        numberOfBike = (mEdtNumberOfBike.getText().toString().trim().length() > 0) ? Integer.parseInt(mEdtNumberOfBike.getText().toString()) : 0;
        currentElectricity = (mEdtCurrentElectricity.getText().toString().trim().length() > 0) ? Float.parseFloat(mEdtCurrentElectricity.getText().toString()) : 0;
        currentWater = (mEdtCurrentWater.getText().toString().trim().length() > 0) ? Float.parseFloat(mEdtCurrentWater.getText().toString()) : 0;
    }

    public void setDataCustomer() {
        name = mEdtCustomerName.getText().toString();
        phone = mEdtCustomerPhone.getText().toString();
        personalId = mEdtPersonalId.getText().toString();
        address = mTxtWards.getText().toString() + ", " + mTxtDistrict.getText().toString() + ", " + mTxtProvince.getText().toString();
        sex = mTxtGenderShow.getText().toString();
        yob = mTxtDateTenantsYOB.getText().toString();
    }

    public void createContract() {
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.edt_deposit, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_customer_name, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_current_electricity, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_current_water, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_numer_of_customer, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_number_of_bike, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.txt_pay_cycle, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.txt_date_start, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.txt_date_end, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        if (awesomeValidation.validate()) {
            setDataContract();
            contract = new Contract(startDate, endDate, deposit, paymentCycle, paymentDay, contractCode, presentator, numberOfCustomer, numberOfBike, currentElectricity, currentWater, room.getId(), true);
            mCreateContractPresenter.createContract(contract);
        }
    }

    public void createCustomer() {
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.edt_customer_phone, RegexTemplate.NOT_EMPTY, R.string.app_name);
        awesomeValidation.addValidation(this, R.id.edt_personal_id, RegexTemplate.NOT_EMPTY, R.string.app_name);
        awesomeValidation.addValidation(this, R.id.txt_date_tenants_yob, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.txt_gender_show, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.txt_province, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.txt_district, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.txt_wards, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        if (awesomeValidation.validate()) {
            int contractId = contract.getId();
            setDataCustomer();
            customer = new Customer(address, name, personalId, phone, sex, yob, true, contractId);
            mCreateCustomerPresenter.createCustomer(customer);
        }

    }

    @Override
    public void createContractSuccessful(Contract contract) {

        this.contract = contract;
        createCustomer();
    }

    @Override
    public void createContractFail() {

    }

    @Override
    public void createCustomerSuccessful(Customer customer) {
        roomList.get(position).setActive(false);
        mUpdateRoomPresenter.updateRoom(roomList.get(position));
//        Intent intent = new Intent(CreateContractActivity.this, RoomBlankActivity.class);
//        intent.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
//        startActivity(intent);
    }

    @Override
    public void createCustomerFail() {

    }

    @Override
    public void updateRoomSuccessful(Room room) {
        Intent intent = new Intent(CreateContractActivity.this, MainHomeActivity.class);
        intent.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
        startActivity(intent);
    }

    @Override
    public void updateRoomFail() {

    }
}
