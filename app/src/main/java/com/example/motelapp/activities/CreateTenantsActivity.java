package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.example.motelapp.R;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.District;
import com.example.motelapp.models.Province;
import com.example.motelapp.models.Room;
import com.example.motelapp.models.Tenants;
import com.example.motelapp.models.Wards;
import com.example.motelapp.presenters.CreateCustomerPresenter;
import com.example.motelapp.views.CreateContractView;
import com.example.motelapp.views.CreateCustomerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CreateTenantsActivity extends AppCompatActivity implements View.OnClickListener, CreateCustomerView {
    private LinearLayout mLnlItemBack, mLnlTenantsYOB, mLnlProvince, mLnlDistrict, mLnlWard, mLnlChoiceGender;
    private EditText mEdtTenantNumberPhone;
    private TextView mTxtTenantsYOB, mTxtProvince, mTxtDistrict, mTxtWards, mTxtGenderShow;
    private EditText mEdtTenantName, mEdtPersonalId;
    private Button mBtnCreateCustomer;

    private int position;
    private List<Room> mRoomList;
    private List<Contract> mContractList;
    private List<Customer> mCustomerList;

    private Room mRoom;
    private Contract mContract;

    private static Province province;
    private static District district;
    private static Wards wards;

    private static String phone = "";
    private static String name = "";
    private static String yob = "";
    private static String personalId = "";
    private static String sex = "";

    private CreateCustomerPresenter mCreateCustomerPresenter;

    private AwesomeValidation awesomeValidation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_tenants);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mLnlTenantsYOB = findViewById(R.id.lnl_date_tenants_yob);
        mEdtTenantNumberPhone = findViewById(R.id.edt_customer_phone);
        mEdtTenantName = findViewById(R.id.edt_customer_name);
        mEdtPersonalId = findViewById(R.id.edt_personal_id);
        mTxtTenantsYOB = findViewById(R.id.txt_date_tenants_yob);
        mTxtProvince = findViewById(R.id.txt_province);
        mTxtDistrict = findViewById(R.id.txt_district);
        mTxtWards = findViewById(R.id.txt_wards);
        mTxtGenderShow = findViewById(R.id.txt_gender_show);
        mBtnCreateCustomer = findViewById(R.id.btn_create_tenants);
        mLnlProvince = findViewById(R.id.lnl_province);
        mLnlDistrict = findViewById(R.id.lnl_district);
        mLnlWard = findViewById(R.id.lnl_wards);
        mLnlChoiceGender = findViewById(R.id.lnl_choice_gender);
    }

    private void initialData() {
        mLnlItemBack.setOnClickListener(this);
        mBtnCreateCustomer.setOnClickListener(this);
        mLnlTenantsYOB.setOnClickListener(this);
        mLnlDistrict.setOnClickListener(this);
        mLnlProvince.setOnClickListener(this);
        mLnlWard.setOnClickListener(this);
        mLnlChoiceGender.setOnClickListener(this);
        mCreateCustomerPresenter = new CreateCustomerPresenter(getApplication(), this, this);
        mRoomList = (ArrayList<Room>) getIntent().getSerializableExtra("LIST_ROOM");
        position = getIntent().getIntExtra("POSITION", position);
        mContractList = (ArrayList<Contract>) getIntent().getSerializableExtra("LIST_CONTRACT");
        mCustomerList = (ArrayList<Customer>) getIntent().getSerializableExtra("LIST_CUSTOMER");
        mRoom = (Room) getIntent().getSerializableExtra("ROOM");
        mRoom = (mRoom == null) ? mRoomList.get(position) : mRoom;
        for (Contract contract : mContractList) {
            mContract = (contract.getRoomId() == mRoom.getId() && contract.isActive()) ? contract : new Contract();
        }
        for (int i = 0; i < mCustomerList.size(); i++) {
            if (mCustomerList.get(i).getContract_id() != mContract.getId()) {
                mCustomerList.remove(i);
                i = i - 1;
            }
        }

        province = (Province) getIntent().getSerializableExtra("PROVINCE");
        district = (District) getIntent().getSerializableExtra("DISTRICT");
        wards = (Wards) getIntent().getSerializableExtra("WARDS");
        String provinceName = (province != null) ? province.getName() : "";
        String districtName = (district != null) ? district.getName() : "";
        String wardsName = (wards != null) ? wards.getName() : "";
        mTxtProvince.setText(provinceName);
        mTxtDistrict.setText(districtName);
        mTxtWards.setText(wardsName);

        mEdtTenantNumberPhone.setText((phone.trim().length() > 0) ? phone : "");
        mEdtTenantName.setText((name.trim().length() > 0) ? name : "");
        mTxtTenantsYOB.setText((yob.trim().length() > 0) ? yob : "");
        mTxtGenderShow.setText((sex.trim().length() > 0) ? sex : "");
        mEdtPersonalId.setText((personalId.trim().length() > 0) ? personalId : "");


    }

    public void setData() {
        name = (mEdtTenantName.getText().toString().length() > 0) ? mEdtTenantName.getText().toString() : "";
        phone = (mEdtTenantNumberPhone.getText().toString().length() > 0) ? mEdtTenantNumberPhone.getText().toString() : "";
        yob = (mTxtTenantsYOB.getText().toString().length() > 0) ? mTxtTenantsYOB.getText().toString() : "";
        sex = (mTxtGenderShow.getText().toString().length() > 0) ? mTxtGenderShow.getText().toString() : "";
        personalId = (mEdtPersonalId.getText().toString().length() > 0) ? mEdtPersonalId.getText().toString() : "";
    }

    public void createTenants() {
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.edt_customer_phone, RegexTemplate.NOT_EMPTY, R.string.app_name);
        awesomeValidation.addValidation(this, R.id.edt_personal_id, RegexTemplate.NOT_EMPTY, R.string.app_name);
        awesomeValidation.addValidation(this, R.id.edt_customer_name, RegexTemplate.NOT_EMPTY, R.string.app_name);
        awesomeValidation.addValidation(this, R.id.txt_date_tenants_yob, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.txt_gender_show, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.txt_province, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.txt_district, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.txt_wards, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        if (awesomeValidation.validate()) {
            setData();
            String address = mTxtWards.getText().toString() + ", " + mTxtDistrict.getText().toString() + ", " + mTxtProvince.getText().toString();
            Customer tenant = new Customer(address, name, personalId, phone, sex, yob, true, mContract.getId());
            mCreateCustomerPresenter.createCustomer(tenant);
        }
    }

    public void showDialogYOB() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog pickerDialog = new DatePickerDialog(CreateTenantsActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String tenantYOB = year + "-" + month + "-" + day;
                mTxtTenantsYOB.setText(tenantYOB);
            }
        },
                year, month, day);
        pickerDialog.show();

    }

    private void showGenderDialog() {
        final Dialog dialog = new Dialog(CreateTenantsActivity.this);
        dialog.setContentView(R.layout.dialog_gender);
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.YELLOW));
        //gender
        TextView mTxtNam, mTxtNu, mTxtKhac;

        mTxtNam = dialog.findViewById(R.id.txt_nam);
        mTxtNu = dialog.findViewById(R.id.txt_nu);
        mTxtKhac = dialog.findViewById(R.id.txt_khac);

        //gender onclick
        mTxtNam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtGenderShow.setText("Nam");
                dialog.dismiss();
            }
        });
        mTxtNu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtGenderShow.setText("Nữ");
                dialog.dismiss();
            }
        });
        mTxtKhac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtGenderShow.setText("Khác");
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
                Intent intentToTennant = new Intent(CreateTenantsActivity.this, TenantsActivity.class);
                intentToTennant.putExtra("LIST_CONTRACT", (ArrayList<Contract>) mContractList);
                intentToTennant.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                intentToTennant.putExtra("LIST_ROOM", (ArrayList<Room>) mRoomList);
                intentToTennant.putExtra("POSITION", position);
                startActivity(intentToTennant);
                break;
            case R.id.btn_create_tenants:
                if (mContractList.get(0).getNumberOfCustomer() > mCustomerList.size()) {
                    createTenants();
                } else {
                    Toast.makeText(this, "Phòng đã đủ người", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lnl_date_tenants_yob:
                showDialogYOB();
                break;
            case R.id.lnl_choice_gender:
                showGenderDialog();
                break;
            case R.id.lnl_province:
                Intent intentProvince = new Intent(CreateTenantsActivity.this, ProvinceActivity.class);
                intentProvince.putExtra("LIST_CONTRACT", (ArrayList<Contract>) mContractList);
                intentProvince.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                intentProvince.putExtra("LIST_ROOM", (ArrayList<Room>) mRoomList);
                intentProvince.putExtra("POSITION", position);
                intentProvince.putExtra("POSITION", position);
                setData();
                startActivity(intentProvince);
                break;
            case R.id.lnl_district:
                if (mTxtProvince.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Vui lòng chọn tỉnh", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intentToDistrict = new Intent(CreateTenantsActivity.this, DistrictActivity.class);
                    intentToDistrict.putExtra("LIST_CONTRACT", (ArrayList<Contract>) mContractList);
                    intentToDistrict.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                    intentToDistrict.putExtra("LIST_ROOM", (ArrayList<Room>) mRoomList);
                    intentToDistrict.putExtra("POSITION", position);
                    intentToDistrict.putExtra("POSITION", position);
                    intentToDistrict.putExtra("PROVINCE", province);
                    setData();
                    startActivity(intentToDistrict);
                }
                break;
            case R.id.lnl_wards:
                if (mTxtDistrict.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Vui lòng chọn tỉnh, huyện", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intentToWards = new Intent(CreateTenantsActivity.this, WardsActivity.class);
                    intentToWards.putExtra("LIST_CONTRACT", (ArrayList<Contract>) mContractList);
                    intentToWards.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                    intentToWards.putExtra("LIST_ROOM", (ArrayList<Room>) mRoomList);
                    intentToWards.putExtra("POSITION", position);
                    intentToWards.putExtra("DISTRICT", district);
                    intentToWards.putExtra("PROVINCE", province);
                    setData();
                    startActivity(intentToWards);
                }
                break;
        }

    }


    @Override
    public void createCustomerSuccessful(Customer customer) {
        mCustomerList.add(customer);
        Intent intentToTennant = new Intent(CreateTenantsActivity.this, TenantsActivity.class);
        intentToTennant.putExtra("LIST_CONTRACT", (ArrayList<Contract>) mContractList);
        intentToTennant.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
        intentToTennant.putExtra("LIST_ROOM", (ArrayList<Room>) mRoomList);
        intentToTennant.putExtra("POSITION", position);
        startActivity(intentToTennant);
    }

    @Override
    public void createCustomerFail() {

    }
}
