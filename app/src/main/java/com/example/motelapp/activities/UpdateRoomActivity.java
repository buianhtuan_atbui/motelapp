package com.example.motelapp.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.example.motelapp.R;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.models.Image;
import com.example.motelapp.models.Room;
import com.example.motelapp.presenters.CreateRoomPresenter;
import com.example.motelapp.presenters.UpdateRoomPresenter;
import com.example.motelapp.presenters.UploadImagePresenter;
import com.example.motelapp.utils.Permission_Access;
import com.example.motelapp.views.UpdateRoomView;

import com.example.motelapp.views.UploadImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UpdateRoomActivity extends AppCompatActivity implements View.OnClickListener, UpdateRoomView, UploadImageView {

    private LinearLayout mLnlItemBack, mLnlImgUpdateRoom;
    private ImageView mImgLoadImageUpdateRoom;
    private String mediaPath;

    private static MultipartBody.Part fileUpload;
    private static List<MultipartBody.Part> mListFile;

    private EditText mEdtRoomName, mEdtDescription, mEdtRoomMoney, mEdtAcreage, mEdtPeople;
    private Button mBtnUpdateRoom;

    private AwesomeValidation awesomeValidation;
    private UpdateRoomPresenter mUpdateRoomPresenter;
    private UploadImagePresenter mUploadImagePresenter;

    private Room room;

    private static String roomName;
    private static String description;
    private static float price;
    private static float acreage;
    private static String url;
    private static int apartmentId;
    private static boolean active;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_room);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mEdtRoomName = findViewById(R.id.edt_room_name);
        mEdtDescription = findViewById(R.id.edt_description);
        mEdtRoomMoney = findViewById(R.id.edt_room_money);
        mEdtAcreage = findViewById(R.id.edt_acreage);
        mEdtPeople = findViewById(R.id.edt_input_people);
        mBtnUpdateRoom = findViewById(R.id.btn_update_room);
        mLnlImgUpdateRoom = findViewById(R.id.lnl_img_update_room);
        mImgLoadImageUpdateRoom = findViewById(R.id.img_load_img_update_room);
//        mImgLoadImageUpdateRoom.setImageResource(R.drawable.img_room);
    }

    private void initialData() {
        room = (Room) getIntent().getSerializableExtra("ROOM");
        mUpdateRoomPresenter = new UpdateRoomPresenter(getApplication(), this, this);
        mUploadImagePresenter = new UploadImagePresenter(getApplication(), this, this);
        mLnlItemBack.setOnClickListener(this);
        mBtnUpdateRoom.setOnClickListener(this);

        mEdtRoomName.setText(room.getName());
        mEdtAcreage.setText(String.valueOf(room.getArea()));
        mEdtDescription.setText(room.getDescription());
        mEdtRoomMoney.setText(String.valueOf(room.getRawPrice()));

        url = room.getImage();
        mLnlImgUpdateRoom.setOnClickListener(this);
        if(room.getImage() != null) {
            Picasso.Builder builder = new Picasso.Builder(getApplicationContext());
            builder.build().load("http://" + room.getImage())
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_background)
                    .fit()
                    .centerInside()
                    .into(mImgLoadImageUpdateRoom);
        } else {
            mImgLoadImageUpdateRoom.setImageResource(R.drawable.img_room);
        }

    }

    public void uploadImage() {
        mListFile = new ArrayList<>();
        File file = new File(mediaPath);

        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        fileUpload = MultipartBody.Part.createFormData("files", file.getName(), requestBody);
        mListFile.add(fileUpload);
        String fileName = url.substring(23);
        if (!fileName.equals(file.getName())) {
            mUploadImagePresenter.uploadImage(mListFile);
        } else {
            updateRoom();
        }
    }

    public void updateRoom() {

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.edt_room_name, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_description, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_acreage, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_room_money, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_input_people, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        if (awesomeValidation.validate()) {
            roomName = mEdtRoomName.getText().toString();
            description = mEdtDescription.getText().toString();
            price = Float.parseFloat(mEdtRoomMoney.getText().toString());
            acreage = Float.parseFloat(mEdtAcreage.getText().toString());
            apartmentId = room.getApartmentId();
            active = true;
            Room room = new Room(roomName, description, price, acreage, url, active, apartmentId);
            room.setId(this.room.getId());
            mUpdateRoomPresenter.updateRoom(room);
        }


    }


    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
                Intent intentBack = new Intent(UpdateRoomActivity.this, RoomActivity.class);
                intentBack.putExtra("ROOM", room);
//                startActivity(intentBack);
                setResult(1);
                finish();
                break;
            case R.id.btn_update_room:
                uploadImage();
                break;
            case R.id.lnl_img_update_room:
                Permission_Access.verifyStoragePermissions(UpdateRoomActivity.this);
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 0);
                break;
        }
    }

    //load img


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == 0 && resultCode == RESULT_OK && data != null) {

                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mediaPath = cursor.getString(columnIndex);
                mImgLoadImageUpdateRoom.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                // Set the Image in ImageView for Previewing the Media

                cursor.close();

            } else {
                Toast.makeText(this, "You haven't picked Image/Video", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void updateRoomSuccessful(Room room) {
        Intent intentBack = new Intent(UpdateRoomActivity.this, MainHomeActivity.class);
        intentBack.putExtra("ROOM", room);
        startActivity(intentBack);
    }

    @Override
    public void updateRoomFail() {

    }

    @Override
    public void uploadImageSuccessFul(List<Image> image) {
        url = image.get(0).getUrl();
        updateRoom();
    }

    @Override
    public void uploadImageFail() {

    }
}
