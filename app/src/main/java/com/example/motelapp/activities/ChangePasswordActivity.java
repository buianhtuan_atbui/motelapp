package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.example.motelapp.R;
import com.example.motelapp.models.Account;
import com.example.motelapp.presenters.ChangePasswordPresenter;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.views.ChangePasswordView;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener, ChangePasswordView {

    private LinearLayout mLnlItemBack;
    private Button mBtnChangePass;
    private EditText mEdtOldPassword, mEdtNewPassword, mEdtConfirmPassword;

    private User mUser;

    private AwesomeValidation awesomeValidation;

    private ChangePasswordPresenter mChangePasswordPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mBtnChangePass = findViewById(R.id.btn_change_pass);
        mEdtOldPassword = findViewById(R.id.edt_old_password);
        mEdtNewPassword = findViewById(R.id.edt_new_password);
        mEdtConfirmPassword = findViewById(R.id.edt_confirm_new_password);
    }

    private void initialData() {
        mUser = (User) getIntent().getSerializableExtra("USER");
        mChangePasswordPresenter = new ChangePasswordPresenter(getApplication(), this, this);
        mLnlItemBack.setOnClickListener(this);
        mBtnChangePass.setOnClickListener(this);
    }

    private void changePassword() {
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        if (!mEdtOldPassword.getText().toString().equals(mUser.getPassword())) {
            awesomeValidation.addValidation(this, R.id.edt_old_password, mEdtOldPassword.getText().toString(), R.string.old_password_wrong);
        }
        awesomeValidation.addValidation(this, R.id.edt_new_password, RegexTemplate.NOT_EMPTY, R.string.app_name);
        if (awesomeValidation.validate()) {
            String newPassword = mEdtNewPassword.getText().toString();
            String confirmPassword = mEdtConfirmPassword.getText().toString();
            if (newPassword.equals(confirmPassword)) {
                mUser.setPassword(newPassword);
                Account account = new Account(mUser.getPassword(), mUser.getEmail(), mUser.getFull_name(), mUser.getPhoneNumber(), mUser.getAddress(), mUser.getYOB(), mUser.getSex(), "", true);
                account.setId(mUser.getUserId());
                mChangePasswordPresenter.changePassword(account);
            } else {
                awesomeValidation.addValidation(this, R.id.edt_confirm_new_password, confirmPassword, R.string.comfirm_password_not_match);
            }
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
                Intent intentToHome = new Intent(ChangePasswordActivity.this, MainHomeActivity.class);
//                startActivity(intentToHome);
                setResult(1);
                finish();
                break;

            case R.id.btn_change_pass:
                changePassword();
                break;
        }
    }

    @Override
    public void changePasswordSuccessful() {
        Intent intentToLogin = new Intent(this, LoginActivity.class);
        startActivity(intentToLogin);
    }

    @Override
    public void changePasswordFail() {

    }
}
