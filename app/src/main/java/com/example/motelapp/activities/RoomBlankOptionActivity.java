package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.motelapp.R;
import com.example.motelapp.models.Room;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class RoomBlankOptionActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout mLnlItemBack, mLnlBtnUpdateRoom, mLnlBtnContractRoom;
    private TextView mTxtRoomName;

    private List<Room> mRoomList;
    private int position;
    private Room mRoom;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_blank_option);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mLnlBtnContractRoom = findViewById(R.id.lnl_btn_contract_room);
        mLnlBtnUpdateRoom = findViewById(R.id.lnl_btn_update_room);
        mTxtRoomName = findViewById(R.id.txt_room_name);
    }
    private void initialData() {
        mLnlItemBack.setOnClickListener(this);
        mLnlBtnContractRoom.setOnClickListener(this);
        mLnlBtnUpdateRoom.setOnClickListener(this);

        mRoomList = (ArrayList<Room>) getIntent().getSerializableExtra("LIST_ROOM");
        position = getIntent().getIntExtra("POSITION", position);
        mRoom = mRoomList.get(position);
        mTxtRoomName.setText(mRoom.getName());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lnl_item_back:
                Intent intent = new Intent(RoomBlankOptionActivity.this, MainHomeActivity.class);
                startActivity(intent);
                break;
            case R.id.lnl_btn_contract_room:
                Intent intentCreateContract = new Intent(RoomBlankOptionActivity.this, CreateContractActivity.class);
                intentCreateContract.putExtra("LIST_ROOM", (ArrayList<Room>) mRoomList);
                intentCreateContract.putExtra("POSITION", position);
                startActivity(intentCreateContract);
                break;
            case R.id.lnl_btn_update_room:
                Intent intentUpdateRoom = new Intent(RoomBlankOptionActivity.this, UpdateRoomActivity.class);
                intentUpdateRoom.putExtra("ROOM", mRoom);
                startActivity(intentUpdateRoom);
                break;
        }
    }
}
