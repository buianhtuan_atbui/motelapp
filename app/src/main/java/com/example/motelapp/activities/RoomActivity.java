package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.motelapp.R;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.Room;

import java.util.ArrayList;
import java.util.List;

public class RoomActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout mLnlItemBack, mLnlDeleteRoom, mLnlBtnUpdateRoom, mLnlBtnTenantsRoom, mLnlBtnContractRoom, mLnlBtnCreateBill;
    private TextView mTxtTenantsRoom, mTxtStatusRoom, mTxtVehicleRoom;
    private int position;
    private List<Room> mRoomList;
    private List<Contract> mContractList;
    private List<Customer> mCustomerList;
    private Room mRoom;
    private Contract mContract;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlBtnCreateBill = findViewById(R.id.lnl_btn_bill);
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mLnlDeleteRoom = findViewById(R.id.lnl_delete_room);
        mLnlBtnUpdateRoom = findViewById(R.id.lnl_btn_update_room);
        mLnlBtnContractRoom = findViewById(R.id.lnl_btn_contract_room);
        mLnlBtnTenantsRoom = findViewById(R.id.lnl_btn_tenants_room);
        mTxtTenantsRoom = findViewById(R.id.txt_tenants_of_room);
        mTxtStatusRoom = findViewById(R.id.txt_status_room);
        mTxtVehicleRoom = findViewById(R.id.txt_vehicle_of_room);
    }

    private void initialData() {
        mLnlBtnCreateBill.setOnClickListener(this);
        mRoomList =(ArrayList<Room>) getIntent().getSerializableExtra("LIST_ROOM");
        position = getIntent().getIntExtra("POSITION", position);
        mContractList = (ArrayList<Contract>) getIntent().getSerializableExtra("LIST_CONTRACT");
        mCustomerList = (ArrayList<Customer>) getIntent().getSerializableExtra("LIST_CUSTOMER");
        mRoom = (Room) getIntent().getSerializableExtra("ROOM");
        mRoom = (mRoom == null) ? mRoomList.get(position) : mRoom;
        for (Contract contract: mContractList) {
            mContract = (contract.getRoomId() == mRoom.getId() && contract.isActive()) ? contract : new Contract();
        }
        for (int i = 0; i < mCustomerList.size(); i++){
            if (mCustomerList.get(i).getContract_id() != mContract.getId()){
                mCustomerList.remove(i);
                i = i -1;
            }
        }

        mLnlItemBack.setOnClickListener(this);
        mLnlDeleteRoom.setOnClickListener(this);
        mLnlBtnUpdateRoom.setOnClickListener(this);
        mLnlBtnTenantsRoom.setOnClickListener(this);
        mLnlBtnContractRoom.setOnClickListener(this);
        mTxtTenantsRoom.setText(String.valueOf(mContract.getNumberOfCustomer()));
        mTxtVehicleRoom.setText(String.valueOf(mContract.getNumberOfBike()));
        mTxtStatusRoom.setText("Đang ở");
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
                Intent intent = new Intent(RoomActivity.this, MainHomeActivity.class);
                setResult(1);
                finish();
                break;
            case R.id.lnl_delete_room:
                showDeleteDialog();
                break;
            case R.id.lnl_btn_update_room:
                Intent intentUpdate = new Intent(RoomActivity.this, UpdateRoomActivity.class);
                intentUpdate.putExtra("ROOM", mRoom);
                startActivity(intentUpdate);
                break;
            case R.id.lnl_btn_contract_room:
                Intent intentContract = new Intent(RoomActivity.this, UpdateContractActivity.class);
                intentContract.putExtra("LIST_CONTRACT", (ArrayList<Contract>)mContractList);
                intentContract.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                intentContract.putExtra("LIST_ROOM", (ArrayList<Room>) mRoomList);
                intentContract.putExtra("POSITION", position);
                startActivity(intentContract);
                break;
            case R.id.lnl_btn_tenants_room:
                Intent intentTenants = new Intent(RoomActivity.this, TenantsActivity.class);
                intentTenants.putExtra("LIST_CONTRACT", (ArrayList<Contract>)mContractList);
                intentTenants.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                intentTenants.putExtra("LIST_ROOM", (ArrayList<Room>) mRoomList);
                intentTenants.putExtra("POSITION", position);
                startActivity(intentTenants);
                break;

            case R.id.lnl_btn_bill:
                Intent intentBill = new Intent(RoomActivity.this, CreateBillActivity.class);
                intentBill.putExtra("LIST_CONTRACT", (ArrayList<Contract>)mContractList);
                intentBill.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                intentBill.putExtra("LIST_ROOM", (ArrayList<Room>) mRoomList);
                intentBill.putExtra("POSITION", position);
                startActivity(intentBill);
                break;
        }
    }

    //    show dialog delete
    public void showDeleteDialog() {
        final Dialog dialog = new Dialog(RoomActivity.this);
        dialog.setContentView(R.layout.dialog_delete_room);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button buttonOk = dialog.findViewById(R.id.btn_ok_delete_room);
        Button buttonNo = dialog.findViewById(R.id.btn_No_delete_room);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
