package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.motelapp.R;
import com.example.motelapp.adapters.ApartmentAdapter;
import com.example.motelapp.adapters.ProvinceAdapter;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.Province;
import com.example.motelapp.models.Room;
import com.example.motelapp.presenters.ProvincePresenter;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.views.ProvinceView;
import com.example.motelapp.views.UpdateApartmentView;

import java.util.ArrayList;
import java.util.List;

public class ProvinceActivity extends AppCompatActivity implements View.OnClickListener, ProvinceView {

    private User mUser;
    //    private Customer mCustomer;
    private List<Customer> mCustomerList;
    private List<Room> roomList;
    private List<Apartment> mApartmentList;
    private List<Contract> mContractList;

    private LinearLayout mLnlItemBack;

    private static ProvincePresenter mProvincePresenter;
    private ProvinceAdapter mProvinceAdapter;
    private List<Province> mProvinceList;
    private RecyclerView mRecycleProvince;

    private int mPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_province);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mRecycleProvince = findViewById(R.id.recycle_province);
        mRecycleProvince.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        mRecycleProvince.setLayoutManager(layoutManager);
    }

    private void initialData() {
        mUser = (User) getIntent().getSerializableExtra("USER");
        mCustomerList = (ArrayList<Customer>) getIntent().getSerializableExtra("LIST_CUSTOMER");
        roomList = (ArrayList<Room>) getIntent().getSerializableExtra("LIST_ROOM");
        mPosition = getIntent().getIntExtra("POSITION", mPosition);
        mApartmentList = (List<Apartment>) getIntent().getSerializableExtra("LIST_APARTMENT");
        mContractList = (ArrayList<Contract>) getIntent().getSerializableExtra("LIST_CONTRACT");
        mLnlItemBack.setOnClickListener(this);

        mProvincePresenter = new ProvincePresenter(getApplication(), this, this);

        mProvinceList = mProvincePresenter.getListProvince();
    }

    public void updateUI(final List<Province> provinces) {
        if (mProvinceAdapter == null) {
            mProvinceAdapter = new ProvinceAdapter(getApplicationContext(), provinces);
            mRecycleProvince.setAdapter(mProvinceAdapter);

            mProvinceAdapter.getPosition(new ProvinceAdapter.OnClickListener() {
                @Override
                public void onClickListener(int position) {
                    Toast.makeText(ProvinceActivity.this, "position province " + position, Toast.LENGTH_SHORT).show();
                    if (mUser != null) {
                        Intent intentCreateApartment = new Intent(ProvinceActivity.this, CreateApartmentActivity.class);
                        intentCreateApartment.putExtra("USER", mUser);
                        intentCreateApartment.putExtra("PROVINCE", provinces.get(position));
                        startActivity(intentCreateApartment);
                    } else if (mCustomerList != null && mContractList == null) {
                        Intent intentTenantsInfo = new Intent(ProvinceActivity.this, TenantsInfoActivity.class);
                        intentTenantsInfo.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                        intentTenantsInfo.putExtra("POSITION", mPosition);
                        intentTenantsInfo.putExtra("PROVINCE", provinces.get(position));
                        startActivity(intentTenantsInfo);
                    } else if (roomList != null && mContractList == null) {
                        Intent intentTenantsInfo = new Intent(ProvinceActivity.this, CreateContractActivity.class);
                        intentTenantsInfo.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                        intentTenantsInfo.putExtra("POSITION", mPosition);
                        intentTenantsInfo.putExtra("PROVINCE", provinces.get(position));
                        startActivity(intentTenantsInfo);
                    } else if (mApartmentList != null) {
                        Intent intentUpdateApartment = new Intent(ProvinceActivity.this, UpdateApartmentActivity.class);
                        intentUpdateApartment.putExtra("LIST_APARTMENT", (ArrayList<Apartment>) mApartmentList);
                        intentUpdateApartment.putExtra("POSITION", mPosition);
                        intentUpdateApartment.putExtra("PROVINCE", provinces.get(position));
                        startActivity(intentUpdateApartment);
                    } else if (mContractList != null) {
                        Intent intentCreateTenants = new Intent(ProvinceActivity.this, CreateTenantsActivity.class);
                        intentCreateTenants.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                        intentCreateTenants.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                        intentCreateTenants.putExtra("LIST_CONTRACT", (ArrayList<Contract>) mContractList);
                        intentCreateTenants.putExtra("POSITION", mPosition);
                        intentCreateTenants.putExtra("PROVINCE", provinces.get(position));
                        startActivity(intentCreateTenants);
                    } else {
                        Intent intentNewAccount = new Intent(ProvinceActivity.this, AccountRegistrationActivity.class);
                        intentNewAccount.putExtra("PROVINCE", provinces.get(position));
                        startActivity(intentNewAccount);
                    }
                }
            });
        } else {
            mProvinceAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
                if (mUser != null) {

                    Intent intentCreateApartment = new Intent(ProvinceActivity.this, CreateApartmentActivity.class);
                    intentCreateApartment.putExtra("USER", mUser);
                    startActivity(intentCreateApartment);

                } else if (mCustomerList != null && mContractList == null) {
                    Intent intentTenantInfo = new Intent(ProvinceActivity.this, TenantsInfoActivity.class);
                    intentTenantInfo.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                    intentTenantInfo.putExtra("POSITION", mPosition);
                    startActivity(intentTenantInfo);
                } else if (roomList != null && mContractList == null) {
                    Intent intentCreateContract = new Intent(ProvinceActivity.this, CreateContractActivity.class);
                    intentCreateContract.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                    intentCreateContract.putExtra("POSITION", mPosition);
                    startActivity(intentCreateContract);
                } else if (mContractList != null) {
                    Intent intentCreateTenants = new Intent(ProvinceActivity.this, CreateTenantsActivity.class);
                    intentCreateTenants.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                    intentCreateTenants.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                    intentCreateTenants.putExtra("LIST_CONTRACT", (ArrayList<Contract>) mContractList);
                    intentCreateTenants.putExtra("POSITION", mPosition);
                    startActivity(intentCreateTenants);
                } else {
                    Intent intentNewAccount = new Intent(ProvinceActivity.this, AccountRegistrationActivity.class);
                    startActivity(intentNewAccount);
                }
                break;
        }
    }


    @Override
    public void getListProvinceSuccess(List<Province> provinceList) {
        if (mProvinceList == null) {
            mProvinceList = new ArrayList<>();
            mProvinceList = provinceList;
        } else {
            mProvinceList.addAll(provinceList);
        }
        updateUI(mProvinceList);
    }

    @Override
    public void getListProvinceFail(String msgGetListProvince) {

    }
}
