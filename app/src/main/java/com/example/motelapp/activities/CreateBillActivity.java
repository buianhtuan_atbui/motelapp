package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.example.motelapp.R;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.models.Bill;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.Room;
import com.example.motelapp.presenters.ApartmentPresenter;
import com.example.motelapp.presenters.CreateBillPresenter;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.room.managements.UserManagement;
import com.example.motelapp.views.ApartmentView;
import com.example.motelapp.views.CreateBillView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CreateBillActivity extends AppCompatActivity implements View.OnClickListener, ApartmentView, CreateBillView {

    LinearLayout mLnlItemBack;
    private int position;
    private List<Room> mRoomList;
    private List<Contract> mContractList;
    private List<Customer> mCustomerList;
    private Apartment mApartment;
    private UserManagement mUserManager;
    private ApartmentPresenter mApartmentPresenter;
    private CreateBillPresenter mCreateBillPresenter;

    private EditText mEdtEndElectricity, mEdtEndWater;
    private Button mBtnCreateBill;

    private AwesomeValidation awesomeValidation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_bill);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mEdtEndElectricity = findViewById(R.id.edt_out_electric);
        mEdtEndWater = findViewById(R.id.edt_out_water);
        mBtnCreateBill = findViewById(R.id.btn_create_bill);
    }

    private void initialData() {
        mLnlItemBack.setOnClickListener(this);
        mBtnCreateBill.setOnClickListener(this);
        mRoomList = (ArrayList<Room>) getIntent().getSerializableExtra("LIST_ROOM");
        position = getIntent().getIntExtra("POSITION", position);
        mContractList = (ArrayList<Contract>) getIntent().getSerializableExtra("LIST_CONTRACT");
        mCustomerList = (ArrayList<Customer>) getIntent().getSerializableExtra("LIST_CUSTOMER");
        mApartmentPresenter = new ApartmentPresenter(getApplication(), this, this);
        mCreateBillPresenter = new CreateBillPresenter(getApplication(), this, this);
        mUserManager = new UserManagement(getApplication());
        mUserManager.getmUserInfo(new UserManagement.OnDataCallBackUser() {
            @Override
            public void onDataSuccess(User user) {
                mApartmentPresenter.getListApartment(user.getUserId());
            }

            @Override
            public void onDataFail() {

            }
        });


    }

    public void createBill() {
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.edt_out_electric, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_out_water, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        if (awesomeValidation.validate()){
            Date date = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String createDate = simpleDateFormat.format(date);
            float endElectrictity = Float.parseFloat(mEdtEndElectricity.getText().toString());
            float endWater = Float.parseFloat(mEdtEndWater.getText().toString());
            float total = (endElectrictity - mContractList.get(0).getCurrentElectricity()) * mApartment.getElectricity()
                    + (endWater - mContractList.get(0).getCurrentWater()) * mApartment.getWater()
                    + mRoomList.get(position).getRawPrice();
            int contractId = mContractList.get(0).getId();
            Bill bill = new Bill(createDate, endElectrictity, endWater, total, contractId);
            mCreateBillPresenter.createBill(bill);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lnl_item_back:
                Intent intentRoom = new Intent(CreateBillActivity.this, RoomActivity.class);
                startActivity(intentRoom);
                break;
            case R.id.btn_create_bill:
                createBill();
                break;
        }
    }

    @Override
    public void getListApartmentSuccess(List<Apartment> apartmentList) {
        for (Apartment apartment : apartmentList
        ) {
            mApartment = (apartment.getId() == mRoomList.get(position).getApartmentId()) ? apartment : null;
        }
    }

    @Override
    public void getListApartmentFail(String msgErr) {

    }

    @Override
    public void createBillSuccessful(Bill bill) {
        Intent intentToMainHome = new Intent(CreateBillActivity.this, MainHomeActivity.class);
        startActivity(intentToMainHome);
    }

    @Override
    public void createBillFail() {

    }
}
