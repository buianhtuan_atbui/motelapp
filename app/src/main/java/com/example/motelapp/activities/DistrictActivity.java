package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.motelapp.R;
import com.example.motelapp.adapters.ApartmentAdapter;
import com.example.motelapp.adapters.DistrictAdapter;
import com.example.motelapp.adapters.ProvinceAdapter;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.District;
import com.example.motelapp.models.Province;
import com.example.motelapp.models.Room;
import com.example.motelapp.presenters.DistrictPresenter;
import com.example.motelapp.presenters.ProvincePresenter;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.views.DistrictView;

import java.util.ArrayList;
import java.util.List;

public class DistrictActivity extends AppCompatActivity implements View.OnClickListener, DistrictView {

    private User mUser;
    //    private Customer mCustomer;
    private List<Customer> mCustomerList;
    private Province province;
    private List<Room> roomList;
    private int mPosition;
    private List<Apartment> mApartmentList;
    private List<Contract> mContractList;


    private LinearLayout mLnlItemBack;

    private static DistrictPresenter mDistrictPresenter;
    private DistrictAdapter mDistrictAdapter;
    private List<District> mDistrictList;
    private RecyclerView mRecycleDistrict;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_district);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mRecycleDistrict = findViewById(R.id.recycle_district);
        mRecycleDistrict.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        mRecycleDistrict.setLayoutManager(layoutManager);
    }

    private void initialData() {
        mUser = (User) getIntent().getSerializableExtra("USER");
        mCustomerList = (ArrayList<Customer>) getIntent().getSerializableExtra("LIST_CUSTOMER");
        roomList = (ArrayList<Room>) getIntent().getSerializableExtra("LIST_ROOM");
        mPosition = getIntent().getIntExtra("POSITION", mPosition);
        mApartmentList = (ArrayList<Apartment>) getIntent().getSerializableExtra("LIST_APARTMENT");
        mContractList = (ArrayList<Contract>) getIntent().getSerializableExtra("LIST_CONTRACT");
        mLnlItemBack.setOnClickListener(this);

        mDistrictPresenter = new DistrictPresenter(getApplication(), this, this);
        province = (Province) getIntent().getSerializableExtra("PROVINCE");
        int provinceId = Integer.parseInt(province.getId());
        mDistrictList = mDistrictPresenter.getListDistrict(provinceId);
    }

    public void updateUI(final List<District> districts) {
        if (mDistrictAdapter == null) {
            mDistrictAdapter = new DistrictAdapter(getApplicationContext(), districts);
            mRecycleDistrict.setAdapter(mDistrictAdapter);


            mDistrictAdapter.getPosition(new DistrictAdapter.OnClickListener() {
                @Override
                public void onClickListener(int position) {
                    Toast.makeText(DistrictActivity.this, "position province " + position, Toast.LENGTH_SHORT).show();
                    if (mUser != null) {
                        Intent intent = new Intent(DistrictActivity.this, CreateApartmentActivity.class);
                        intent.putExtra("USER", mUser);
                        intent.putExtra("DISTRICT", districts.get(position));
                        intent.putExtra("PROVINCE", province);
                        startActivity(intent);
                    } else if (mCustomerList != null && mContractList == null) {
                        Intent intent = new Intent(DistrictActivity.this, TenantsInfoActivity.class);
                        intent.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                        intent.putExtra("POSITION", mPosition);
                        intent.putExtra("DISTRICT", districts.get(position));
                        intent.putExtra("PROVINCE", province);
                        startActivity(intent);
                    } else if (roomList != null && mContractList == null) {
                        Intent intent = new Intent(DistrictActivity.this, CreateContractActivity.class);
                        intent.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                        intent.putExtra("POSITION", mPosition);
                        intent.putExtra("DISTRICT", districts.get(position));
                        intent.putExtra("PROVINCE", province);
                        startActivity(intent);
                    } else if (mApartmentList != null) {
                        Intent intentUpdateApartment = new Intent(DistrictActivity.this, UpdateApartmentActivity.class);
                        intentUpdateApartment.putExtra("LIST_APARTMENT", (ArrayList<Apartment>) mApartmentList);
                        intentUpdateApartment.putExtra("POSITION", mPosition);
                        intentUpdateApartment.putExtra("DISTRICT", districts.get(position));
                        intentUpdateApartment.putExtra("PROVINCE", province);
                        startActivity(intentUpdateApartment);
                    } else if (mContractList != null) {
                        Intent intentCreateTenants = new Intent(DistrictActivity.this, CreateTenantsActivity.class);
                        intentCreateTenants.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                        intentCreateTenants.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                        intentCreateTenants.putExtra("LIST_CONTRACT", (ArrayList<Contract>) mContractList);
                        intentCreateTenants.putExtra("POSITION", mPosition);
                        intentCreateTenants.putExtra("DISTRICT", districts.get(position));
                        intentCreateTenants.putExtra("PROVINCE", province);
                        startActivity(intentCreateTenants);
                    } else {
                        Intent intent = new Intent(DistrictActivity.this, AccountRegistrationActivity.class);
                        intent.putExtra("DISTRICT", districts.get(position));
                        intent.putExtra("PROVINCE", province);
                        startActivity(intent);
                    }
                }
            });
        } else {
            mDistrictAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
                if (mUser != null) {

                    Intent intentCreateApartment = new Intent(DistrictActivity.this, CreateApartmentActivity.class);
                    intentCreateApartment.putExtra("USER", mUser);
                    intentCreateApartment.putExtra("PROVINCE", province);
                    startActivity(intentCreateApartment);

                } else if (mCustomerList != null && mContractList == null) {
                    Intent intentTenantInfo = new Intent(DistrictActivity.this, TenantsInfoActivity.class);
                    intentTenantInfo.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                    intentTenantInfo.putExtra("POSITION", mPosition);
                    intentTenantInfo.putExtra("PROVINCE", province);
                    startActivity(intentTenantInfo);
                } else if (roomList != null && mContractList == null) {
                    Intent intentCreateContract = new Intent(DistrictActivity.this, CreateContractActivity.class);
                    intentCreateContract.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                    intentCreateContract.putExtra("POSITION", mPosition);
                    intentCreateContract.putExtra("PROVINCE", province);
                    startActivity(intentCreateContract);
                } else if (mContractList != null) {
                    Intent intentCreateTenants = new Intent(DistrictActivity.this, CreateTenantsActivity.class);
                    intentCreateTenants.putExtra("LIST_ROOM", (ArrayList<Room>) roomList);
                    intentCreateTenants.putExtra("LIST_CUSTOMER", (ArrayList<Customer>) mCustomerList);
                    intentCreateTenants.putExtra("LIST_CONTRACT", (ArrayList<Contract>) mContractList);
                    intentCreateTenants.putExtra("POSITION", mPosition);
                    intentCreateTenants.putExtra("PROVINCE", province);
                    startActivity(intentCreateTenants);
                } else {
                    Intent intentNewAccount = new Intent(DistrictActivity.this, AccountRegistrationActivity.class);
                    intentNewAccount.putExtra("PROVINCE", province);
                    startActivity(intentNewAccount);
                }
                break;
        }
    }

    @Override
    public void getListDistrictSuccess(List<District> districtList) {
        if (mDistrictList == null) {
            mDistrictList = new ArrayList<>();
            mDistrictList = districtList;
        } else {
            mDistrictList.addAll(districtList);
        }
        updateUI(mDistrictList);
    }

    @Override
    public void getListDistrictFail(String msgErr) {

    }
}
