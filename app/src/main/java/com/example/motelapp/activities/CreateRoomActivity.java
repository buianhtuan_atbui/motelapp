package com.example.motelapp.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.example.motelapp.R;
import com.example.motelapp.fragments.RoomFragment;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.models.Image;
import com.example.motelapp.models.Room;
import com.example.motelapp.presenters.CreateApartmentPresenter;
import com.example.motelapp.presenters.CreateRoomPresenter;
import com.example.motelapp.presenters.UploadImagePresenter;
import com.example.motelapp.utils.Permission_Access;
import com.example.motelapp.views.CreateRoomView;
import com.example.motelapp.views.UploadImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class CreateRoomActivity extends AppCompatActivity implements View.OnClickListener, CreateRoomView, UploadImageView {

    private LinearLayout mLnlItemBack, mLnlImgRoom;
    private ImageView mimgLoadImgRoom;
    private EditText mEdtRoomName, mEdtArea, mEdtRoomMoney, mEdtAcreage, mEdtDescription;
    private Button mBtnCreateRoom;

    private AwesomeValidation awesomeValidation;
    private CreateRoomPresenter mCreateRoomPresenter;
    private UploadImagePresenter mUploadImagePresenter;

    private Room room;
    private Apartment apartment;

    private String mediaPath;

    private static String roomName;
    private static String description;
    private static float price;
    private static float acreage;
    private static String url;
    private static boolean active;
    private static int apartmentId;
    private static MultipartBody.Part fileUpload;
    private static List<MultipartBody.Part> mListFile;

    private List<Image> mImageList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_room);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mEdtRoomName = findViewById(R.id.edt_room_name);
        mEdtArea = findViewById(R.id.edt_description);
        mEdtRoomMoney = findViewById(R.id.edt_room_money);
        mEdtAcreage = findViewById(R.id.edt_acreage);
        mEdtDescription = findViewById(R.id.edt_description);
        mBtnCreateRoom = findViewById(R.id.btn_create_room);
        mLnlImgRoom = findViewById(R.id.lnl_img_room);
        mimgLoadImgRoom = findViewById(R.id.img_load_img_room);
        mimgLoadImgRoom.setImageResource(R.drawable.img_room);

    }

    private void initialData() {
        mCreateRoomPresenter = new CreateRoomPresenter(getApplication(), this, this);
        mUploadImagePresenter = new UploadImagePresenter(getApplication(), this, this);
        mLnlItemBack.setOnClickListener(this);
        mBtnCreateRoom.setOnClickListener(this);
        mLnlImgRoom.setOnClickListener(this);
        apartment = (Apartment) getIntent().getSerializableExtra("APARTMENT");

    }

    public void uploadImage() {
        mListFile = new ArrayList<>();
        File file = new File(mediaPath);

        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        fileUpload = MultipartBody.Part.createFormData("files", file.getName(), requestBody);
        mListFile.add(fileUpload);
        mUploadImagePresenter.uploadImage(mListFile);
    }

    public void createRoom() {

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.edt_room_name, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_description, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_acreage, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_room_money, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_input_people, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        if (awesomeValidation.validate()) {
            roomName = mEdtRoomName.getText().toString();
            description = mEdtDescription.getText().toString();
            price = Float.parseFloat(mEdtRoomMoney.getText().toString());
            acreage = Float.parseFloat(mEdtAcreage.getText().toString());
            url = mImageList.get(0).getUrl();
            active = true;
            apartmentId = apartment.getId();
            Room room = new Room(roomName, description, price, acreage, url, active, apartmentId);
            mCreateRoomPresenter.createRoom(room);
        }


    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
                Intent intentBack = new Intent(CreateRoomActivity.this, MainHomeActivity.class);
                startActivity(intentBack);
//                setResult(1);
//                finish();
                break;
            case R.id.btn_create_room:
                uploadImage();
                break;
            case R.id.lnl_img_room:
                Permission_Access.verifyStoragePermissions(CreateRoomActivity.this);
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 0);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == 0 && resultCode == RESULT_OK && data != null) {

                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mediaPath = cursor.getString(columnIndex);
                mimgLoadImgRoom.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                // Set the Image in ImageView for Previewing the Media

                cursor.close();

            } else {
                Toast.makeText(this, "You haven't picked Image/Video", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void createRoomSuccess(Room room) {
        this.room = room;
        Toast.makeText(this, "Create Room Successfully", Toast.LENGTH_SHORT).show();
        Intent intentMainHome = new Intent(CreateRoomActivity.this, MainHomeActivity.class);
        startActivity(intentMainHome);
        finish();
    }

    @Override
    public void createRoomFail(String msgErr) {
        Toast.makeText(this, msgErr, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void uploadImageSuccessFul(List<Image> imageList) {
        this.mImageList = imageList;
        createRoom();
    }

    @Override
    public void uploadImageFail() {

    }

    //
}
