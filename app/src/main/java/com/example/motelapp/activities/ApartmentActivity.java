package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Application;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.motelapp.R;
import com.example.motelapp.adapters.ApartmentAdapter;
import com.example.motelapp.fragments.ApartmentFragment;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.presenters.ApartmentPresenter;
import com.example.motelapp.presenters.DeleteApartmentPresenter;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.room.managements.UserManagement;
import com.example.motelapp.views.ApartmentView;
import com.example.motelapp.views.DeleteApartmentView;

import java.util.ArrayList;
import java.util.List;

public class ApartmentActivity extends AppCompatActivity implements View.OnClickListener, DeleteApartmentView {

    private LinearLayout mLnlItemBack, mLnlCreateApartment;

    private List<Apartment> mApartmentList;

    private RecyclerView mRecycleApartment;
    private ApartmentAdapter mApartmentAdapter;

    private DeleteApartmentPresenter mDeleteApartmentPresenter;

    private User mUser;

    private int currentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apartment);

        initialView();
        initialData();
    }

    private void initialView() {

        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mLnlCreateApartment = findViewById(R.id.lnl_create_apartment);

        mRecycleApartment = findViewById(R.id.recycle_apartment);
        mRecycleApartment.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        mRecycleApartment.setLayoutManager(layoutManager);
    }

    private void initialData() {

        currentPosition = getIntent().getIntExtra("POSITION", currentPosition);

        mDeleteApartmentPresenter = new DeleteApartmentPresenter(getApplication(), this, this);

        mUser = (User) getIntent().getSerializableExtra("USER");

        mLnlItemBack.setOnClickListener(this);
        mLnlCreateApartment.setOnClickListener(this);

        mApartmentList = new ArrayList<>();
        mApartmentList = (ArrayList<Apartment>) getIntent().getSerializableExtra("LIST_APARTMENT");

        updateUI(mApartmentList);


    }


    public void updateUI(List<Apartment> apartments) {
        if (mApartmentAdapter == null) {
            mApartmentAdapter = new ApartmentAdapter(getApplicationContext(), apartments);
            mRecycleApartment.setAdapter(mApartmentAdapter);

            //onclick adapter
//            mApartmentAdapter.getPosition(new ApartmentAdapter.OnClickListener() {
//                @Override
//                public void onClickListener(int position) {
//                    Toast.makeText(ApartmentActivity.this, "position " + position, Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(ApartmentActivity.this, MainHomeActivity.class);
//                    intent.putExtra("POSITION", position);
//                    startActivity(intent);
//                }
//            });

            //edit
            mApartmentAdapter.getPosition(new ApartmentAdapter.OnClickListener() {
                @Override
                public void onClickListener(final int position, View view) {

                    if (view.findViewById(R.id.lnl_root_apartment_edit) != null) {
                        view.findViewById(R.id.lnl_root_apartment_edit).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Toast.makeText(ApartmentActivity.this, "edit: " + position, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(ApartmentActivity.this, UpdateApartmentActivity.class);
                                intent.putExtra("LIST_APARTMENT", (ArrayList<Apartment>) mApartmentList);
                                intent.putExtra("POSITION", position);
                                startActivity(intent);
                            }
                        });
                    }

                    if (view.findViewById(R.id.lnl_root_apartment_delete) != null) {
                        view.findViewById(R.id.lnl_root_apartment_delete).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDeleteDialog(position);

                            }
                        });
                    }

                    if (view.findViewById(R.id.lnl_root_apartment_form) != null) {
                        view.findViewById(R.id.lnl_root_apartment_form).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Toast.makeText(ApartmentActivity.this, "form: " + position, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(ApartmentActivity.this, MainHomeActivity.class);
                                intent.putExtra("POSITION", position);
                                startActivity(intent);
                            }
                        });
                    }
                }
            });
        } else {
            mApartmentAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
                Intent intentToMainHome = new Intent(ApartmentActivity.this, MainHomeActivity.class);
                intentToMainHome.putExtra("USER", mUser);
                startActivity(intentToMainHome);
                break;
            case R.id.lnl_create_apartment:

                Intent intentCreateApartment = new Intent(ApartmentActivity.this, CreateApartmentActivity.class);
                intentCreateApartment.putExtra("USER", mUser);
                startActivity(intentCreateApartment);
                break;
        }
    }

    //    show dialog delete
    public void showDeleteDialog(final int position) {
        final Dialog dialog = new Dialog(ApartmentActivity.this);
        dialog.setContentView(R.layout.dialog_delete_apartment);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button buttonOk = dialog.findViewById(R.id.btn_ok_delete_apartment);
        Button buttonNo = dialog.findViewById(R.id.btn_No_delete_apartment);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDeleteApartmentPresenter.deleteApartment(mApartmentList.get(position).getId());
                if (position == currentPosition) {
                    if (mApartmentList.size() == currentPosition + 1) {
                        currentPosition = 0;
                    } else {
                        currentPosition = position + 1;
                    }
                }
                dialog.dismiss();
            }
        });
        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void deleteApartmentSuccessful(Apartment apartment) {
        for (int i = 0; i < mApartmentList.size(); i++) {
            if (mApartmentList.get(i).getId() == apartment.getId()) {
                mApartmentList.remove(i);
            }
        }
        Intent intent = new Intent(ApartmentActivity.this, MainHomeActivity.class);
        intent.putExtra("LIST_APARTMENT", (ArrayList<Apartment>) mApartmentList);
        intent.putExtra("POSITION", currentPosition);
        startActivity(intent);
    }

    @Override
    public void deleteApartmentFail() {

    }
}
