package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.motelapp.R;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.networks.CheckNetwork;
import com.example.motelapp.presenters.ApartmentPresenter;
import com.example.motelapp.presenters.LoginPresenter;
import com.example.motelapp.room.entities.User;
import com.example.motelapp.views.ApartmentView;
import com.example.motelapp.views.LoginView;

import java.util.ArrayList;
import java.util.List;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener, LoginView, ApartmentView {

    private Button mBtnLogin, mBtnForgetPass, mBtnAccountRegister;
    private EditText mEdtPhoneNumber, mEdtPassword;
    private LoginPresenter mLoginPresenter;
    private String mPhoneNumber, mPassword;
    private Intent intentMainHome;

    private ApartmentPresenter mApartmentPresenter;
    private List<Apartment> apartmentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initialView();
        initialData();
    }

    private void initialView() {
        mBtnLogin = findViewById(R.id.btn_login);
        mBtnForgetPass = findViewById(R.id.btn_forget_pass);
        mBtnAccountRegister = findViewById(R.id.btn_account_register);
        mEdtPhoneNumber = findViewById(R.id.edt_phone_number);
        mEdtPassword = findViewById(R.id.edt_password);
        mApartmentPresenter = new ApartmentPresenter(getApplication(), this, this);
    }

    private void initialData() {
        mBtnLogin.setOnClickListener(LoginActivity.this);
        mBtnForgetPass.setOnClickListener(LoginActivity.this);
        mBtnAccountRegister.setOnClickListener(LoginActivity.this);

        mLoginPresenter = new LoginPresenter(getApplication(), this, this);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i){
            case R.id.btn_login:
                if (CheckNetwork.isInternetAvailable(LoginActivity.this)) {
                    checkLogin();
                } else {
//                    Toast.makeText(this, "ket noi internet khg thanh cong", Toast.LENGTH_SHORT).show();
                    showNetworkDialog();
                }
                break;
            case R.id.btn_forget_pass:
                Intent intentForgetPass = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intentForgetPass);
                break;
            case R.id.btn_account_register:
                Intent intentNewAccount = new Intent(LoginActivity.this, AccountRegistrationActivity.class);
                startActivity(intentNewAccount);
                break;
        }
    }

    //check login
    private void checkLogin() {
        mPhoneNumber = mEdtPhoneNumber.getText().toString();
        mPassword = mEdtPassword.getText().toString();
        mLoginPresenter.login(mPhoneNumber, mPassword);
    }

//    public static void intentToHome(Activity activity) {
//        Intent intentMainHome = new Intent(activity, MainHomeActivity.class);
//        activity.startActivity(intentMainHome);
//    }

    @Override
    public void loginSuccess(User user) {
        Toast.makeText(this, "Login thanh cong (LoginActivity)", Toast.LENGTH_SHORT).show();
        intentMainHome = new Intent(LoginActivity.this, MainHomeActivity.class);
        intentMainHome.putExtra("USER", user);
        mApartmentPresenter.getListApartment(user.getUserId());

        finish();
    }

    @Override
    public void loginFail(String msgLoginFail) {

    }

    //intent to mainhome
    public static void intentToMainHome(Activity activity) {
        Intent intentMainHome = new Intent(activity, MainHomeActivity.class);
        activity.startActivity(intentMainHome);
    }

    //intent activity
    public static void intentToLoginActivity(Activity activity) {
        Intent intentLogin = new Intent(activity, LoginActivity.class);
        activity.startActivity(intentLogin);
    }

    @Override
    public void getListApartmentSuccess(List<Apartment> apartmentList) {
        this.apartmentList = apartmentList;
        intentMainHome.putExtra("LIST_APARTMENT", (ArrayList<Apartment>) apartmentList);
        startActivity(intentMainHome);

    }

    @Override
    public void getListApartmentFail(String msgGetListApartment) {

    }

    //    show dialog network
    public void showNetworkDialog() {
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.setContentView(R.layout.dialog_network_fail);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button buttonOk = dialog.findViewById(R.id.btn_ok_login);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
