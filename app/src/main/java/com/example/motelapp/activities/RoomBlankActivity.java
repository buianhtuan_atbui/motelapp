package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.motelapp.R;
import com.example.motelapp.adapters.RoomBlankAdapter;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.models.Room;

import java.util.ArrayList;
import java.util.List;

public class RoomBlankActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout mLnlItemBack;
    private RecyclerView mRecycleRoomBlank;
    private RoomBlankAdapter mRoomBlankAdapter;

    private List<Room> roomList;
    private List<Apartment> mApartmentList;
    boolean check = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_blank);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mRecycleRoomBlank = findViewById(R.id.recycle_room_blank);
        mRecycleRoomBlank.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        mRecycleRoomBlank.setLayoutManager(layoutManager);

    }

    private void initialData() {
        check = getIntent().getBooleanExtra("CHECK", check);
        mLnlItemBack.setOnClickListener(this);
        roomList = new ArrayList<>();
        roomList = (ArrayList<Room>) getIntent().getSerializableExtra("LIST_ROOM");
        mApartmentList = (ArrayList<Apartment>) getIntent().getSerializableExtra("LIST_APARTMENT");
        for (int i = 0; i < roomList.size(); i++){
            if (!roomList.get(i).isActive()){
                roomList.remove(i);
                i = i -1;
            }
        }
        updateUI(roomList);
    }

    public void updateUI(final List<Room> roomList) {
        if (mRoomBlankAdapter == null) {
            mRoomBlankAdapter = new RoomBlankAdapter(getApplicationContext(), roomList);
            mRecycleRoomBlank.setAdapter(mRoomBlankAdapter);
            mRoomBlankAdapter.getPosition(new RoomBlankAdapter.OnClickListener() {
                @Override
                public void onClickListener(final int position) {
                    if (!check){
                        Intent intent = new Intent(RoomBlankActivity.this, CreateContractActivity.class);
                        intent.putExtra("LIST_ROOM", (ArrayList<Room>)roomList);
                        intent.putExtra("POSITION", position);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(RoomBlankActivity.this, CreateNewsActivity.class);
                        intent.putExtra("LIST_ROOM", (ArrayList<Room>)roomList);
                        intent.putExtra("POSITION", position);
                        intent.putExtra("LIST_APARTMENT" ,(ArrayList<Apartment>) mApartmentList);
                        startActivity(intent);
                    }
                }
            });
        } else {
            mRoomBlankAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
                Intent intentToHome = new Intent(RoomBlankActivity.this, MainHomeActivity.class);
//                startActivity(intentToHome);
                setResult(1);
                finish();
                break;
        }
    }
}
