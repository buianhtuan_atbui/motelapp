package com.example.motelapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.example.motelapp.R;
import com.example.motelapp.models.Apartment;
import com.example.motelapp.models.District;
import com.example.motelapp.models.Province;
import com.example.motelapp.models.Wards;
import com.example.motelapp.presenters.UpdateApartmentPresenter;
import com.example.motelapp.views.UpdateApartmentView;

import java.util.ArrayList;
import java.util.List;

public class UpdateApartmentActivity extends AppCompatActivity implements View.OnClickListener, UpdateApartmentView {

    private LinearLayout mLnlItemBack, mLnlChoiceTypeApartment, mLnlProvince, mLnlDistrict, mLnlWards;
    private TextView mTxtTypeApartmentShow, mTxtProvince, mTxtDistrict, mTxtWards;
    private EditText mEdtName, mEdtContact, mEdtElectricity, mEdtWater, mEdtAddressDetail;
    private Button mBtnUpdateApartment;

    private static Province province;
    private static District district;
    private static Wards wards;

    private static String name = "";
    private static String address = "";
    private static String contact = "";
    private static boolean active = true;
    private static String type = "";
    private static float electricity = 0;
    private static float water = 0;
    private static int account_id;

    private AwesomeValidation awesomeValidation;

    private UpdateApartmentPresenter mUpdateApartmentPresenter;

    private List<Apartment> mApartmentList;
    private int position;
    private Apartment mApartment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_apartment);

        initialView();
        initialData();
    }

    private void initialView() {
        mLnlItemBack = findViewById(R.id.lnl_item_back);
        mLnlChoiceTypeApartment = findViewById(R.id.lnl_choice_type_apartment);
        mTxtTypeApartmentShow = findViewById(R.id.txt_type_apartment_show);

        mLnlProvince = findViewById(R.id.lnl_province);
        mLnlDistrict = findViewById(R.id.lnl_district);
        mLnlWards = findViewById(R.id.lnl_wards);
        mTxtProvince = findViewById(R.id.txt_province);
        mTxtDistrict = findViewById(R.id.txt_district);
        mTxtWards = findViewById(R.id.txt_wards);

        mEdtName = findViewById(R.id.edt_apartment_name);
        mEdtElectricity = findViewById(R.id.edt_apartment_electricity);
        mEdtWater = findViewById(R.id.edt_apartment_water);
        mEdtAddressDetail = findViewById(R.id.edt_apartment_address_detail);
        mEdtContact = findViewById(R.id.edt_apartment_contact);

        mBtnUpdateApartment = findViewById(R.id.btn_update_apartment);
    }

    public void setData() {
        name = mEdtName.getText().toString();
        contact = mEdtContact.getText().toString();
        active = true;
        type = mTxtTypeApartmentShow.getText().toString();
        electricity = (mEdtElectricity.getText().toString().trim().length() > 0) ? Float.parseFloat(mEdtElectricity.getText().toString()) : 0;
        water = (mEdtWater.getText().toString().trim().length() > 0) ? Float.parseFloat(mEdtWater.getText().toString()) : 0;
        account_id = mApartment.getAccount_id();
    }

    private void initialData() {
        mApartmentList = (ArrayList<Apartment>) getIntent().getSerializableExtra("LIST_APARTMENT");
        position = getIntent().getIntExtra("POSITION", position);
        mApartment = mApartmentList.get(position);
        mUpdateApartmentPresenter = new UpdateApartmentPresenter(getApplication(), this, this);
        mLnlItemBack.setOnClickListener(this);
        mLnlChoiceTypeApartment.setOnClickListener(this);

        mLnlProvince.setOnClickListener(this);
        mLnlDistrict.setOnClickListener(this);
        mLnlWards.setOnClickListener(this);

        mBtnUpdateApartment.setOnClickListener(this);

        address = mApartment.getAddress();

        province = (Province) getIntent().getSerializableExtra("PROVINCE");
        district = (District) getIntent().getSerializableExtra("DISTRICT");
        wards = (Wards) getIntent().getSerializableExtra("WARDS");
        String provinceName = (province != null) ? province.getName() : address.split(",")[3];
        String districtName = (district != null) ? district.getName() : address.split(",")[2];
        String wardsName = (wards != null) ? wards.getName() : address.split(",")[1];
        mTxtProvince.setText(provinceName);
        mTxtDistrict.setText(districtName);
        mTxtWards.setText(wardsName);
        mEdtName.setText((name.trim().length() > 0) ? name : mApartment.getName());
        mEdtContact.setText((contact.trim().length() > 0) ? contact : String.valueOf(mApartment.getContact()));
        mEdtWater.setText(String.valueOf(mApartment.getWater()));
        mEdtElectricity.setText(String.valueOf(mApartment.getElectricity()));
        mEdtAddressDetail.setText(address.split(",")[0]);

    }

    public void updateApartment() {
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.edt_apartment_name, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_apartment_water, RegexTemplate.NOT_EMPTY, R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.edt_apartment_electricity, RegexTemplate.NOT_EMPTY, R.string.nameerror);

        if (awesomeValidation.validate()) {
            setData();
            address = mEdtAddressDetail.getText().toString() + ", " + mTxtWards.getText().toString() + ", " + mTxtDistrict.getText().toString() + ", " + mTxtProvince.getText().toString();
            Apartment apartment = new Apartment(name, address, Integer.parseInt(contact), electricity, water, type, account_id, active);
            mUpdateApartmentPresenter.updateApartment(apartment);
        }
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i) {
            case R.id.lnl_item_back:
                Intent intentBack = new Intent(this, MainHomeActivity.class);
//                startActivity(intentBack);
                setResult(1);
                finish();
                break;
            case R.id.lnl_choice_type_apartment:
                showTypeApartmentDialog();
                break;
            case R.id.lnl_province:
                Intent intentToProvince = new Intent(UpdateApartmentActivity.this, ProvinceActivity.class);
                intentToProvince.putExtra("LIST_APARTMENT", (ArrayList<Apartment>) mApartmentList);
                intentToProvince.putExtra("POSITION", position);
                setData();
                startActivity(intentToProvince);
                break;
            case R.id.lnl_district:
                if (mTxtProvince.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Vui lòng chọn tỉnh", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intentToDistrict = new Intent(UpdateApartmentActivity.this, DistrictActivity.class);
                    intentToDistrict.putExtra("APARTMENT", mApartment);
                    intentToDistrict.putExtra("PROVINCE", province);
                    intentToDistrict.putExtra("LIST_APARTMENT", (ArrayList<Apartment>) mApartmentList);
                    intentToDistrict.putExtra("POSITION", position);
                    setData();
                    startActivity(intentToDistrict);
                }
                break;
            case R.id.lnl_wards:
                if (mTxtDistrict.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Vui lòng chọn tỉnh, huyện", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intentToWards = new Intent(UpdateApartmentActivity.this, WardsActivity.class);
                    intentToWards.putExtra("LIST_APARTMENT", (ArrayList<Apartment>) mApartmentList);
                    intentToWards.putExtra("POSITION", position);
                    intentToWards.putExtra("DISTRICT", district);
                    intentToWards.putExtra("PROVINCE", province);
                    setData();
                    startActivity(intentToWards);
                }
                break;
            case R.id.btn_update_apartment:
                updateApartment();
                break;
        }
    }
    private void showTypeApartmentDialog() {
        final Dialog dialog = new Dialog(UpdateApartmentActivity.this);
        dialog.setContentView(R.layout.dialog_choice_type_apartment);
//        dialog.setTitle("Chọn loại nhà trọ");
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.YELLOW));
        //gender
        TextView mTxtType1, mTxtType2, mTxtType3;

        mTxtType1 = dialog.findViewById(R.id.txt_type_1);
        mTxtType2 = dialog.findViewById(R.id.txt_type_2);
        mTxtType3 = dialog.findViewById(R.id.txt_type_3);

        //gender onclick
        mTxtType1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtTypeApartmentShow.setText("Nhà cấp 4");
                dialog.dismiss();
            }
        });
        mTxtType2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtTypeApartmentShow.setText("Phòng trọ");
                dialog.dismiss();
            }
        });
        mTxtType3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTxtTypeApartmentShow.setText("Chung cư");
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    @Override
    public void updateApartmentSuccessful(Apartment apartment) {
        Intent intent = new Intent(UpdateApartmentActivity.this, MainHomeActivity.class);
        startActivity(intent);
    }

    @Override
    public void updateApartmentFail() {

    }
}
