package com.example.motelapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Wards implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("district_id")
    private int district_id;

    @SerializedName("name")
    private String name;

    @SerializedName("type")
    private String type;

    public Wards(int id, int district_id, String name, String type) {
        this.id = id;
        this.district_id = district_id;
        this.name = name;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(int district_id) {
        this.district_id = district_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
