package com.example.motelapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import okhttp3.MultipartBody;

public class Image implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("link")
    private String url;
    @SerializedName("name")
    private String name;
    @SerializedName("new_id")
    private int newsId;

    public Image(String name, int newsId) {
        this.name = name;
        this.newsId = newsId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
