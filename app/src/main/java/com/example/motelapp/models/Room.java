package com.example.motelapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Room implements Serializable  {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("raw_price")
    private float rawPrice;
    @SerializedName("area")
    private float area;
    @SerializedName("image")
    private String image;
    @SerializedName("active")
    private boolean active;
    @SerializedName("apartment_id")
    private int apartmentId;

    public Room() {
    }

    public Room(String name, String description, float rawPrice, float area, String image, boolean active, int apartmentId) {
        this.name = name;
        this.description = description;
        this.rawPrice = rawPrice;
        this.area = area;
        this.image = image;
        this.active = active;
        this.apartmentId = apartmentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getRawPrice() {
        return rawPrice;
    }

    public void setRawPrice(float rawPrice) {
        this.rawPrice = rawPrice;
    }

    public float getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(int apartmentId) {
        this.apartmentId = apartmentId;
    }
}
