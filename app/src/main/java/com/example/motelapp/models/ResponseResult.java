package com.example.motelapp.models;

public class ResponseResult<T> {
    public DataResponse<T> data;

    public ResponseResult(){}

    public DataResponse<T> getData() {
        return data;
    }

    public void setData(DataResponse<T> data) {
        this.data = data;
    }
}
