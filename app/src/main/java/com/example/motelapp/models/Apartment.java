package com.example.motelapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Apartment implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("address")
    private String address;
    @SerializedName("contact")
    private int contact;
    @SerializedName("electricity")
    private float electricity;
    @SerializedName("water")
    private float water;
    @SerializedName("type")
    private String type;
    @SerializedName("account_id")
    private int account_id;
    @SerializedName("active")
    private boolean active;

    public Apartment() {
    }

    public Apartment(String name, String address, int contact, float electricity, float water, String type, int account_id, boolean active) {
        this.name = name;
        this.address = address;
        this.contact = contact;
        this.electricity = electricity;
        this.water = water;
        this.type = type;
        this.account_id = account_id;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getContact() {
        return contact;
    }

    public void setContact(int contact) {
        this.contact = contact;
    }

    public float getElectricity() {
        return electricity;
    }

    public void setElectricity(float electricity) {
        this.electricity = electricity;
    }

    public float getWater() {
        return water;
    }

    public void setWater(float water) {
        this.water = water;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getAccount_id() {
        return account_id;
    }

    public void setAccount_id(int account_id) {
        this.account_id = account_id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}

