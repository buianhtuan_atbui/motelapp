package com.example.motelapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Customer implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("address")
    private String address;
    @SerializedName("name")
    private String name;
    @SerializedName("personal_id_card")
    private String personal_id_card;
    @SerializedName("phone")
    private String phone;
    @SerializedName("sex")
    private String sex;
    @SerializedName("yob")
    private String yob;
    @SerializedName("active")
    private boolean active;
    @SerializedName("contract_id")
    private int contract_id;

    public Customer(String address, String name, String personal_id_card, String phone, String sex, String yob, boolean active, int contract_id) {
        this.address = address;
        this.name = name;
        this.personal_id_card = personal_id_card;
        this.phone = phone;
        this.sex = sex;
        this.yob = yob;
        this.active = active;
        this.contract_id = contract_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersonal_id_card() {
        return personal_id_card;
    }

    public void setPersonal_id_card(String personal_id_card) {
        this.personal_id_card = personal_id_card;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getYob() {
        return yob;
    }

    public void setYob(String yob) {
        this.yob = yob;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getContract_id() {
        return contract_id;
    }

    public void setContract_id(int contract_id) {
        this.contract_id = contract_id;
    }
}
