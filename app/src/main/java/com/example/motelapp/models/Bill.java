package com.example.motelapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Bill implements Serializable {

    @SerializedName("id")
    private String id;
    @SerializedName("create_date")
    private String createDate;
    @SerializedName("end_electricity")
    private float endElectricity;
    @SerializedName("end_water")
    private float endWater;
    @SerializedName("total")
    private float total;
    @SerializedName("contract_id")
    private int contractId;

    public Bill(String createDate, float endElectricity, float endWater, float total, int contractId) {
        this.createDate = createDate;
        this.endElectricity = endElectricity;
        this.endWater = endWater;
        this.total = total;
        this.contractId = contractId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public float getEndElectricity() {
        return endElectricity;
    }

    public void setEndElectricity(float endElectricity) {
        this.endElectricity = endElectricity;
    }

    public float getEndWater() {
        return endWater;
    }

    public void setEndWater(float endWater) {
        this.endWater = endWater;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getContractId() {
        return contractId;
    }

    public void setContractId(int contractId) {
        this.contractId = contractId;
    }
}
