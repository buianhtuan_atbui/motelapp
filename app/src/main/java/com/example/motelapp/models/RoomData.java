package com.example.motelapp.models;

public class RoomData {
    private int mImgRoom;
    private String mRoomOwnerName;
    private double mPrice;
    private String mTenants;
    private String mAcreage;

    public RoomData(int mImgRoom, String mRoomOwnerName, double mPrice, String mTenants, String mAcreage) {
        this.mImgRoom = mImgRoom;
        this.mRoomOwnerName = mRoomOwnerName;
        this.mPrice = mPrice;
        this.mTenants = mTenants;
        this.mAcreage = mAcreage;
    }

    public int getmImgRoom() {
        return mImgRoom;
    }

    public void setmImgRoom(int mImgRoom) {
        this.mImgRoom = mImgRoom;
    }

    public String getmRoomOwnerName() {
        return mRoomOwnerName;
    }

    public void setmRoomOwnerName(String mRoomOwnerName) {
        this.mRoomOwnerName = mRoomOwnerName;
    }

    public double getmPrice() {
        return mPrice;
    }

    public void setmPrice(double mPrice) {
        this.mPrice = mPrice;
    }

    public String getmTenants() {
        return mTenants;
    }

    public void setmTenants(String mTenants) {
        this.mTenants = mTenants;
    }

    public String getmAcreage() {
        return mAcreage;
    }

    public void setmAcreage(String mAcreage) {
        this.mAcreage = mAcreage;
    }
}
