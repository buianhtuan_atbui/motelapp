package com.example.motelapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.sql.Date;

public class Contract implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("start_date")
    private String startDate;
    @SerializedName("end_date")
    private String endDate;
    @SerializedName("deposit")
    private int deposit;
    @SerializedName("payment_cycle")
    private int payment_cycle;
    @SerializedName("payment_day")
    private int payment_day;
    @SerializedName("contract_code")
    private String contractCode;
    @SerializedName("representator")
    private String representator;
    @SerializedName("number_of_people")
    private int numberOfCustomer;
    @SerializedName("number_of_bike")
    private int numberOfBike;
    @SerializedName("current_electricity")
    private float currentElectricity;
    @SerializedName("current_water")
    private float currentWater;
    @SerializedName("room_id")
    private int roomId;
    @SerializedName("active")
    private boolean active;

    public Contract() {
    }

    public Contract(String startDate, String endDate, int deposit, int payment_cycle, int payment_day, String contractCode, String representator, int numberOfCustomer, int numberOfBike, float currentElectricity, float currentWater, int roomId, boolean active) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.deposit = deposit;
        this.payment_cycle = payment_cycle;
        this.payment_day = payment_day;
        this.contractCode = contractCode;
        this.representator = representator;
        this.numberOfCustomer = numberOfCustomer;
        this.numberOfBike = numberOfBike;
        this.currentElectricity = currentElectricity;
        this.currentWater = currentWater;
        this.roomId = roomId;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public int getPayment_cycle() {
        return payment_cycle;
    }

    public void setPayment_cycle(int payment_cycle) {
        this.payment_cycle = payment_cycle;
    }

    public int getPayment_day() {
        return payment_day;
    }

    public void setPayment_day(int payment_day) {
        this.payment_day = payment_day;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public String getRepresentator() {
        return representator;
    }

    public void setRepresentator(String representator) {
        this.representator = representator;
    }

    public int getNumberOfCustomer() {
        return numberOfCustomer;
    }

    public void setNumberOfCustomer(int numberOfCustomer) {
        this.numberOfCustomer = numberOfCustomer;
    }

    public int getNumberOfBike() {
        return numberOfBike;
    }

    public void setNumberOfBike(int numberOfBike) {
        this.numberOfBike = numberOfBike;
    }

    public float getCurrentElectricity() {
        return currentElectricity;
    }

    public void setCurrentElectricity(float currentElectricity) {
        this.currentElectricity = currentElectricity;
    }

    public float getCurrentWater() {
        return currentWater;
    }

    public void setCurrentWater(float currentWater) {
        this.currentWater = currentWater;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
