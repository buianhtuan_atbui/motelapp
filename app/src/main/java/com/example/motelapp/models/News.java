package com.example.motelapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class News implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("contents")
    private String contents;
    @SerializedName("create_date")
    private String createDate;
    @SerializedName("title")
    private String tittle;
    @SerializedName("status")
    private int status;
    @SerializedName("active")
    private boolean active;
    @SerializedName("room_id")
    private int room_Id;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public News(String contents, String createDate, String tittle, int room_Id, boolean active) {
        this.contents = contents;
        this.createDate = createDate;
        this.tittle = tittle;
        this.room_Id = room_Id;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public int getRoom_Id() {
        return room_Id;
    }

    public void setRoom_Id(int room_Id) {
        this.room_Id = room_Id;
    }
}
