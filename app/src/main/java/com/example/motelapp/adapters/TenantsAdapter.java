package com.example.motelapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.motelapp.R;
import com.example.motelapp.models.Customer;
import com.example.motelapp.models.Tenants;

import java.util.List;

public class TenantsAdapter extends RecyclerView.Adapter<TenantsAdapter.ViewHolder> {

    private Context mContext;
    private List<Customer> mTenantsList;
    private OnClickListener mOnClickListener;

    public TenantsAdapter(Context mContext, List<Customer> mTenantsList) {
        this.mContext = mContext;
        this.mTenantsList = mTenantsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.item_row_tenants, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

//        holder.mImgTenants.setImageResource();
       holder.mTxtNameTenants.setText(mTenantsList.get(position).getName());
       holder.mTxtPhoneNumberTenants.setText(mTenantsList.get(position).getPhone());

       holder.mLnlRootTenantsForm.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if(mOnClickListener != null) {
                   mOnClickListener.onClickListener(position);
               }
           }
       });

    }

    @Override
    public int getItemCount() {
        int count = (mTenantsList != null) ? mTenantsList.size() : 0;
        return count;
    }


    public interface OnClickListener {
        void onClickListener(int position);
    }

    public void getPosition(OnClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImgTenants;
        TextView mTxtNameTenants, mTxtPhoneNumberTenants;
        LinearLayout mLnlRootTenantsForm;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mImgTenants = itemView.findViewById(R.id.img_tenants);
            mTxtNameTenants = itemView.findViewById(R.id.txt_name_tenants);
            mTxtPhoneNumberTenants = itemView.findViewById(R.id.txt_phone_number_tenants);
            mLnlRootTenantsForm = itemView.findViewById(R.id.lnl_root_tenants_form);
        }
    }
}
