package com.example.motelapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.motelapp.R;
import com.example.motelapp.models.Apartment;

import java.util.List;

public class ApartmentAdapter extends RecyclerView.Adapter<ApartmentAdapter.Viewholder> {
    private Context mContext;
    private List<Apartment> mListApartment;
    private OnClickListener mOnClickListener;

    public ApartmentAdapter(Context mContext, List<Apartment> mApartment) {
        this.mContext = mContext;
        this.mListApartment = mApartment;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.item_row_apartment, parent, false);
        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final Viewholder holder, final int position) {
        holder.mTxtApartmentName.setText(mListApartment.get(position).getName());
        holder.mTxtApartmentAddress.setText(mListApartment.get(position).getAddress());

        //onclick adapter
        holder.mLnlRootApartment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mOnClickListener != null) {
                    mOnClickListener.onClickListener(position, view);
                }
            }
        });
//        holder.mLnlRootApartmentDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mOnClickListener != null){
//                    mOnClickListener.onClickListener(position , view);
//                }
//            }
//        });


    }

    @Override
    public int getItemCount() {
        int count = (mListApartment != null) ? mListApartment.size() : 0;
        return count;
    }

    //interface onclick adapter
    public interface OnClickListener {
        void onClickListener(int position, View view);
    }

    public void getPosition(OnClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }


    public class Viewholder extends RecyclerView.ViewHolder {
        TextView mTxtApartmentName, mTxtApartmentAddress;
        LinearLayout mLnlRootApartment, mLnlRootApartmentForm, mLnlRootApartmentEdit, mLnlRootApartmentDelete;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            mTxtApartmentName = itemView.findViewById(R.id.txt_apartment_name);
            mTxtApartmentAddress = itemView.findViewById(R.id.txt_apartment_address);
            mLnlRootApartment = itemView.findViewById(R.id.lnl_root_apartment);
            mLnlRootApartmentForm = itemView.findViewById(R.id.lnl_root_apartment_form);
            mLnlRootApartmentEdit = itemView.findViewById(R.id.lnl_root_apartment_edit);
            mLnlRootApartmentDelete = itemView.findViewById(R.id.lnl_root_apartment_delete);
        }
    }
}

