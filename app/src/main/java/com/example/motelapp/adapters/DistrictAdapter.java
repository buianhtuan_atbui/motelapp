package com.example.motelapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.motelapp.R;
import com.example.motelapp.models.District;
import com.example.motelapp.models.Province;

import java.util.List;

public class DistrictAdapter extends RecyclerView.Adapter<DistrictAdapter.ViewHolder> {
    private Context mContext;
    private List<District> mDistrictList;
    private OnClickListener mOnClickListener;

    public DistrictAdapter(Context mContext, List<District> mDistrictList) {
        this.mContext = mContext;
        this.mDistrictList = mDistrictList;
    }

    @NonNull
    @Override
    public DistrictAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.item_row_district, parent, false);
        return new  ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DistrictAdapter.ViewHolder holder, final int position) {
        holder.mTxtDistrict.setText(mDistrictList.get(position).getName());

        //        onclick adapter
        holder.mLnl_root_district.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mOnClickListener != null) {
                    mOnClickListener.onClickListener(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        int count = (mDistrictList != null) ? mDistrictList.size() : 0;
        return count;
    }

    //onclick interface
    public interface OnClickListener {
        void onClickListener(int position);
    }

    public void getPosition(OnClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTxtDistrict;
        LinearLayout mLnl_root_district;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTxtDistrict = itemView.findViewById(R.id.txt_district);
            mLnl_root_district = itemView.findViewById(R.id.lnl_root_district);
        }
    }
}
