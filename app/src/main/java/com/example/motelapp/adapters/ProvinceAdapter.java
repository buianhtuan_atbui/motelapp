package com.example.motelapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.motelapp.R;
import com.example.motelapp.models.Province;

import java.util.List;

public class ProvinceAdapter extends RecyclerView.Adapter<ProvinceAdapter.ViewHolder> {
    private Context mContext;
    private List<Province> mProvinceList;
    private OnClickListener mOnClickListener;

    public ProvinceAdapter(Context mContext, List<Province> mProvinceList) {
        this.mContext = mContext;
        this.mProvinceList = mProvinceList;
    }

    @NonNull
    @Override
    public ProvinceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.item_row_province, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProvinceAdapter.ViewHolder holder, final int position) {
        holder.mTxtProvince.setText(mProvinceList.get(position).getName());

//        onclick adapter
        holder.mLnlRootProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mOnClickListener != null) {
                    mOnClickListener.onClickListener(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        int count = (mProvinceList != null) ? mProvinceList.size() : 0;
        return count;
    }

    //interface onclick adapter
    public interface OnClickListener {
        void onClickListener(int position);
    }

    public void getPosition(OnClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTxtProvince;
        LinearLayout mLnlRootProvince;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTxtProvince = itemView.findViewById(R.id.txt_province);
            mLnlRootProvince = itemView.findViewById(R.id.lnl_root_province);
        }
    }
}
