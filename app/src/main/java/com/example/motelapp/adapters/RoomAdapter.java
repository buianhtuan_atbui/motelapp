package com.example.motelapp.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.example.motelapp.R;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.Room;
import com.example.motelapp.models.RoomData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.ViewHolder> {

    private Context mContext;
    private List<Room> mListRoom;
    private List<Contract> mListContract;
    private OnClickListener mOnClickListener;

    public RoomAdapter(Context mContext, List<Room> mListRoom, List<Contract> mListContract) {
        this.mContext = mContext;
        this.mListRoom = mListRoom;
        this.mListContract = mListContract;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.item_row_room, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Contract contract = new Contract();
        for (Contract info: mListContract) {
            if (info.getRoomId() == mListRoom.get(position).getId() && info.isActive()) {
                contract = info;
            }
        }
//        holder.mImgRoom.setImageResource(R.mipmap.img_room);
        holder.mNameRoomOwner.setText(mListRoom.get(position).getName());
        holder.mPrice.setText(String.valueOf(mListRoom.get(position).getRawPrice() + " VNĐ"));
        holder.mTenants.setText(contract.getNumberOfCustomer() + " Khách thuê");
        holder.mAcreage.setText(mListRoom.get(position).getArea() + " M2");
        boolean status = mListRoom.get(position).isActive();
        if(status == true) {
            holder.mTxtStatusRoom.setText("Chưa ở");
            holder.mTxtStatusRoom.setBackgroundResource(R.drawable.bg_txt_border);
            holder.mTxtStatusRoom.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        } else {
            holder.mTxtStatusRoom.setText("Đang ở");
            holder.mTxtStatusRoom.setBackgroundResource(R.drawable.bg_txt_border);
            holder.mTxtStatusRoom.setTextColor(ContextCompat.getColor(mContext, R.color.turquoise));
        }

        if(mListRoom.get(position).getImage() != null) {
            Picasso.Builder builder = new Picasso.Builder(mContext);
            builder.build().load("http://" + mListRoom.get(position).getImage())
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_background)
                    .fit()
                    .centerInside()
                    .into(holder.mImgRoom);
        } else {
            holder.mImgRoom.setImageResource(R.drawable.img_room);
        }


// onclick room
        holder.mLnlRootRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mOnClickListener != null) {
                    mOnClickListener.onClickListener(position);
                }
            }
        });

//        holder.mImgRoom.setImageResource(mListRoom.get(position).getmImgRoom());
    }

    @Override
    public int getItemCount() {
        int count = (mListRoom != null) ? mListRoom.size() : 0;
        return count;
    }

    //OnClickListener interface
    public interface OnClickListener {
        void onClickListener(int position);
    }
    public void getPosition(OnClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImgRoom;
        TextView mNameRoomOwner, mPrice, mTenants, mAcreage, mTxtStatusRoom;
        LinearLayout mLnlRootRoom;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mImgRoom = itemView.findViewById(R.id.img_room_picture);
            mNameRoomOwner = itemView.findViewById(R.id.txt_room_owner_name);
            mPrice = itemView.findViewById(R.id.txt_room_price);
            mTenants = itemView.findViewById(R.id.txt_room_tenants);
            mAcreage = itemView.findViewById(R.id.txt_room_acreage);
            mLnlRootRoom = itemView.findViewById(R.id.lnl_root_room);
            mTxtStatusRoom = itemView.findViewById(R.id.txt_status_room);
        }
    }
}
