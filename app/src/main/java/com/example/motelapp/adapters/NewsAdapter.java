package com.example.motelapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.motelapp.R;
import com.example.motelapp.models.Contract;
import com.example.motelapp.models.News;
import com.example.motelapp.models.Room;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private Context mContext;
    private List<News> mNewsList;

    public NewsAdapter(Context mContext, List<News> mNewsList) {
        this.mContext = mContext;
        this.mNewsList = mNewsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.item_row_news, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = dateFormat.parse(mNewsList.get(position).getCreateDate());//You will get date object relative to server/client timezone wherever it is parsed
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); //If you need time just put specific format for time like 'HH:mm:ss'
            String dateStr = formatter.format(date);
//            Date date = new Date(mNewsList.get(position).getCreateDate());
            holder.mTxtTitle.setText(mNewsList.get(position).getTittle());
            holder.mTxtCreateDate.setText(dateStr);
        } catch (Exception ex){
                ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        int count = (mNewsList != null) ? mNewsList.size() : 0;
        return count;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTxtTitle, mTxtCreateDate;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mTxtTitle = itemView.findViewById(R.id.txt_title);
            mTxtCreateDate = itemView.findViewById(R.id.txt_create_date);
        }
    }
}
