package com.example.motelapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.motelapp.R;
import com.example.motelapp.models.Room;

import java.util.List;

public class RoomBlankAdapter extends RecyclerView.Adapter<RoomBlankAdapter.ViewHolder>{

    private Context mContext;
    private List<Room> mListRoom;
    private OnClickListener mOnClickListener;

    public RoomBlankAdapter(Context mContext, List<Room> mListRoom) {
        this.mContext = mContext;
        this.mListRoom = mListRoom;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.item_row_room_blank, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.mNameRoomOwner.setText(mListRoom.get(position).getName());
        holder.mPrice.setText(mListRoom.get(position).getRawPrice() + " vnd");
        holder.mTenants.setText("0 khách thuê");
        holder.mAcreage.setText(mListRoom.get(position).getArea() + " m2");
        holder.mLnlRoomBlankForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mOnClickListener != null) {
                    mOnClickListener.onClickListener(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (mListRoom.size() > 0){
            for (Room room:mListRoom
            ) {
                if (room.isActive()){
                    count++;
                }
            }
        }
        return count;
    }

    public interface OnClickListener {
        void onClickListener(int position);
    }

    public void getPosition(OnClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImgRoom;
        TextView mNameRoomOwner, mPrice, mTenants, mAcreage;
        LinearLayout mLnlRoomBlankForm;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mImgRoom = itemView.findViewById(R.id.img_room_picture);
            mNameRoomOwner = itemView.findViewById(R.id.txt_room_owner_name);
            mPrice = itemView.findViewById(R.id.txt_room_price);
            mTenants = itemView.findViewById(R.id.txt_room_tenants);
            mAcreage = itemView.findViewById(R.id.txt_room_acreage);
            mLnlRoomBlankForm = itemView.findViewById(R.id.lnl_room_blank_form);
        }
    }
}
