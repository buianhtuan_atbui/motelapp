package com.example.motelapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.motelapp.R;
import com.example.motelapp.models.Wards;

import java.util.List;

public class WardsAdapter extends RecyclerView.Adapter<WardsAdapter.ViewHolder> {
    private Context mContext;
    private List<Wards> mWardsList;
    private OnClickListener mOnClickListener;

    public WardsAdapter(Context mContext, List<Wards> mWardsList) {
        this.mContext = mContext;
        this.mWardsList = mWardsList;
    }

    @NonNull
    @Override
    public WardsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.item_row_wards, parent, false);
        return new  ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull WardsAdapter.ViewHolder holder, final int position) {
        holder.mTxtWards.setText(mWardsList.get(position).getName());

        //        onclick adapter
        holder.mLnlWards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mOnClickListener != null) {
                    mOnClickListener.onClickListener(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        int count = (mWardsList != null) ? mWardsList.size() : 0;
        return count;
    }

    //onclick interface
    public interface OnClickListener {
        void onClickListener(int position);
    }

    public void getPosition(OnClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTxtWards;
        LinearLayout mLnlWards;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTxtWards = itemView.findViewById(R.id.txt_wards);
            mLnlWards = itemView.findViewById(R.id.lnl_root_wards);
        }
    }
}
