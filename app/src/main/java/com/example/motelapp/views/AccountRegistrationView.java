package com.example.motelapp.views;

import com.example.motelapp.models.Account;
import com.example.motelapp.room.entities.User;

public interface AccountRegistrationView {
    void createAccountSuccess();
    void createAccountFail();
}
