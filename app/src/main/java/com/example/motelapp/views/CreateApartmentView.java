package com.example.motelapp.views;

import com.example.motelapp.models.Apartment;

public interface CreateApartmentView {
    void createApartmentSuccess(Apartment apartment);
    void createApartmentFail();
}
