package com.example.motelapp.views;

import com.example.motelapp.models.Apartment;

public interface DeleteApartmentView {
    void deleteApartmentSuccessful(Apartment apartment);
    void deleteApartmentFail();
}
