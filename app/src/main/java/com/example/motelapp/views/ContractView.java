package com.example.motelapp.views;

import com.example.motelapp.models.Contract;

import java.util.List;

public interface ContractView {
    void getListContractSuccess(List<Contract> contractList);
    void getListContractFail(String msgErr);
}
