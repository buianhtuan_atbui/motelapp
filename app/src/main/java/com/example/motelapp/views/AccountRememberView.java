package com.example.motelapp.views;

import com.example.motelapp.room.entities.User;

public interface AccountRememberView {

    void accountIsExist(User user);
    void accountNoExist();
}
