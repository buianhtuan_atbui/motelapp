package com.example.motelapp.views;

import com.example.motelapp.room.entities.User;

public interface LoginView {

    void loginSuccess(User user);
    void loginFail(String msgLoginFail);
}
