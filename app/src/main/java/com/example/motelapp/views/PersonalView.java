package com.example.motelapp.views;

import com.example.motelapp.room.entities.User;

public interface PersonalView {
    void showInfoPersonal(User user);
    void showInfoPersonalErr(String msg);
}
