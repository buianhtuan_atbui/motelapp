package com.example.motelapp.views;

public interface LogoutView{
    void logoutSuccess();
    void logoutFail(String msgLogouFail);
}
