package com.example.motelapp.views;

import com.example.motelapp.models.Contract;

public interface UpdateContractView {
    void updateContractSuccessful(Contract contract);
    void updateContractFail();
}
