package com.example.motelapp.views;

import com.example.motelapp.models.Wards;

import java.util.List;

public interface WardsView {
    void getListWardsSuccess(List<Wards> wardsList);
    void getListWardsFail(String msgErr);
}
