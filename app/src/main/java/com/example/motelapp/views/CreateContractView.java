package com.example.motelapp.views;

import com.example.motelapp.models.Contract;

public interface CreateContractView {
    void createContractSuccessful(Contract contract);
    void createContractFail();
}
