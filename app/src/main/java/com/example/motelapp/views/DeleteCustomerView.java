package com.example.motelapp.views;

public interface DeleteCustomerView {
    void deleteCustomerSuccessful();
    void deleteCustomerFail();
}
