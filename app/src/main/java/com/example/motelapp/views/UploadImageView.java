package com.example.motelapp.views;

import com.example.motelapp.models.Image;

import java.util.List;

public interface UploadImageView {
    void uploadImageSuccessFul(List<Image> image);
    void uploadImageFail();
}
