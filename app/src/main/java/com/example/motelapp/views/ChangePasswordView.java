package com.example.motelapp.views;

public interface ChangePasswordView {
    void changePasswordSuccessful();
    void changePasswordFail();
}
