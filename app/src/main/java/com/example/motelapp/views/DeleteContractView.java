package com.example.motelapp.views;

import com.example.motelapp.models.Contract;

public interface DeleteContractView {
    void deleteContractSuccessful(Contract contract);
    void deleteContractFail();
}
