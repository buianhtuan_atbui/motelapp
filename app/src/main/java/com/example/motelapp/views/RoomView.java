package com.example.motelapp.views;

import com.example.motelapp.models.Room;

import java.util.List;

public interface RoomView {
        void getListRoomSuccess(List<Room> roomList);
        void getListRoomFail(String msgErr);
}
