package com.example.motelapp.views;

import com.example.motelapp.models.Province;

import java.util.List;

public interface ProvinceView {
    void getListProvinceSuccess(List<Province> provinceList);
    void getListProvinceFail(String msgErr);
}
