package com.example.motelapp.views;

import com.example.motelapp.models.Room;

public interface CreateRoomView {
    void createRoomSuccess(Room room);
    void createRoomFail(String msgErr);
}
