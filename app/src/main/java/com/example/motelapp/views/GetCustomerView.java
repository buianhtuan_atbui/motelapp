package com.example.motelapp.views;

import com.example.motelapp.models.Customer;

import java.util.List;

public interface GetCustomerView {
   void getListCustomerSuccess(List<Customer> customerList);
   void getListCustomerFail();
}
