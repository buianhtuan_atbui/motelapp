package com.example.motelapp.views;

import com.example.motelapp.models.District;
import com.example.motelapp.models.Province;

import java.util.List;

public interface DistrictView {
    void getListDistrictSuccess(List<District> districtList);
    void getListDistrictFail(String msgErr);
}
