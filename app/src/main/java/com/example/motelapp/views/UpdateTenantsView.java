package com.example.motelapp.views;

import com.example.motelapp.models.Customer;

public interface UpdateTenantsView {
    void updateTenantsSuccess(Customer customer);
    void updateTenantsFail();
}
