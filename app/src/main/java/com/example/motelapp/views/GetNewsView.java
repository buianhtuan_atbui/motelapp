package com.example.motelapp.views;

import com.example.motelapp.models.News;

import java.util.List;

public interface GetNewsView {
    void getNewsSuccessful(List<News> mNewsList);
    void getNewsFail();
}
