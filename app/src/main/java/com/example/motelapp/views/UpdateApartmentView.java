package com.example.motelapp.views;

import com.example.motelapp.models.Apartment;

public interface UpdateApartmentView {
    void updateApartmentSuccessful(Apartment apartment);
    void updateApartmentFail();
}
