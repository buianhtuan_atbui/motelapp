package com.example.motelapp.views;

import com.example.motelapp.models.Bill;

public interface CreateBillView {
    void createBillSuccessful(Bill bill);
    void createBillFail();
}
