package com.example.motelapp.views;

import com.example.motelapp.models.Customer;

public interface CreateCustomerView {
    void createCustomerSuccessful(Customer customer);
    void createCustomerFail();
}
