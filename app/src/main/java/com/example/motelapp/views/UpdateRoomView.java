package com.example.motelapp.views;

import com.example.motelapp.models.Room;

public interface UpdateRoomView {
    void updateRoomSuccessful(Room room);
    void updateRoomFail();
}
