package com.example.motelapp.views;

import com.example.motelapp.models.Apartment;

import java.util.List;

public interface ApartmentView {

    void getListApartmentSuccess(List<Apartment> apartmentList);
    void getListApartmentFail(String msgErr);
}
