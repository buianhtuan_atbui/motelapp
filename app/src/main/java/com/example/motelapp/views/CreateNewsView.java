package com.example.motelapp.views;

import com.example.motelapp.models.News;

public interface CreateNewsView {
    void createNewSuccessful(News news);
    void createNewsFail();
}
